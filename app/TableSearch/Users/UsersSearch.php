<?php

namespace App\TableSearch\Users;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class UsersSearch
{
    public static function apply(Builder $query)
    {
        return static::applyDecoratorsFromRequest($query);
    }

    private static function applyDecoratorsFromRequest(Builder $query)
    {
        $filterBy = [
            'reprezentant',
            'organization_name',
            'category',
            'status',
            'reprezentantCompany',
            'idno',
            'category_company',
            'status_company'
        ];

        if ($filterBy) {
            foreach ($filterBy as $filter) {

                $decorator = static::createFilterDecorator($filter);

                if (static::isValidDecorator($decorator)) {
                    $query = $decorator::apply($query, data_get(request('search'), 'value'));
                }
            }
        }

        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filters\\' . ucwords(Str::camel($name));
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }
}

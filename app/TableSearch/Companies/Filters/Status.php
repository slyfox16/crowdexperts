<?php

namespace App\TableSearch\Companies\Filters;

use App\TableSearch\Filter;
use Illuminate\Database\Eloquent\Builder;

class Status implements Filter
{
    const STATUS_LIST = [
        0 => 'active',
        1 => 'deleted'
    ];

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        $value = array_filter(self::STATUS_LIST, function($status) use ($value) {
            return preg_match('/^' . str_replace("/", "\/", preg_quote($value)) . '/i', $status);
        });

        return $builder->when(!empty($value) && count($value) == 1, function ($query) use ($value) {
            return $query->orWhere('deleted', array_key_first($value));
        });
    }
}

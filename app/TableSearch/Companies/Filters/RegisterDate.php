<?php

namespace App\TableSearch\Companies\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use App\TableSearch\Filter;

class RegisterDate implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @param null $filterType
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        $isDate = false;
        if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/", $value)) {
            $isDate = true;
        }

        return $builder->when($isDate, function ($query) use ($value) {
            $daterange = Carbon::createFromFormat('d/m/Y', $value);
            return $query->orWhere('register_date', '=', $daterange->format('Y-m-d'));
        });
    }
}

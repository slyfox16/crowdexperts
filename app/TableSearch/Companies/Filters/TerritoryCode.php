<?php

namespace App\TableSearch\Companies\Filters;

use App\TableSearch\Filter;
use Illuminate\Database\Eloquent\Builder;

class TerritoryCode implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->when(!empty($value), function ($query) use ($value) {
            return $query->orWhere('territory_code', $value);
        });
    }
}

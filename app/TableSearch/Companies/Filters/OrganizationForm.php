<?php

namespace App\TableSearch\Companies\Filters;

use App\TableSearch\Filter;
use Illuminate\Database\Eloquent\Builder;

class OrganizationForm implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->when(!empty($value), function ($query) use ($value) {
            return $query->orWhereHas('organisationForm', function (Builder $q) use ($value) {
                $q->where('type', 'LIKE', '%' . $value . '%');
            });
        });
    }
}

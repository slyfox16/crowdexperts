<?php

namespace App\TableSearch\Companies;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class CompaniesSearch
{
    public static function apply(Builder $query)
    {
        return static::applyDecoratorsFromRequest($query);
    }

    private static function applyDecoratorsFromRequest(Builder $query)
    {
        $filterBy = [
            'idno',
            'register_date',
            'company_name',
            'organization_form',
            'territory_code',
            'status'
        ];

        if ($filterBy) {
            foreach ($filterBy as $filter) {

                $decorator = static::createFilterDecorator($filter);

                if (static::isValidDecorator($decorator)) {
                    $query = $decorator::apply($query, data_get(request('search'), 'value'));
                }
            }
        }

        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filters\\' . ucwords(Str::camel($name));
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }
}

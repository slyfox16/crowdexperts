<?php

namespace App\TableSearch\MemberCE\Filters;

use App;
use DB;
use App\TableSearch\Filter;
use Illuminate\Database\Eloquent\Builder;

class OrganizationName implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->when(!empty($value), function ($query) use ($value) {
            return $query->whereHas('cememberInfo', function (Builder $q) use ($value) {
                $q->where(DB::raw('JSON_UNQUOTE(JSON_EXTRACT(organization_name, "$.' . App::getLocale() . '"))'), 'LIKE', '%' . $value . '%');
            });
        });
    }
}

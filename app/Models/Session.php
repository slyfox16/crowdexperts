<?php

namespace App\Models;

use App\Models\BasicModel as Model;
use Illuminate\Support\Facades\Auth;

class Session extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * Chats
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chats()
    {
        return $this->hasMany(Chat::class, 'session_id', 'id');
    }

    /**
     * Messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class, 'session_id', 'id');
    }

    public function deleteChats()
    {
        $this->chats()->where('user_id', Auth::id())->delete();
    }

    public function deleteMessages()
    {
        $this->messages()->delete();
    }

    public function block()
    {
        $this->update([
            'block' => true,
            'blocked_by' => Auth::id()
        ]);
    }

    public function getBlocked()
    {
        return $this->block;
    }

    public function unblock()
    {
        $this->update([
            'block' => false,
            'blocked_by' => null
        ]);
    }
}

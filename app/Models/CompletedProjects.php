<?php

namespace App\Models;

use App\Models\BasicModel as Model;
use App\Traits\Uploadable;

class CompletedProjects extends Model
{
    use Uploadable;

    /**
     * Are available 3 indexes:
     * - `path` Path where the file will be moved.
     * - `tempPath` Temporary path from where file will be moved.
     * - `url` Path URL where file will be saved.
     *
     * @var array Attributes array
     */
    public $uploadSettings = [
        'image'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'date',
    ];

    public $slug = 'association-membership';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'completed_projects';
}

<?php

namespace App\Models;

use App\Models\BasicModel as Model;

class EventUser extends Model
{
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events_users';
}

<?php

namespace App\Models\amigo;

use Amigo\Cms\Traits\EloquentTrait;
use Amigo\Cms\Traits\ModelEventLogger;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use EloquentTrait, ModelEventLogger;

    protected $fillable = ['locale', 'title', 'site', 'admin'];

    public $listing = ['title' => 'Title', 'locale' => 'Locale'];

    public $form = [
        ['type' => 'text', 'multilingual' => false, 'name' => 'title', 'label' => 'Title'],
        ['type' => 'text', 'multilingual' => false, 'name' => 'locale', 'label' => 'Locale'],
        ['type' => 'checkbox', 'multilingual' => false, 'name' => 'site', 'label' => 'Site'],
        ['type' => 'checkbox', 'multilingual' => false, 'name' => 'admin', 'label' => 'Admin']
    ];
}

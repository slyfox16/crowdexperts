<?php

namespace App\Models\amigo;

use Amigo\Cms\Traits\ModelEventLogger;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Menu extends Model
{
    protected $table = 'menu';

    use HasTranslations, ModelEventLogger;

    public $translatable = [];

    public function list()
    {
        return $this->hasMany('App\Models\amigo\MenuPages', 'menu_id')->orderBy('position');
    }
}

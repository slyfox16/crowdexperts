<?php

namespace App\Models\amigo;

use Amigo\Cms\Traits\ModelEventLogger;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class AmigoPage extends Model
{
    protected $table = 'amigo_pages';

    use HasTranslations, ModelEventLogger;

    public $translatable = ['title', 'text', 'seo_title', 'seo_keywords', 'seo_description'];

    public function children()
    {
        return $this->hasMany('App\Models\amigo\AmigoPage', 'parent_id','id');
    }
}

<?php

namespace App\Models\amigo;

use Amigo\Cms\Traits\ModelEventLogger;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class MenuPages extends Model
{
    protected $table = 'menu_pages';

    use HasTranslations, ModelEventLogger;

    public $translatable = [];

    public function page()
    {
        return $this->belongsTo('App\Models\amigo\AmigoPage', 'page_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\amigo\MenuPages', 'parent_id','id');
    }
}

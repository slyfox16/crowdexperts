<?php

namespace App\Models\amigo;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'key', 'display_name', 'value', 'type', 'order', 'group'
    ];
}

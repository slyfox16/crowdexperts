<?php

namespace App\Models;

use App\Extensions\DataTable\DataTableModel;
use App\Traits\Uploadable;
use Html;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Image;

class Admin extends Authenticatable
{
    use Uploadable, HasFactory, Notifiable, DataTableModel;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'admins';

    /**
     * Are available 3 indexes:
     * - `path` Path where the file will be moved.
     * - `tempPath` Temporary path from where file will be moved.
     * - `url` Path URL where file will be saved.
     *
     * @var array Attributes array
     */
    public $uploadSettings = [
        'avatar'
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param $password
     */
    public function changePassword($password): void
    {
        $this->update(['password' => bcrypt($password)]);
    }

    public function getAvatar()
    {
        $filePath = $this->getFilePath('avatar');
        $cropedFileName = str_replace('.', '_100_100.', $this->avatar);
        $pathCropedFile = storage_path('app/' . $this->getPath('avatar') . $cropedFileName);

        if (!File::exists($pathCropedFile) && $filePath) {
            Image::make(schema_asset($this->getFile('avatar')))->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($pathCropedFile);
        }

        return Html::image($this->getUrl('avatar') . $cropedFileName, $this->name)->toHtml();
    }

    public function canDeleteAdmin()
    {
        return $this->id != Auth::guard('admin')->id() && $this->id != 1;
    }
}

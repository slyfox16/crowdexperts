<?php

namespace App\Models;

use App\Models\BasicModel as Model;

class Chat extends Model
{
    const SEND = 'sent';
    const RECEIVED = 'received';

    /**
     * @var bool
     */
    public $timestamps = true;

    protected $casts = [
        'read_at' => 'datetime'
    ];

    /**
     * Get Message.
     */
    public function message()
    {
        return $this->belongsTo(Message::class, 'message_id', 'id');
    }
}

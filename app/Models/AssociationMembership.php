<?php

namespace App\Models;

use App\Models\BasicModel as Model;
use App\Traits\Uploadable;

class AssociationMembership extends Model
{
    use Uploadable;

    /**
     * Are available 3 indexes:
     * - `path` Path where the file will be moved.
     * - `tempPath` Temporary path from where file will be moved.
     * - `url` Path URL where file will be saved.
     *
     * @var array Attributes array
     */
    public $uploadSettings = [
        'logo'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'association_membership';

    /**
     * Role List
     *
     * @return array
     */
    public static function associationList(): array
    {
        return self::all()->pluck('name', 'id')->toArray();
    }
}

<?php

namespace App\Models;

use App\Extensions\DataTable\DataTableModel;
use App\Traits\Uploadable;
use Auth;
use DB;
use Html;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Image;

class User extends \TCG\Voyager\Models\User
{
    use Uploadable, HasFactory, Notifiable, DataTableModel, SoftDeletes;

    const STATUS_ACTIVE = 1;

    /**
     * Are available 3 indexes:
     * - `path` Path where the file will be moved.
     * - `tempPath` Temporary path from where file will be moved.
     * - `url` Path URL where file will be saved.
     *
     * @var array Attributes array
     */
    public $uploadSettings = [
        'avatar'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'username';
    }

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Role List
     *
     * @return array
     */
    public function countryList(): array
    {
        return $this->country()->pluck('namede', 'id')->toArray();
    }

    /**
     * Role List
     *
     * @return array
     */
    public function cityList(): array
    {
        return $this->city()->pluck('ascii', 'id')->toArray();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function associations()
    {
        return $this->belongsToMany(AssociationMembership::class, 'user_association', 'user_id', 'association_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function certificates()
    {
        return $this->belongsToMany(Certificate::class, 'user_certificate', 'user_id', 'certificate_id')->withPivot('date');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function specialities()
    {
        return $this->belongsToMany(Speciality::class, 'user_speciality', 'user_id', 'speciality_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany(Event::class, 'events_users', 'user_id', 'event_id');
    }

    /**
     * Partner categories.
     */
    public function projects()
    {
        return $this->hasMany(CompletedProjects::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', self::STATUS_ACTIVE);
    }

    // Events
    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->username = $model->createSlug(request()->input('username'));

            if (!$model->country_id) {
                $model->country_id = 55;
            }

            if (!$model->city_id) {
                $model->city_id = 8065570;
            }

            $model->is_active = 1;
        });
    }

    public function getCountryCity()
    {
        $arr = [];
        $arr[] = optional($this->country)->namede;
        $arr[] = optional($this->city)->ascii;

        if (!$arr) {
            return $arr;
        }

        return implode(', ', $arr);
    }

    /**
     * @return array
     */
    public static function getChatFriends()
    {
        $friendsList = DB::table('chat_friends')
            ->select('first_user_id', 'second_user_id')
            ->where('first_user_id', Auth::id())
            ->get();

        $friendsList = $friendsList->map(function ($friend) {
            if (data_get($friend, 'first_user_id') == Auth::id()) {
                return data_get($friend, 'second_user_id');
            }

            if (data_get($friend, 'second_user_id') == Auth::id()) {
                return data_get($friend, 'first_user_id');
            }

            return false;
        })
        ->reject(function ($value) {
            return $value === false;
        });

        return $friendsList->toArray();
    }

    public function getAvatar()
    {
        $cropedFileName = str_replace('.', '_100_100.', $this->avatar);
        $pathCropedFile = storage_path('app/' . $this->getPath('avatar') . $cropedFileName);

        if (!File::exists($pathCropedFile)) {
            Image::make(schema_asset($this->getFile('avatar')))->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($pathCropedFile);
        }

        return Html::image($this->getUrl('avatar') . $cropedFileName, $this->name)->toHtml();
    }

    public function getConfirmStatus()
    {
        if ($this->expert_confirm) {
            return Html::tag('i', '', ['class' => 'fa fa-check'])->toHtml();
        } else {
            return Html::tag('i', '', ['class' => 'fa fa-minus'])->toHtml();
        }
    }

    public function getActive()
    {
        if ($this->active) {
            return Html::tag('i', '', ['class' => 'fa fa-check'])->toHtml();
        } else {
            return Html::tag('i', '', ['class' => 'fa fa-minus'])->toHtml();
        }
    }

    public function userInFriends()
    {
        return DB::table('chat_friends')
            ->where('first_user_id', Auth::id())
            ->where('second_user_id', $this->id)
            ->exists();
    }

    private function createSlug($slug = null, $postfix = null)
    {
        $slug = Str::slug($slug, '-');

        if (!$slug) {
            $slug = Str::slug($this->name, '-');
        }

        $slug = $slug . $postfix;

        $alreadyUsername = User::where('username', $slug)->first();
        if ($alreadyUsername) {
            $postfix = $postfix ? $postfix : 0;
            $this->createSlug($slug, $postfix++);
        }

        return $slug;
    }

    public function addUserToFriends()
    {
        DB::table('chat_friends')->insert([
            'first_user_id' => \Illuminate\Support\Facades\Auth::id(),
            'second_user_id' => $this->id,
        ]);

        DB::table('chat_friends')->insert([
            'first_user_id' => $this->id,
            'second_user_id' => Auth::id(),
        ]);
    }
}

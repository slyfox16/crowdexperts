<?php

namespace App\Models;

use App\Models\BasicModel as Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * Chats
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chats()
    {
        return $this->hasMany(Chat::class, 'message_id', 'id');
    }

    public function createForSend($session_id)
    {
        return $this->chats()->create([
            'session_id' => $session_id,
            'type' => Chat::SEND,
            'user_id' => Auth::id()
        ]);
    }

    public function createForReceive($session_id, $to_user)
    {
        return $this->chats()->create([
            'session_id' => $session_id,
            'type' => Chat::RECEIVED,
            'user_id' => $to_user
        ]);
    }
}

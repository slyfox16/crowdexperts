<?php

namespace App\Models;

use App\Models\BasicModel as Model;
use DB;

class City extends Model
{
    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cities';

    /**
     * Role Taken Cities
     *
     * @return array
     */
    public static function getTakenCities(): array
    {
        return DB::table('users')
            ->join('cities', 'cities.id', '=', 'users.city_id')
            ->pluck('cities.ascii', 'cities.id')
            ->toArray();
    }
}

<?php

namespace App\Models;

use App\Models\BasicModel as Model;

class Country extends Model
{
    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';
}

<?php

namespace App\Models;

use App\Models\BasicModel as Model;

class Speciality extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'speciality';

    /**
     * Role List
     *
     * @return array
     */
    public static function specialityList(): array
    {
        return self::all()->pluck('speciality', 'id')->toArray();
    }
}

<?php

namespace App\Models;

use App\Models\BasicModel as Model;
use DB;

class Page extends Model
{
    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';
}

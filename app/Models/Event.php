<?php

namespace App\Models;

use App\Extensions\DataTable\DataTableModel;
use App\Models\BasicModel as Model;
use App\Traits\Uploadable;
use Carbon\Carbon;
use Html;
use Illuminate\Support\Facades\Auth;

class Event extends Model
{
    use Uploadable, DataTableModel;

    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * Are available 3 indexes:
     * - `path` Path where the file will be moved.
     * - `tempPath` Temporary path from where file will be moved.
     * - `url` Path URL where file will be saved.
     *
     * @var array Attributes array
     */
    public $uploadSettings = [
        'image'
    ];

    public $slug = 'events';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date_start' => 'date',
        'date_end' => 'date',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    // Events
    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            if (request('user_id')) {
                $model->user_id = request('user_id');
            }
        });

        static::creating(function ($model) {
            if (request()->input('daterange')) {
                $model->user_id = Auth::id();
                $model->active = request()->has('active');

                $daterange = explode(' - ', request()->input('daterange'));
                $model->date_start = Carbon::createFromFormat('d/m/Y', data_get($daterange, 0))->format('Y-m-d');
                $model->date_end = Carbon::createFromFormat('d/m/Y', data_get($daterange, 1))->format('Y-m-d');
            }
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', self::STATUS_ACTIVE)
            ->where('date_end', '>=', today());
    }

    public function getUser()
    {
        return optional($this->user)->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    /**
     * Get Users List.
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'events_users', 'event_id', 'user_id');
    }

    public function getCountryCity()
    {
        $arr = [];
        $arr[] = optional($this->country)->namede;
        $arr[] = optional($this->city)->ascii;

        return implode(', ', $arr);
    }

    public function getActive()
    {
        if ($this->active) {
            return Html::tag('i', '', ['class' => 'fa fa-check'])->toHtml();
        } else {
            return Html::tag('i', '', ['class' => 'fa fa-minus'])->toHtml();
        }
    }

    /**
     * Role List
     *
     * @return array
     */
    public function countryList(): array
    {
        return $this->country()->pluck('namede', 'id')->toArray();
    }

    /**
     * Role List
     *
     * @return array
     */
    public function cityList(): array
    {
        return $this->city()->pluck('ascii', 'id')->toArray();
    }
}

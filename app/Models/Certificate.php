<?php

namespace App\Models;

use App\Extensions\DataTable\DataTableModel;
use App\Models\BasicModel as Model;
use App\Traits\Uploadable;
use Html;
use Illuminate\Support\Str;

class Certificate extends Model
{
    use Uploadable;

    /**
     * Are available 3 indexes:
     * - `path` Path where the file will be moved.
     * - `tempPath` Temporary path from where file will be moved.
     * - `url` Path URL where file will be saved.
     *
     * @var array Attributes array
     */
    public $uploadSettings = [
        'logo'
    ];

    protected $fillable = [
        'name',
        'logo',
        'points',
        'description',
        'slug',
    ];

    public $slug = 'certificate';

    // Events
    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->slug2 = $model->createSlug(request()->input('name'));
        });
    }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'certificates';

    /**
     * Role List
     *
     * @return array
     */
    public static function certificateList(): array
    {
        return self::all()->pluck('name', 'id')->toArray();
    }

    public function getLogo()
    {
        return Html::image($this->getFile('logo'), $this->name, ['width' => '60px'])->toHtml();
    }

    private function createSlug($slug = null, $postfix = null)
    {
        $slug = Str::slug($slug, '-');

        if (!$slug) {
            $slug = Str::slug($this->name, '-');
        }

        $slug = $slug . $postfix;

        $alreadyUsername = User::where('username', $slug)->first();
        if ($alreadyUsername) {
            $postfix = $postfix ? $postfix : 0;
            $this->createSlug($slug, $postfix++);
        }

        return $slug;
    }
}

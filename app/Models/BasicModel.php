<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BasicModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicModel dataTable()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicModel query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicModel nPerGroup($group, $n = 10)
 */
class BasicModel extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * @var array
     */
    protected $guarded = [];
}

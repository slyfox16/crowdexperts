<?php

namespace App\Notifications;

use Auth;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ContactMassage extends Notification
{
    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param $fullName
     * @param $callPhone
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('You have a new message from crowdexperts.de')
            ->greeting('Hello!')
            ->line('Name - ' . Auth::user()->name)
            ->line('Email - ' . Auth::user()->email)
            ->line('Message - ' . request('message'));
    }
}

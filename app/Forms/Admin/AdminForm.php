<?php

namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class AdminForm extends Form
{
    public function buildForm()
    {
        $uniqueExcept = !empty($this->model) ? $this->model->id : null;

        $this
            ->add('avatar', 'fileinput', [
                'rules' => 'nullable|mimes:jpg,bmp,png',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('name', 'text', [
                'rules' => "required|string|max:255",
                'template' => 'fields.text',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('email', 'text', [
                'rules' => "required|email|max:255|unique:users,email,{$uniqueExcept},id",
                'template' => 'fields.text',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('password', Field::PASSWORD, [
                'rules' => 'nullable|string|min:8|confirmed',
                'attr' => [
                    'required' => false,
                ],
                'wrapper' => [
                    'class' => 'form-group',
                ],
                'value' => ''
            ])
            ->add('password_confirmation', Field::PASSWORD, [
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ]);
    }
}

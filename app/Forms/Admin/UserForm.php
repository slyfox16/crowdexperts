<?php

namespace App\Forms\Admin;

use Illuminate\Database\Query\Expression;
use Kris\LaravelFormBuilder\Form;

class UserForm extends Form
{
    public function buildForm()
    {
        $uniqueExcept = !empty($this->model) ? $this->model->id : null;

        $this
            ->add('avatar', 'fileinput', [
                'rules' => 'nullable|mimes:jpg,bmp,png',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('name', 'text', [
                'rules' => "required|string|max:255",
                'template' => 'fields.text',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('email', 'text', [
                'rules' => "required|email|max:255|unique:users,email,{$uniqueExcept},id",
                'template' => 'fields.text',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('title', 'text', [
                'rules' => "required|string|max:255",
                'template' => 'fields.text',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('username', 'text', [
                'rules' => "required|string|max:255",
                'template' => 'fields.text',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('phone', 'text', [
                'rules' => 'required|string|max:255',
                'template' => 'fields.text',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('country_id', 'select2', [
                'rules' => 'nullable|exists:countries,iso',
                'widgetOptions' => [
                    'dataType' => 'json',
                    'width' => '100%',
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'ajax' => [
                        'url' => route('cabinet.suggestCountry'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('countryFormatResult'),
                    'templateSelection' => new Expression('countryFormatSelection'),
                ],
                'label' => 'Select country',
                'choices' => $this->model ? optional($this->model)->countryList() : []
            ])
            ->add('city_id', 'select2', [
                'rules' => 'nullable|exists:cities,id',
                'widgetOptions' => [
                    'dataType' => 'json',
                    'width' => '100%',
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'ajax' => [
                        'url' => route('cabinet.suggestCity'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('cityFormatResult'),
                    'templateSelection' => new Expression('cityFormatSelection'),
                ],
                'label' => 'Select city',
                'choices' => $this->model ? optional($this->model)->cityList() : []
            ]);
    }
}

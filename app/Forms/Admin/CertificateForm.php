<?php

namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class CertificateForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', Field::TEXT, [
                'label' => __('frontend.title'),
                'rules' => 'required|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('points', Field::TEXT, [
                'label' => __('frontend.points'),
                'rules' => 'required|integer',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('logo', 'fileinput', [
                'label' => __('frontend.logo'),
                'rules' => 'nullable|mimes:jpg,bmp,png',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('description', Field::TEXTAREA, [
                'label' => __('frontend.description'),
                'rules' => 'required|string|max:2000',
                'wrapper' => [
                    'class' => 'form-group',
                ],
                'attr' => [
                    'rows' => 10
                ]
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Create event',
                'attr' => [
                    'class' => 'btn btn-block btn-primary',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

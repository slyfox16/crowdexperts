<?php

namespace App\Forms\Admin;

use Illuminate\Database\Query\Expression;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class EventForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', Field::TEXT, [
                'label' => __('frontend.title'),
                'rules' => 'required|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('description', Field::TEXTAREA, [
                'label' => __('frontend.description'),
                'rules' => 'required|string|max:2000',
                'wrapper' => [
                    'class' => 'form-group',
                ],
                'attr' => [
                    'rows' => 4
                ]
            ])
            ->add('facebook_url', Field::URL, [
                'label' => __('Facebook url'),
                'rules' => 'nullable|url|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('twitter_url', Field::URL, [
                'label' => __('Twitter url'),
                'rules' => 'nullable|url|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('xing_url', Field::URL, [
                'label' => __('Xing url'),
                'rules' => 'nullable|url|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('web_url', Field::URL, [
                'label' => __('Linkedin url'),
                'rules' => 'nullable|url|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('daterange', 'daterangepicker', [
                'rules' => 'required',
                'widgetOptions' => [
                    'todayHighlight' => true,
                    'width' => '100%',
                    'drops' => 'up',
                    'minDate' => '0',
                    'locale' => [
                        'format' => 'DD/MM/YYYY',
                        'applyLabel' => 'Anwenden',
                        'cancelLabel' => 'Abbrechen',
                        'fromLabel' => 'Absender',
                        'toLabel' => 'Empfänger',
                        'daysOfWeek' => ['S','M','D','M','D','F','S'],
                        'monthNames' => [
                            'Januar',
                            'Februar',
                            'März',
                            'April',
                            'Mai',
                            'Juni',
                            'Juli',
                            'August',
                            'September',
                            'Oktober',
                            'November',
                            'Dezember'
                        ]
                    ]
                ],
                'label' => __('frontend.daterange'),
            ])
            ->add('address', Field::TEXT, [
                'label' => __('frontend.address'),
                'rules' => 'required|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('country_id', 'select2', [
                'rules' => 'nullable|exists:countries,iso',
                'widgetOptions' => [
                    'dataType' => 'json',
                    'width' => '100%',
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'ajax' => [
                        'url' => route('cabinet.suggestCountry'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('countryFormatResult'),
                    'templateSelection' => new Expression('countryFormatSelection'),
                ],
                'label' => 'Select country',
                'choices' => $this->model ? optional($this->model)->countryList() : []
            ])
            ->add('city_id', 'select2', [
                'rules' => 'nullable|exists:cities,id',
                'widgetOptions' => [
                    'dataType' => 'json',
                    'width' => '100%',
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'ajax' => [
                        'url' => route('cabinet.suggestCity'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('cityFormatResult'),
                    'templateSelection' => new Expression('cityFormatSelection'),
                ],
                'label' => 'Select city',
                'choices' => $this->model ? optional($this->model)->cityList() : []
            ])
            ->add('image', 'fileinput', [
                'label' => __('frontend.image'),
                'rules' => 'nullable|mimes:jpg,bmp,png',
                'wrapper' => [
                    'class' => "col-xl-12 col-md-10",
                ],
            ])
            ->add('active', Field::CHECKBOX, [
                'label_attr' => ['class' => 'custom-control-label'],
                'value' => false,
                'checked' => optional($this->model)->link_active,
                'template' => 'fields.checkbox',
                'attr' => [
                    'class' => 'custom-control-input',
                ],
                'wrapper' => [
                    'class' => 'col-xl-12 col-md-10',
                ]
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Create event',
                'attr' => [
                    'class' => 'btn btn-block btn-primary',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

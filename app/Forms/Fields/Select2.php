<?php namespace App\Forms\Fields;

use Illuminate\Support\Str;
use Kris\LaravelFormBuilder\Fields\FormField;

class Select2 extends FormField
{
    /**
     * The name of the property that holds the value.
     *
     * @var string
     */
    protected $valueProperty = 'selected';

    protected function getTemplate()
    {
        return 'fields.select2';
    }

    public function render(array $options = [], $showLabel = true, $showField = true, $showError = true)
    {
        $options['attr']['id'] = $this->getOption('attr.id', Str::slug($this->getName()));
        return parent::render($options, $showLabel, $showField, $showError);
    }

    /**
     * @inheritdoc
     */
    public function getDefaults()
    {
        return [
            'choices' => [],
            'empty_value' => null,
            'selected' => null
        ];
    }
}

<?php namespace App\Forms\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class Datepicker extends FormField
{
    protected function getTemplate()
    {
        return 'fields.datepicker';
    }

    public function render(array $options = [], $showLabel = true, $showField = true, $showError = true)
    {
        $options['attr']['id'] = $this->getOption('attr.id', \Str::slug($this->getName()));
        return parent::render($options, $showLabel, $showField, $showError);
    }
}

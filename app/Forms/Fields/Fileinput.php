<?php namespace App\Forms\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class Fileinput extends FormField
{
    protected function getTemplate()
    {
        return 'fields.fileinput';
    }

    public function render(array $options = [], $showLabel = true, $showField = true, $showError = true)
    {
        $options['model'] = $this->parent->getModel();
        return parent::render($options, $showLabel, $showField, $showError);
    }
}

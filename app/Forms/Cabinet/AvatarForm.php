<?php

namespace App\Forms\Cabinet;

use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class AvatarForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('avatar', Field::FILE, [
                'rules' => 'nullable|mimetypes:image/jpeg,image/png',
                'attr' => [
                    'id' => 'avatar',
                    'hidden' => 'hidden',
                    'class' => false,
                ],
            ]);
    }
}

<?php

namespace App\Forms\Cabinet;

use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class ContactsForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('country_id', 'select2', [
                'rules' => 'nullable|exists:countries,id',
                'widgetOptions' => [
                    'dataType' => 'json',
                    'width' => '100%',
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'ajax' => [
                        'url' => route('cabinet.suggestCountry'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('countryFormatResult'),
                    'templateSelection' => new Expression('countryFormatSelection'),
                ],
                'label' => __('frontend.country'),
                'choices' => Auth::user()->countryList()
            ])
            ->add('city_id', 'select2', [
                'rules' => 'nullable|exists:cities,id',
                'widgetOptions' => [
                    'dataType' => 'json',
                    'width' => '100%',
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'ajax' => [
                        'url' => route('cabinet.suggestCity'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('cityFormatResult'),
                    'templateSelection' => new Expression('cityFormatSelection'),
                ],
                'label' => __('frontend.city'),
                'choices' => Auth::user()->cityList()
            ])
            ->add('address', Field::TEXT, [
                'label' => __('frontend.address'),
                'rules' => 'nullable|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('phone', Field::TEXT, [
                'label' => __('frontend.phone'),
                'rules' => 'nullable|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('facebook_url', Field::URL, [
                'label' => __('Facebook url'),
                'rules' => 'nullable|url|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('linkedin_url', Field::URL, [
                'label' => __('Linkedin url'),
                'rules' => 'nullable|url|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('twitter_url', Field::URL, [
                'label' => __('Twitter url'),
                'rules' => 'nullable|url|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('xing_url', Field::URL, [
                'label' => __('Xing url'),
                'rules' => 'nullable|url|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Save Changes',
                'wrapper' => [
                    'class' => 'col-lg-4'
                ],
                'attr' => [
                    'class' => 'btn btn-primary form_submit_button',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

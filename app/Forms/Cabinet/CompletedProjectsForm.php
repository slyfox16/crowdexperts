<?php

namespace App\Forms\Cabinet;

use App\Models\AssociationMembership;
use App\Models\Speciality;
use Illuminate\Database\Query\Expression;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class CompletedProjectsForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', Field::TEXT, [
                'label' => __('frontend.project_name'),
                'rules' => 'required|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('image', Field::FILE, [
                'label' => __('frontend.image'),
                'rules' => 'required|mimetypes:image/jpeg,image/png',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('description', Field::TEXTAREA, [
                'label' => __('frontend.description'),
                'rules' => 'required|string|max:2000',
                'wrapper' => [
                    'class' => 'form-group',
                ],
                'attr' => [
                    'rows' => 4
                ]
            ])
            ->add('date', 'datepicker', [
                'label' => 'Date',
                'rules' => 'required|date',
                'widgetOptions' => [
                    'rtl' => !1,
                    'todayHighlight' => !0,
                    'orientation' => "bottom left",
                    'templates' => [
                        'leftArrow' => '<i class="fa fa-angle-left"></i>',
                        'rightArrow' => '<i class="fa fa-angle-right"></i>'
                    ],
                    'format' => 'yyyy-mm-dd',
                ],
                'attr' => [
                    'id' => 'project-date',
                    'class' => 'form-control',
                    'autocomplete' => "off",
                ],
            ])
            ->add('link', Field::URL, [
                'label' => __('link'),
                'rules' => 'required|url|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Add Project',
                'wrapper' => [
                    'class' => 'col-lg-4'
                ],
                'attr' => [
                    'class' => 'btn btn-primary form_submit_button',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

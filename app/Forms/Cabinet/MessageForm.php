<?php

namespace App\Forms\Cabinet;

use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class MessageForm extends Form
{
    public function buildForm()
    {
        $this
//            ->add('name', Field::TEXT, [
//                'label' => __('frontend.name'),
//                'rules' => 'required|string|max:255',
//                'attr' => [
//                    'wire:model' => 'filter.name'
//                ],
//                'wrapper' => [
//                    'class' => 'form-group',
//                ],
//            ])
//            ->add('email', Field::EMAIL, [
//                'label' => 'Email address',
//                'rules' => 'required|email|max:255',
//                'wrapper' => [
//                    'class' => 'form-group',
//                ],
//            ])
            ->add('message', Field::TEXTAREA, [
                'label' => __('frontend.massage'),
                'rules' => 'required|string|max:2000',
                'wrapper' => [
                    'class' => 'form-group',
                ],
                'attr' => [
                    'rows' => 4
                ]
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Send message',
                'attr' => [
                    'id' => 'filter-form-submit',
                    'class' => 'btn btn-block btn-primary',
                ],
            ]);
    }
}

<?php

namespace App\Forms\Cabinet;

use App\Models\AssociationMembership;
use App\Models\Speciality;
use Illuminate\Database\Query\Expression;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class CabinetForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', Field::TEXT, [
                'label' => __('frontend.title'),
                'rules' => 'sometimes|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('name', Field::TEXT, [
                'label' => __('frontend.name'),
                'rules' => 'required|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('username', Field::TEXT, [
                'label' => __('frontend.username'),
                'rules' => 'required|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('email', Field::EMAIL, [
                'label' => __('frontend.email'),
                'rules' => 'required|email|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('specialities', 'select2', [
                'attr' => [
                    'id' => 'dt-tags',
                    'class' => 'form-control tokenfield input-lg form-control-lg',
                    'multiple' => "multiple",
                ],
                'widgetOptions' => [
                    'placeholder' => 'Select by tags',
                    'tags' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => route('cabinet.specialities'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('formatRepo'),
                    'templateSelection' => new Expression('formatRepoSelection'),
                    'escapeMarkup' => new Expression('function (m) { return m; }'),
                    'createTag' => new Expression('function (params) { return null; }'),
                ],
                'label' => __('frontend.specialities'),
                'choices' => Speciality::specialityList(),
            ])
            ->add('association_membership', 'select2', [
                'label' => __('frontend.product_association'),
                'choices' => AssociationMembership::associationList(),
                'widgetOptions' => [
                    'tags' => false,
                ],
                'attr' => [
                    'id' => 'assoc_list',
                ],
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('companyname', Field::TEXT, [
                'label' => __('frontend.firm_name'),
                'rules' => 'nullable|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('web_url', Field::TEXT, [
                'label' => __('frontend.web_url'),
                'rules' => 'nullable|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('position', Field::TEXT, [
                'label' => __('frontend.position'),
                'rules' => 'nullable|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('description', Field::TEXTAREA, [
                'label' => __('frontend.description'),
                'wrapper' => [
                    'class' => 'form-group',
                ],
                'attr' => [
                    'rows' => 4
                ]
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Save Changes',
                'wrapper' => [
                    'class' => 'col-lg-4'
                ],
                'attr' => [
                    'class' => 'btn btn-primary form_submit_button',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

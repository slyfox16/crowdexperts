<?php

namespace App\Forms\Cabinet;

use App\Models\Certificate;
use App\Models\City;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class FilterForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', Field::TEXT, [
                'label' => __('frontend.name'),
                'rules' => 'level|string|max:255',
                'attr' => [
                    'wire:model' => 'filter.name'
                ],
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('city_id', 'select2', [
                'rules' => 'nullable|exists:cities,id',
                'attr' => [
                    'wire:model' => 'filter.city_id'
                ],
                'widgetOptions' => [
                    'dataType' => 'json',
                    'width' => '100%',
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'allowClear' => true,
                    'dropdownParent' => 'aside.overlay',
                    'placeholder' => "Select the city",
                    'ajax' => [
                        'url' => route('cabinet.suggestCity'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('cityFormatResult'),
                    'templateSelection' => new Expression('cityFormatSelection'),
                ],
                'label' => __('frontend.city'),
            ])
            ->add('certificate_id', 'select2', [
                'label' => __('frontend.certificate'),
                'choices' => Certificate::certificateList(),
                'empty_value' => 'Select certificate',
                'attr' => [
                    'wire:model' => 'filter.certificate_id'
                ],
                'widgetOptions' => [
                    'tags' => true,
                    'width' => '100%'
                ],
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
//            ->add('rating', Field::TEXT, [
//                'label' => __('frontend.rating'),
//                'rules' => 'nullable|string|max:255',
//                'attr' => [
//                    'wire:model.lazy' => 'filter.rating'
//                ],
//                'wrapper' => [
//                    'class' => 'form-group',
//                ],
//            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Search',
                'attr' => [
                    'id' => 'filter-form-submit',
                    'class' => 'btn btn-block btn-primary',
                ],
            ]);
    }
}

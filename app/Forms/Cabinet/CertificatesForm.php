<?php

namespace App\Forms\Cabinet;

use App\Models\Certificate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class CertificatesForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('certificate_id', 'select2', [
                'label' => __('frontend.product_association'),
                'choices' => Certificate::certificateList(),
                'rules' => [
                    'required',
                    'exists:certificates,id',
                    Rule::unique('user_certificate', 'certificate_id')->where(function ($query) {
                        return $query->where('user_id', Auth::id());
                    }),
                ],
                'widgetOptions' => [
                    'tags' => true,
                    'width' => '100%'
                ],
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('date', 'datepicker', [
                'label' => 'Due date',
                'rules' => 'required|date',
                'widgetOptions' => [
                    'rtl' => !1,
                    'todayHighlight' => !0,
                    'orientation' => "bottom left",
                    'templates' => [
                        'leftArrow' => '<i class="fa fa-angle-left"></i>',
                        'rightArrow' => '<i class="fa fa-angle-right"></i>'
                    ],
                    'format' => 'yyyy-mm-dd',
                ],
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => "off",
                ],
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Add Certificate',
                'wrapper' => [
                    'class' => 'col-lg-4'
                ],
                'attr' => [
                    'class' => 'btn btn-primary form_submit_button',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

<?php

namespace App\Forms\Auth;

use App\Forms\Validations\GoogleReCaptchaV3ValidationRule;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class LoginForm extends Form
{
    public function buildForm()
    {
        $this
            ->setFormOptions([
                'id' => 'login-form',
                'method' => 'POST',
                'novalidate' => 'novalidate',
            ])
            ->add('email', Field::EMAIL, [
                'rules' => ['required', 'email', 'max:255'],
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('password', Field::PASSWORD, [
                'rules' => 'required|string|min:8',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('remember_me', Field::CHECKBOX, [
                'value' => 1,
                'checked' => true,
                'rules' => [new GoogleReCaptchaV3ValidationRule('default')],
                'wrapper' => [
                    'class' => 'custom-control custom-checkbox',
                ],
                'attr' => [
                    'class' => 'custom-control-input',
                ],
                'label_attr' => [
                    'class' => 'custom-control-label',
                ],
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Sign in',
                'wrapper' => [
                    'class' => 'col-lg-4'
                ],
                'attr' => [
                    'class' => 'btn btn-primary form_submit_button',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

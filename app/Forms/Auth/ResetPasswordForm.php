<?php

namespace App\Forms\Auth;

use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class ResetPasswordForm extends Form
{
    public function buildForm()
    {
        $this
            ->setFormOptions([
                'id' => 'reset-password-form',
                'url' => route('password.update'),
                'method' => 'POST',
                'novalidate' => 'novalidate',
            ])
            ->add('token', Field::HIDDEN, [
                'value' => request('token'),
            ])
            ->add('email', Field::EMAIL, [
                'label' => 'Your email address',
                'rules' => 'required|email|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('password', Field::PASSWORD, [
                'rules' => 'required|string|min:8|confirmed',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('password_confirmation', Field::PASSWORD, [
                'rules' => 'required|string|min:8',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Register',
                'wrapper' => [
                    'class' => 'col-lg-4',
                ],
                'attr' => [
                    'class' => 'btn btn-primary form_submit_button',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

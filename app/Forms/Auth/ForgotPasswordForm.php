<?php

namespace App\Forms\Auth;

use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class ForgotPasswordForm extends Form
{
    public function buildForm()
    {
        $this
            ->setFormOptions([
                'id' => 'forgot-password-form',
                'method' => 'POST',
                'novalidate' => 'novalidate',
            ])
            ->add('email', Field::EMAIL, [
                'rules' => ['required', 'email', 'max:255'],
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Sign in',
                'wrapper' => [
                    'class' => 'col-lg-4'
                ],
                'attr' => [
                    'class' => 'btn btn-primary form_submit_button',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

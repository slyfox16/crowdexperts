<?php

namespace App\Forms\Auth;

use App\Forms\Validations\GoogleReCaptchaV3ValidationRule;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class RegisterForm extends Form
{
    public function buildForm()
    {
        $this
            ->setFormOptions([
                'id' => 'register-form',
                'method' => 'POST',
                'novalidate' => 'novalidate',
            ])
            ->add('title', Field::TEXT, [
                'label' => __('frontend.title'),
                'rules' => 'sometimes|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('name', Field::TEXT, [
                'rules' => 'required|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('email', Field::EMAIL, [
                'label' => 'Email address',
                'rules' => 'required|email|max:255|unique:users',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('country_id', 'select2', [
                'rules' => 'nullable|exists:countries,id',
                'widgetOptions' => [
                    'dataType' => 'json',
                    'width' => '100%',
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'ajax' => [
                        'url' => route('cabinet.suggestCountry'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('countryFormatResult'),
                    'templateSelection' => new Expression('countryFormatSelection'),
                ],
                'label' => __('frontend.country'),
            ])
            ->add('city_id', 'select2', [
                'rules' => 'nullable|exists:cities,id',
                'widgetOptions' => [
                    'dataType' => 'json',
                    'width' => '100%',
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'ajax' => [
                        'url' => route('cabinet.suggestCity'),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new Expression('function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1
                            };
                        }'),
                        'cache' => true,
                    ],
                    'templateResult' => new Expression('cityFormatResult'),
                    'templateSelection' => new Expression('cityFormatSelection'),
                ],
                'label' => __('frontend.city'),
            ])
            ->add('address', Field::TEXT, [
                'label' => __('frontend.address'),
                'rules' => 'nullable|string|max:255',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('password', Field::PASSWORD, [
                'rules' => 'required|string|min:8|confirmed',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('password_confirmation', Field::PASSWORD, [
                'rules' => 'required|string|min:8',
                'wrapper' => [
                    'class' => 'form-group',
                ],
            ])
            ->add('accept', 'checkbox', [
                'label' => __('frontend.accept_conditions', [':url' => '#']),
                'value' => 1,
                'checked' => true,
                'rules' => ['required', new GoogleReCaptchaV3ValidationRule('default')],
                'attr' => [
                    'id' => 'customCheck1',
                    'class' => 'custom-control-input',
                ],
            ])
            ->add('send', Field::BUTTON_SUBMIT, [
                'label' => 'Register',
                'wrapper' => [
                    'class' => 'col-lg-4',
                ],
                'attr' => [
                    'class' => 'btn btn-primary form_submit_button',
                    'loadingText' => 'Loading...',
                ],
            ]);
    }
}

<?php

namespace App\Console\Commands;

use App\Models\CompletedProjects;
use App\Models\User;
use DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class GetDadaFromOldBase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:old-base-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = DB::connection('mysql2')->table('user')->get();

        if (!$users) {
            return false;
        }

        foreach ($users as $user) {
//            $countryID = DB::table('countries')->where('iso', $user->country_id)->value('id');

//            $newUser = User::create([
//                'title' => $user->title,
//                'name' => $user->name . ' ' . $user->surname,
//                'email' => $user->email,
//                'username' => Str::slug($user->name . ' ' . $user->surname),
//                'phone' => $user->phone,
//                'country_id' => $countryID,
//                'city_id' => $user->city_id,
//                'address' => $user->address,
//                'companyname' => $user->companyname,
//                'position' => $user->position,
//                'description' => $user->description,
//                'facebook_url' => $user->facebook_url,
//                'linkedin_url' => $user->linkedin_url,
//                'twitter_url' => $user->twitter_url,
//                'xing_url' => $user->xing_url,
//                'web_url' => $user->web_url,
//                'expert_confirm' => $user->expert_confirm,
//                'is_active' => $user->is_active,
//                'is_staff' => $user->is_staff,
//                'password' => bcrypt('password'),
//            ]);

//            if ($user->avatar) {
//                $avatar = $this->saveImage($newUser->id, storage_path('app/public/users/' . $user->avatar), 'user');
//                $newUser->update([
//                    'avatar' => $avatar
//                ]);
//            }

//            $this->setAssoc($user->id, $newUser->id);
//            $this->setCertificate($user->id, $newUser->id);

            $newUser = User::where('email', $user->email)->first();
//            $this->setSpeciality($user->id, $newUser->id);
            $this->setCompletedProjects($user->id, $newUser->id);
        }
    }

    public function setCompletedProjects($userID, $newUserID)
    {
        $completedProjects = DB::connection('mysql2')->table('completed_projects')
            ->where('user_id', $userID)
            ->get();

        if (!$completedProjects) {
            return false;
        }

        foreach ($completedProjects as $completedProject) {
            $image = null;
            if ($completedProject->image) {
                $image = $this->saveImage($newUserID, storage_path('app/public/users/' . $completedProject->image), 'completedprojects');
            }

            $newUser = CompletedProjects::create([
                'user_id' => $newUserID,
                'name' => $completedProject->name,
                'image' => null,
                'description' => $completedProject->description,
                'date' => $completedProject->date,
                'confirm' => $completedProject->confirm,
                'link' => $completedProject->link,
            ]);

            $newUser->update([
                'image' => $image
            ]);
        }
    }

    public function setSpeciality($userID, $newUserID)
    {
        $userSpecialities = DB::connection('mysql2')
            ->table('user_speciality')
            ->where('user_id', $userID)
            ->get();

        if (!$userSpecialities) {
            return false;
        }

        foreach ($userSpecialities as $userSpeciality) {
            DB::table('user_speciality')
                ->insert([
                    'user_id' => $newUserID,
                    'speciality_id' => $userSpeciality->speciality_id
                ]);
        }
    }

    public function setCertificate($userID, $newUserID)
    {
        $userCertificates = DB::connection('mysql2')->table('user_certificate')
            ->where('user_id', $userID)
            ->get();

        if (!$userCertificates) {
            return false;
        }

        foreach ($userCertificates as $userCertificate) {
            DB::table('user_certificate')
                ->insert([
                    'user_id' => $newUserID,
                    'certificate_id' => $userCertificate->certificate_id,
                    'confirm' => $userCertificate->confirm,
                    'date' => $userCertificate->date,
                ]);
        }
    }

    public function setAssoc($userID, $newUserID)
    {
        $userAssocs = DB::connection('mysql2')->table('user_association')
            ->where('user_id', $userID)
            ->get();

        if (!$userAssocs) {
            return false;
        }

        foreach ($userAssocs as $userAssoc) {
            DB::table('user_association')
                ->insert([
                    'user_id' => $newUserID,
                    'association_id' => $userAssoc->association_id
                ]);
        }
    }

    public function saveImage($userID, $fileName, $dir)
    {
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        $new_filename = Str::slug('image_' . time() . '_' . Str::random(10)) . '.' . $extension;
        $path = 'app/public/' . $dir . '/' . $userID . '/' . $new_filename;


            @mkdir(storage_path('app/public/' . $dir . '/' . $userID));


        if (File::exists($fileName) && File::copy($fileName, storage_path($path))) {
            return $dir . '/' . $userID . '/' . $new_filename;
        }
    }
}

<?php

namespace App\Services;

use Alexwijn\Select2\Engines\QueryEngine;
use Illuminate\Http\JsonResponse;

/**
 * Alexwijn\Select2\QueryDropDown
 */
class MyQueryEngine extends QueryEngine
{
    /**
     * Render json response.
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function render(array $data): JsonResponse
    {
        $output = [
            'results' => $data,
            'pagination' => [
                'more' => false,
            ],
        ];

        if ($this->jsonHeaders === null) {
            $this->jsonHeaders = config('select2.json.headers', []);
        }

        if ($this->jsonOptions === null) {
            $this->jsonOptions = config('select2.json.options', 0);
        }

        return new JsonResponse(
            $output,
            200,
            $this->jsonHeaders,
            $this->jsonOptions
        );
    }
}

<?php

namespace App\Providers;

use App\Extensions\SocialShare\Share;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('share', function () {
            return new Share();
        });

        Blade::directive('script', function () {
            return '<?php ob_start(); ?>';
        });

        Blade::directive('endscript', function ($class) {
            return "<?php \App\Extensions\Asset::script(ob_get_clean()); ?>";
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

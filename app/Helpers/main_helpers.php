<?php

use App\Models\amigo\Setting;

if (!function_exists('schema_asset')) {

    /**
     * @param $path
     *
     * @return string
     */
    function schema_asset($path)
    {
        return config('app.https') ? secure_asset($path) : asset($path);
    }
}

if (!function_exists('array_set_value')) {

    /**
     * @param array $array
     * @param array|string $parents
     * @param mixed $value
     * @param string $glue
     */
    function array_set_value(array &$array, $parents, $value, $glue = '.')
    {
        if (!is_array($parents)) {
            $parents = explode($glue, (string)$parents);
        }

        $ref = &$array;

        foreach ($parents as $parent) {
            if (isset($ref) && !is_array($ref)) {
                $ref = [];
            }

            $ref = &$ref[$parent];
        }

        $ref = $value;
    }
}

if (!function_exists('array_get_value')) {

    /**
     * @param array $array
     * @param array|string $parents
     * @param string $glue
     * @return mixed
     */
    function array_get_value(array &$array, $parents, $glue = '.')
    {
        if (!is_array($parents)) {
            $parents = explode($glue, $parents);
        }

        $ref = &$array;

        foreach ((array) $parents as $parent) {
            if (is_array($ref) && array_key_exists($parent, $ref)) {
                $ref = &$ref[$parent];
            } else {
                return null;
            }
        }
        return $ref;
    }
}

if (!function_exists('array_unset_value')) {

    /**
     * @param array $array
     * @param array|string $parents
     * @param string $glue
     */
    function array_unset_value(&$array, $parents, $glue = '.')
    {
        if (!is_array($parents)) {
            $parents = explode($glue, $parents);
        }

        $key = array_shift($parents);

        if (empty($parents)) {
            unset($array[$key]);
        } else {
            array_unset_value($array[$key], $parents);
        }
    }
}

if (!function_exists('prep_url')) {
    /**
     * Prep URL
     *
     * Simply adds the http:// part if no scheme is included
     *
     * @param   string  the URL
     * @return  string
     */
    function prep_url($str = '')
    {
        if ($str === 'http://' OR $str === '')
        {
            return '';
        }
        $url = parse_url($str);
        if ( ! $url OR ! isset($url['scheme']))
        {
            return 'http://'.$str;
        }
        return $str;
    }
}

if (!function_exists('get_eloquent_sql_with_bindings')) {
    /**
     * Combines SQL and its bindings
     *
     * @param \Eloquent $query
     * @return string
     */
    function get_eloquent_sql_with_bindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
}

if (!function_exists('nl2p')) {
    function nl2p($string)
    {
        $paragraphs = '';

        foreach (explode("\n", $string) as $line) {
            if (trim($line)) {
                $paragraphs .= '<p>' . $line . '</p>';
            }
        }

        return $paragraphs;
    }
}

if (!function_exists('currentPage')) {
    function currentPage($urlName)
    {
        return Route::currentRouteName() == $urlName;
    }
}

if (!function_exists('getBetweenTags')) {
    function getBetweenTags($tag, $string)
    {
        preg_match('~<' . $tag . '>(.*)</' . $tag . '>~i', $string, $match);
        return data_get($match, 1);
    }
}

if (!function_exists('br2tag')) {

    /**
     * @param $items
     */
    function br2tag($items, $tag)
    {
        $retString = [];
        $items = preg_split("/(\r\n|\n|\r)/", $items);

        if ($items) {
            foreach ($items as $item) {
                $retString[] = Html::tag($tag, trim($item))->toHtml();
            }
        }

        return implode('', $retString);
    }
}

if (!function_exists('settingGetFile')) {

    /**
     * @param $setting
     * @return string
     */
    function settingGetFile($setting)
    {
        $setting = Setting::where('key', $setting)->first();

        if (!$setting) {
            return '#';
        }

        if ($setting->type == Setting::IMAGE) {
            return Storage::disk(config('amigo.storage.disk'))->url($setting->value);
        }

        $file = $setting->getFile('value');

        if ($file) {
            return schema_asset($file);
        }
    }
}

if (!function_exists('array_wrap')) {

    /**
     * @param $string
     * @return array
     */
    function array_wrap($string)
    {
        return Arr::wrap($string);
    }
}

<?php

namespace App\Widgets;

use Illuminate\Database\Query\Expression;
use Illuminate\Support\Str;

class Table extends Widget
{
    private $_id = [];
    private $_settings = [];

    /**
     * @var array Html options for table
     */
    private $_tableOptions = [];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->initColumns();
        $this->_settings = $this->getDefaultSettings();
        $this->parseParams();

        $this->_settings = array_merge($this->_settings, $this->getAttributes());

        \Asset::setParams($this->_settings);
        \Asset::js( mix('js/datatable.js') );
        \Asset::script('DataTableConstruct("' . $this->_id .'", ' . \Asset::getParams() . ');');
    }

    protected function parseParams()
    {
        $this->_settings['ajax']['url'] = $this->get('url');
        $this->offsetUnset('url');

        $this->_tableOptions = $this->get('tableOptions');
        foreach ($this->_tableOptions as $key => $value) {
            if (is_array($value)) {
                $this->_tableOptions[$key] = json_encode($value);
            }
        }
        $this->offsetUnset('tableOptions');

        $this->_id = $this->_tableOptions['id'] ?? $this->getId();
        $this->_tableOptions = array_merge($this->_tableOptions, ['id' => $this->_id]);
    }

    protected function initColumns()
    {
        if (isset($this->attributes['columns'])) {
            foreach ($this->attributes['columns'] as $key => $value) {
                if (is_string($value)) {
                    $this->attributes['columns'][$key] = ['data' => $value, 'title' => Str::camel($value)];
                }
            }
        }
    }

    /**
     * Render the table.
     *
     * @return string
     */
    public function render()
    {
        echo \Html::tag('table', [], $this->_tableOptions);
    }

    private function getDefaultSettings() {
        return [
            'dom'           => "<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'p><'col-sm-12 col-md-7 dataTables_pager'li>>",
            'responsive'    => !0,
            'searching'     => !0,
            'ordering'      => !0,
            'order'         => [],
            'bInfo'         => !0,
            'info'          => !0,
            'paging'        => !0,
            'processing'    => !0,
            'serverSide'    => !0,
            'lengthMenu'    => [5, 10, 25, 50, 100],
            'pageLength'    => 20,
            'pagingType'    => "full_numbers",
            'rowReorder'    => false,
            'language'      => [
                "lengthMenu"    => "Display _MENU_",
                "zeroRecords"   => "Nothing found - sorry",
                "info"          => "Showing page _PAGE_ of _PAGES_",
                "infoEmpty"     => "No records available",
                "emptyTable"    => "No data available in table",
                "infoFiltered"  => "(filtered from _MAX_ total records)",
                "processing"    => '
                <div class="processing-wrapper">
                    <div class="blockui">
                        <span>Please wait...</span>
                        <span>
                            <div class="kt-spinner kt-spinner--brand"></div>
                        </span>
                    </div>
                </div>',
            ],
            'ajax' => [
                'url'       => '',
                'method'    => 'POST',
                'data'      => new Expression('function (d) {
                    d.page = (d.start !== 0) ? ((d.start / d.length) + 1) : 1;
                    if (d.order.length > 0) {
                        d.sort = {
                            field: d.columns[d.order[0].column].data,
                            sort: d.order[0].dir
                        };
                    }

                    if (events) {
                        try {
                            d.searchQuery = new Object();
                            $.each(events, function (eventType, inputs) {
                                $.each(inputs, function (key, id) {
                                    let val = $(\'#\' + id).val();
                                    if (val.length > 0) {
                                        d.searchQuery[id] = val;
                                    }
                                });
                            });
                        } catch (e) {
                            console.log(e.toString());
                        }
                    }
                }')
            ]
        ];
    }
}

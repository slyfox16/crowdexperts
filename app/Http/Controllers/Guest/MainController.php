<?php

namespace App\Http\Controllers\Guest;

use App\Forms\Cabinet\MessageForm;
use App\Forms\Cabinet\MessageUnregisteredForm;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\ContactMassage;
use App\Notifications\ContactMassageUnregistered;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Notification;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class MainController extends Controller
{
    use FormBuilderTrait;

    public function index()
    {
        $users = User::active()->get();
        return view('guest.main.index', compact('users'));
    }

    public function contact(Request $request)
    {
        $messageForm = $this->form(MessageUnregisteredForm::class, [
            'id' => 'message-form',
            'url' => route('messageUnregisteredSend'),
            'novalidate' => 'novalidate',
            'attr' => ['class' => "row gutter-3"]
        ]);

        return view('guest.main.contact', compact('messageForm'));
    }

    public function messageUnregisteredSend(Request $request)
    {
        $form = $this->form(MessageUnregisteredForm::class);

        if (!$form->isValid()) {
            return response()->json([
                'error' => $form->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        Notification::route('mail', setting('site.contact_email'))
            ->notify(new ContactMassageUnregistered());

        return response()->json([
            'message' => __('frontend.message_send')
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function messageSend(Request $request)
    {
        $form = $this->form(MessageForm::class);

        if (!$form->isValid()) {
            return response()->json([
                'error' => $form->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        Notification::route('mail', setting('site.contact_email'))
            ->notify(new ContactMassage());

        return response()->json([
            'message' => __('frontend.message_send')
        ]);
    }

    /**
     * @return BinaryFileResponse
     */
    public function getAgreement()
    {
        $path = 'app/public/Beitrittserklärung_Mitglieder_DCV_2019.pdf';

        return response()->download(storage_path($path))->deleteFileAfterSend(false);
    }
}

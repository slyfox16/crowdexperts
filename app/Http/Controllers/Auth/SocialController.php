<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Contracts\User as SocialUser;

class SocialController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function facebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function facebookRedirect()
    {
        $user = Socialite::driver('facebook')->user();
        $dbUser = $this->loginUser($user);

        Auth::login($dbUser, true);

        return redirect('/');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function linkedin()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function linkedinRedirect()
    {
        $user = Socialite::driver('linkedin')->user();
        $dbUser = $this->loginUser($user);

        Auth::login($dbUser, true);

        return redirect('/');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function xing()
    {
        return Socialite::driver('xing')->redirect();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function xingRedirect()
    {
        $user = Socialite::driver('xing')->user();
        $dbUser = $this->loginUser($user);

        Auth::login($dbUser, true);

        return redirect('/');
    }

    /**
     * @param SocialUser $user
     *
     * @return User
     */
    private function loginUser(SocialUser $user): User
    {
        $dbUser = User::where('email', $user->email)->first();

        if (!$dbUser) {
            $dbUser = User::create([
                'email' => $user->email,
                'name' => $user->name,
                'address' => '',
                'password' => bcrypt(Str::random(24))
            ]);

            $avatar = $this->saveAvatar($dbUser, $user->avatar);
            $dbUser->update([
                'avatar' => $avatar
            ]);
        }

        return $dbUser;
    }

    private function saveAvatar(User $user, $avatar = null)
    {
        $image = file_get_contents($avatar);
        $filename = Str::slug('image_' . time() . '_' . Str::random(10)) . '.jpg';
        $path = storage_path('app/' . $user->getPath('avatar'));

        if (!is_dir($path)) {
            mkdir($path);
        }

        file_put_contents($path . $filename, $image);

        return str_replace('public/', '', $user->getPath('avatar')) . $filename;
    }
}

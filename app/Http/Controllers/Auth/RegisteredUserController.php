<?php

namespace App\Http\Controllers\Auth;

use App\Forms\Auth\RegisterForm;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class RegisteredUserController extends Controller
{
    use FormBuilderTrait;

    /**
     * Display the registration view.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $formRegister = $this->form(RegisterForm::class);
        return view('auth.register', compact('formRegister'));
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function store(Request $request)
    {
        $form = $this->form(RegisterForm::class);

        if (!$form->isValid()) {
            return response()->json([
                'error' => $form->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        Auth::login($user = User::create([
            'title' => $request->title,
            'name' => $request->name,
            'email' => $request->email,
            'country_id' => $request->country_id,
            'city_id' => $request->city_id,
            'address' => $request->address,
            'password' => Hash::make($request->password),
        ]));

        event(new Registered($user));

        return response()->json([
            'redirect' => RouteServiceProvider::HOME
        ]);
    }
}

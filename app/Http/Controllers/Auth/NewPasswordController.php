<?php

namespace App\Http\Controllers\Auth;

use App\Forms\Auth\ResetPasswordForm;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class NewPasswordController extends Controller
{
    use FormBuilderTrait;

    /**
     * Display the password reset view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        $resetPassword = $this->form(ResetPasswordForm::class);
        return view('auth.reset-password', ['resetPassword' => $resetPassword]);
    }

    /**
     * Handle an incoming new password request.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $form = $this->form(ResetPasswordForm::class);

        if (!$form->isValid()) {
            return response()->json([
                'error' => $form->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($request->password),
                    'remember_token' => Str::random(60),
                ])->save();

                event(new PasswordReset($user));
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $status == Password::PASSWORD_RESET
                ? response()->json([
                    'message' => __($status)
                ])
                : response()->json([
                    'error' => ['email' => __($status)],
                ], 400, [], JSON_UNESCAPED_UNICODE);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Forms\Auth\ForgotPasswordForm;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class PasswordResetLinkController extends Controller
{
    use FormBuilderTrait;

    /**
     * Display the password reset link request view.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $resetLogin = $this->form(ForgotPasswordForm::class);
        return view('auth.forgot-password', compact('resetLogin'));
    }

    /**
     * Handle an incoming password reset link request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $form = $this->form(ForgotPasswordForm::class);

        if (!$form->isValid()) {
            return response()->json([
                'error' => $form->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status == Password::RESET_LINK_SENT
                    ? response()->json([
                        'message' => __($status)
                      ])
                    : response()->json([
                        'error' => ['email' => __($status)],
                      ], 400, [], JSON_UNESCAPED_UNICODE);
    }
}

<?php

namespace App\Http\Controllers\Cabinet;

use Alexwijn\Select2\Engines\QueryEngine;
use App\Events\Message;
use App\Forms\Cabinet\AvatarForm;
use App\Forms\Cabinet\CabinetForm;
use App\Forms\Cabinet\CertificatesForm;
use App\Forms\Cabinet\CompletedProjectsForm;
use App\Forms\Cabinet\ContactsForm;
use App\Forms\Cabinet\MessageForm;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cabinet\CabinetRequest;
use App\Models\AssociationMembership;
use App\Models\Session;
use App\Models\User;
use Html;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class MainController extends Controller
{
    use FormBuilderTrait;

    public function impressum()
    {
        return view('frontend.main.impressum');
    }

    /**
     * @param User|null $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showProfile(User $user = null)
    {
        if (!$user) {
            $user = Auth::user();
        }

        $messageForm = $this->form(MessageForm::class, [
            'id' => 'message-form',
            'url' => route('cabinet.messageSend'),
            'novalidate' => 'novalidate',
        ]);

        $showInfo = $user->specialities->isNotEmpty() || $user->associations->isNotEmpty() || $user->events->isNotEmpty() || $user->projects->isNotEmpty();
        return view('cabinet.main.profile', compact('user', 'messageForm', 'showInfo'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function messageSend(Request $request)
    {
        $user = User::where('username', $request->input('user_id'))->first();

        if (!$user) {
            abort(403, 'No such user');
        }

        $session = Session::where(function($q) use ($user) {
                $q->where('user1_id', Auth::id())
                    ->where('user2_id', $user->id);
            })
            ->orWhere(function($q) use ($user) {
                $q->where('user1_id', $user->id)
                    ->where('user2_id', Auth::id());
            })
            ->first();

        if (!$session) {
            $session = Session::create([
                'user1_id' => Auth::id(),
                'user2_id' => $user->id
            ]);
        }

        $message = $session->messages()->create([
            'content' => $request->input('message')
        ]);

        if ($message) {
            $chat = $message->createForSend($session->id);
            $message->createForReceive($session->id, $user->id);
        }

        Message::dispatch($message->content, $chat);

        return response()->json($chat->id);
    }

    public function index()
    {
        $user = Auth::user();
        $cabinetForm = $this->form(CabinetForm::class, [
            'id' => 'user-info-form',
            'method' => 'POST',
            'url' => route('cabinet.main'),
            'novalidate' => 'novalidate',
            'model' => $user,
        ]);
        $contactsForm = $this->form(ContactsForm::class, [
            'id' => 'contacts-form',
            'method' => 'POST',
            'url' => route('cabinet.contactStore'),
            'novalidate' => 'novalidate',
            'model' => $user,
        ]);
        $certificatesForm = $this->form(CertificatesForm::class, [
            'id' => 'certificates-form',
            'method' => 'POST',
            'url' => route('cabinet.certificateStore'),
            'novalidate' => 'novalidate',
        ]);
        $completedProjectsForm = $this->form(CompletedProjectsForm::class, [
            'id' => 'completed-projects-form',
            'method' => 'POST',
            'url' => route('cabinet.projectStore'),
            'novalidate' => 'novalidate',
        ]);
        $avatarForm = $this->form(AvatarForm::class);
        $associations = $user->associations()->get();
        $certificates = $user->certificates()->get();
        $projects = $user->projects()->get();

        return view('cabinet.main.index', compact(
            'cabinetForm',
            'associations',
            'user',
            'avatarForm',
            'contactsForm',
            'certificatesForm',
            'certificates',
            'completedProjectsForm',
            'projects'
        ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cabinetStore(CabinetRequest $request)
    {
        $cabinetForm = $this->form(CabinetForm::class);

        if (!$cabinetForm->isValid()) {
            return response()->json([
                'error' => $cabinetForm->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        Auth::user()->update($request->except(['_token', 'association_membership', 'specialities']));

        Auth::user()
            ->specialities()
            ->sync($request->input('specialities'));

        return response()->json([
            'message' => __('frontend.success'),
            'name' => Auth::user()->name
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function avatarStore(Request $request)
    {
        $form = $this->form(AvatarForm::class);

        if (!$form->isValid()) {
            return response()->json([
                'error' => $form->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        Auth::user()->update($request->all());

        return response()->json([
            'message' => __('frontend.success'),
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function suggestSpecialities()
    {
        $builder = DB::table('speciality');

        $query = new QueryEngine($builder);
        $query->search('speciality');
        $queryEngine = $query->make();

        return $queryEngine;
    }

    public function addAssociationMembership(Request $request)
    {
        $result = Auth::user()
            ->associations()
            ->syncWithoutDetaching(array_wrap($request->input('association_membership')));

        if (data_get($result, 'attached')) {
            $associationMembership = AssociationMembership::find($request->input('association_membership'));
            return Html::tag('li', $associationMembership->name . Html::tag('a', '', [
                    'id' => 'assoc' . $associationMembership->id,
                    'href' => '',
                    'class' => 'icon-x assoc-delete',
                    'data-associd' => $associationMembership->id,
                ]));
        }

        return response()->json([
            'error' => 'Return without saving',
        ], 400, [], JSON_UNESCAPED_UNICODE);
    }

    public function deleteAssociationMembership(Request $request)
    {
        if ($request->input('assocID')) {
            $result = Auth::user()
                ->associations()
                ->detach($request->input('assocID'));

            if ($result) {
                return response()->json([
                    'deleted' => $request->input('assocID'),
                ]);
            }
        }

        return response()->json([
            'error' => 'Return without deleting',
        ], 400, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param User|null $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToChat(User $user)
    {
        $chatFriendsId = $user->userInFriends();

        if (!$chatFriendsId) {
            $user->addUserToFriends();

            return response()->json([
                'message' => 'User added',
                'button' => 'Remove from chat'
            ]);
        }
    }

    public function removeChat(User $user)
    {
        DB::table('chat_friends')
            ->where('first_user_id', Auth::id())
            ->where('second_user_id', $user->id)
            ->delete();

        return response()->json([
            'message' => 'Chat removed',
            'button' => 'Add to chat'
        ]);
    }

}

<?php

namespace App\Http\Controllers\Cabinet;

use App\Forms\Cabinet\EventForm;
use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Mapper;

class EventController extends Controller
{
    use FormBuilderTrait;

    public function index()
    {
        $eventForm = $this->form(EventForm::class, [
            'id' => 'event-form',
            'method' => 'POST',
            'url' => route('cabinet.events.store'),
            'novalidate' => 'novalidate',
        ]);

        $events = Event::active()->get();
        return view('cabinet.events.index', compact('eventForm', 'events'));
    }

    public function show(Event $event)
    {
        $address = urlencode($event->getCountryCity() . ', ' . $event->address);
        return view('cabinet.events.show', compact('event', 'address'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $eventForm = $this->form(EventForm::class);

        if (!$eventForm->isValid()) {
            return response()->json([
                'error' => $eventForm->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        Event::create($request->except(['_token', 'daterange', 'image']));

        return response()->json([
            'message' => __('frontend.success'),
            'name' => Auth::user()->name
        ]);
    }
}

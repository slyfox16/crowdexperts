<?php

namespace App\Http\Controllers\Cabinet;

use App\Forms\Cabinet\CertificatesForm;
use App\Http\Controllers\Controller;
use App\Models\Certificate;
use Html;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class CertificateController extends Controller
{
    use FormBuilderTrait;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Support\HtmlString
     */
    public function certificateStore(Request $request)
    {
        $cert = null;
        $contactsForm = $this->form(CertificatesForm::class);

        if (!$contactsForm->isValid()) {
            return response()->json([
                'error' => $contactsForm->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        $certificateID = $request->input('certificate_id');
        $result = Auth::user()
            ->certificates()
            ->syncWithoutDetaching([$certificateID => ['date' => $request->input('date')]]);

        if (data_get($result, 'attached')) {
            $certificate = Certificate::find($request->input('certificate_id'));
            $cert = Html::tag('li', $certificate->name . Html::tag('a', '', [
                    'id' => 'cert' . $certificate->id,
                    'href' => '',
                    'class' => 'icon-x cert-delete',
                    'data-certid' => $certificate->id,
                ]))->toHtml();
        }

        return response()->json([
            'message' => __('frontend.success'),
            'cert' => $cert
        ]);
    }

    public function deleteCertificate(Request $request)
    {
        if ($request->input('certID')) {
            $result = Auth::user()
                ->certificates()
                ->detach($request->input('certID'));

            if ($result) {
                return response()->json([
                    'deleted' => $request->input('certID'),
                ]);
            }
        }

        return response()->json([
            'error' => 'Return without deleting',
        ], 400, [], JSON_UNESCAPED_UNICODE);
    }
}

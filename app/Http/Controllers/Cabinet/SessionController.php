<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Auth;

class SessionController extends Controller
{
    /**
     * Create session.
     */
    public function sessionCreate()
    {
        return UserResource::collection(User::where('id', '!=', Auth::id())->get());
    }
}

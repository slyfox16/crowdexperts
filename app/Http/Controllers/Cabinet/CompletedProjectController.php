<?php

namespace App\Http\Controllers\Cabinet;

use App\Forms\Cabinet\CompletedProjectsForm;
use App\Http\Controllers\Controller;
use App\Models\CompletedProjects;
use Html;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class CompletedProjectController extends Controller
{
    use FormBuilderTrait;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Support\HtmlString
     */
    public function projectStore(Request $request)
    {
        $cert = null;
        $contactsForm = $this->form(CompletedProjectsForm::class);

        if (!$contactsForm->isValid()) {
            return response()->json([
                'error' => $contactsForm->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        $project = Auth::user()->projects()->create($request->except(['_token']));

        if ($project) {
            $cert = Html::tag('li', $project->name . Html::tag('a', '', [
                    'id' => 'project' . $project->id,
                    'href' => '',
                    'class' => 'icon-x project-delete',
                    'data-projectid' => $project->id,
                ]))->toHtml();
        }

        return response()->json([
            'message' => __('frontend.success'),
            'cert' => $cert
        ]);
    }

    public function deleteProject(Request $request)
    {
        if ($request->input('projectID')) {

            $result = CompletedProjects::where(['id' => $request->input('projectID')])->delete();

            if ($result) {
                return response()->json([
                    'deleted' => $request->input('projectID'),
                ]);
            }
        }

        return response()->json([
            'error' => 'Return without deleting',
        ], 400, [], JSON_UNESCAPED_UNICODE);
    }
}

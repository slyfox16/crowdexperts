<?php

namespace App\Http\Controllers\Cabinet;

use App\Forms\Cabinet\ContactsForm;
use App\Http\Controllers\Controller;
use App\Services\MyQueryEngine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ContactController extends Controller
{
    use FormBuilderTrait;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function suggestCountry()
    {
        $builder = DB::table('countries');
        $query = new MyQueryEngine($builder);

        return $query->search('namede')
            ->value('id')
            ->make();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function suggestCity()
    {
        $builder = DB::table('cities');
        $query = new MyQueryEngine($builder);

        return $query->search('ascii')
            ->value('id')
            ->make();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contactStore(Request $request)
    {
        $contactsForm = $this->form(ContactsForm::class);

        if (!$contactsForm->isValid()) {
            return response()->json([
                'error' => $contactsForm->getErrors(),
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        Auth::user()->update($request->except(['_token']));

        return response()->json([
            'message' => __('frontend.success'),
            'name' => Auth::user()->name
        ]);
    }
}

<?php

namespace App\Http\Controllers\Cabinet;

use App\Events\Block;
use App\Events\Message;
use App\Events\MessageRead;
use App\Events\SessionEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\ChatResource;
use App\Http\Resources\SessionResource;
use App\Http\Resources\UserResource;
use App\Models\Chat;
use App\Models\Session;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function index()
    {
        return view('cabinet.chat.index');
    }

    /**
     * Get chet friends.
     */
    public function getFriends()
    {
        $newCollection = collect();
        $userFriendsIDs = User::getChatFriends();

        if (!$userFriendsIDs) {
            return [];
        }

        foreach (array_chunk($userFriendsIDs, 100) as $arr_chunk) {
            $newCollection = $newCollection->merge(User::whereIn('id', $arr_chunk)->get());
        }

        return UserResource::collection($newCollection);
    }

    /**
     * Get chet friends.
     * @param Request $request
     * @param Session $session
     */
    public function send(Request $request, Session $session)
    {
        $message = $session->messages()->create([
            'content' => $request->input('message')
        ]);

        if ($message) {
            $chat = $message->createForSend($session->id);
            $message->createForReceive($session->id, $request->to_user);
        }

        Message::dispatch($message->content, $chat);

        return response()->json($chat->id);
    }

    public function chats(Request $request, Session $session)
    {
        $chats = $session->chats()->with('message')->where('user_id', Auth::id())->get();
        return ChatResource::collection($chats);
    }

    /**
     * Create session.
     * @param Request $request
     * @return SessionResource
     */
    public function sessionCreate(Request $request)
    {
        $session = Session::firstOrCreate([
            'user1_id' => Auth::id(),
            'user2_id' => $request->input('friend_id')
        ]);

        $sessionResource = new SessionResource($session);
        SessionEvent::dispatch($sessionResource, Auth::id());
        return $sessionResource;
    }

    /**
     * Read messages.
     * @param Request $request
     * @param Session $session
     */
    public function read(Session $session)
    {
        $chats = $session->chats
            ->whereNull('read_at')
            ->where('type', Chat::SEND)
            ->where('user_id', '!=', Auth::id());

        foreach ($chats as $chat) {
            $chat->update([
                'read_at' => Carbon::now()
            ]);

            MessageRead::dispatch(new ChatResource($chat), $chat->session_id);
        }
    }

    /**
     * Clear all messages.
     * @param Session $session
     * @return \Illuminate\Http\JsonResponse
     */
    public function clear(Session $session)
    {
        $session->deleteChats();

        if (!$session->chats->count()) {
            $session->deleteMessages();
        }

        return response()->json('cleared');
    }

    public function block(Session $session)
    {
        $session->block();
        Block::dispatch($session->id, true);
        return response(null, 201);
    }

    public function unblock(Session $session)
    {
        $session->unblock();
        Block::dispatch($session->id, false);
        return response(null, 201);
    }
}

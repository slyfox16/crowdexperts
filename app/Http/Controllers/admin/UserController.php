<?php

namespace App\Http\Controllers\admin;

use Amigo\Cms\Http\Controllers\Controller as BaseController;
use App\Extensions\DataTable\DataTable;
use App\Forms\Admin\UserForm;
use App\Models\User;
use App\Services\MyQueryEngine;
use App\TableSearch\Users\UsersSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class UserController extends BaseController
{
    use DataTable, FormBuilderTrait;

    public function index()
    {
        return view ('admin.users.index');
    }

    /**
     * Get Data for DataTable
     *
     * @param Request $request
     *
     * @return array
     * @throws \Throwable
     */
    public function list(Request $request)
    {
        $listData = $this->getDataForTable(UsersSearch::apply(User::query()), $request);

        $result = [];
        /** @var User $user */
        foreach ($listData['data'] as $user) {
            $result[] = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'avatar' => $user->getAvatar(),
                'username' => $user->username,
                'expert_confirm' => $user->getConfirmStatus(),
                'active' => $user->getActive(),
                'date_joined' => $user->created_at->format('d-m-Y'),
                'actions' => view('admin.users.actions', compact('user'))->render()
            ];
        }
        $listData['data'] = $result;
        $result = $listData;

        return $result;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $form = $this->form(UserForm::class, [
            'method' => 'POST',
            'url' => route('amigo.users.store'),
        ]);

        return view('admin.users.form', compact('form'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $form = $this->form(UserForm::class);
        $form->redirectIfNotValid();

        if ($model = User::create($request->all())) {
            if ($request->get('save') === 'redirect') {
                return redirect()->route('amigo.article-category.index')->with('success','Faq updated');
            } else {
                return redirect()->route('amigo.article-category.edit', $model->id)
                    ->with('success','Faq updated');
            }
        } else {
            return redirect()->route('amigo.article-category.index')->with('error','Looks like something went wrong');
        }
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(User $user)
    {
        $form = $this->form(UserForm::class, [
            'model' => $user,
            'enctype' => 'multipart/form-data',
            'url' => route('amigo.users.update', $user),
            'method' => 'PUT',
        ]);

        return view ('admin.users.form', compact('form'));
    }

    public function update(Request $request, User $user)
    {
        $userSaved = null;
        $form = $this->form(UserForm::class, [
            'model' => $user
        ]);
        $form->redirectIfNotValid();

        if ($user->update($request->except(['save', 'avatar']))) {
            if ($request->get('save') === 'redirect') {
                return redirect()->route('amigo.users.index')->with('success','User updated');
            } else {
                return redirect()->route('amigo.users.index', $user->id)
                    ->with('success','User updated');
            }
        } else {
            return redirect()->route('amigo.users.index',  $user->id)->with('error','Looks like something went wrong');
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function suggestCountry()
    {
        $builder = DB::table('countries');
        $query = new MyQueryEngine($builder);

        return $query->search('namede')
            ->value('id')
            ->make();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function suggestCity()
    {
        $builder = DB::table('cities');
        $query = new MyQueryEngine($builder);

        return $query->search('ascii')
            ->value('id')
            ->make();
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(['success' => true, 'message' => 'Team has been deleted']);
    }
}

<?php

namespace App\Http\Controllers\admin;

use Amigo\Cms\Http\Controllers\Controller as BaseController;
use App\Extensions\DataTable\DataTable;
use App\Forms\Admin\EventForm;
use App\Models\Event;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class EventController extends BaseController
{
    use DataTable, FormBuilderTrait;

    public function index()
    {
        return view ('admin.events.index');
    }

    /**
     * Get Data for DataTable
     *
     * @param Request $request
     *
     * @return array
     * @throws \Throwable
     */
    public function list(Request $request)
    {
        $listData = $this->getDataForTable(Event::query(), $request);

        $result = [];
        /** @var Event $admin */
        foreach ($listData['data'] as $event) {
            $result[] = [
                'id' => $event->id,
                'user' => $event->getUser(),
                'title' => $event->title,
                'date_start' => $event->date_start->format('d-m-Y'),
                'active' => $event->getActive(),
                'actions' => view('admin.events.actions', compact('event'))->render()
            ];
        }
        $listData['data'] = $result;
        $result = $listData;

        return $result;
    }

    /**
     * @param Event $event
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Event $event)
    {
        $form = $this->form(EventForm::class, [
            'model' => $event,
            'enctype' => 'multipart/form-data',
            'url' => route('amigo.events.update', $event),
            'method' => 'PUT',
        ]);

        return view ('admin.events.form', compact('form'));
    }

    public function update(Request $request, Event $event)
    {
        $form = $this->form(EventForm::class, [
            'model' => $event
        ]);
        $form->redirectIfNotValid();

        if ($event->update($request->except(['save', '_token', 'daterange', 'image']))) {
            if ($request->get('save') === 'redirect') {
                return redirect()->route('amigo.events.index')->with('success','User updated');
            } else {
                return redirect()->route('amigo.events.edit', $event->id)
                    ->with('success','User updated');
            }
        } else {
            return redirect()->route('amigo.events.index',  $event->id)->with('error','Looks like something went wrong');
        }
    }

    /**
     * @param Event $event
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Event $event)
    {
        $event->delete();

        return response()->json(['success' => true, 'message' => 'Team has been deleted']);
    }
}

<?php

namespace App\Http\Controllers\admin;

use Amigo\Cms\Http\Controllers\Controller as BaseController;
use App\Extensions\DataTable\DataTable;
use App\Forms\Admin\AdminForm;
use App\Forms\Admin\UserForm;
use App\Models\Admin;
use App\Models\User;
use App\Services\MyQueryEngine;
use App\TableSearch\Users\UsersSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class AdminController extends BaseController
{
    use DataTable, FormBuilderTrait;

    public function index()
    {
        return view ('admin.admins.index');
    }

    /**
     * Get Data for DataTable
     *
     * @param Request $request
     *
     * @return array
     * @throws \Throwable
     */
    public function list(Request $request)
    {
        $listData = $this->getDataForTable(Admin::query(), $request);

        $result = [];
        /** @var Admin $admin */
        foreach ($listData['data'] as $admin) {
            $result[] = [
                'id' => $admin->id,
                'name' => $admin->name,
                'email' => $admin->email,
                'avatar' => $admin->getAvatar(),
                'actions' => view('admin.admins.actions', compact('admin'))->render()
            ];
        }
        $listData['data'] = $result;
        $result = $listData;

        return $result;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $form = $this->form(AdminForm::class, [
            'method' => 'POST',
            'enctype' => 'multipart/form-data',
            'url' => route('amigo.admins.store'),
        ]);

        return view('admin.admins.form', compact('form'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $form = $this->form(AdminForm::class);
        $form->redirectIfNotValid();

        $model = Admin::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        if ($model) {
            if ($request->get('save') === 'redirect') {
                return redirect()->route('amigo.admins.index')->with('success','Faq updated');
            } else {
                return redirect()->route('amigo.admins.edit', $model->id)
                    ->with('success','Faq updated');
            }
        } else {
            return redirect()->route('amigo.admins.index')->with('error','Looks like something went wrong');
        }
    }

    /**
     * @param Admin $admin
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Admin $admin)
    {
        $form = $this->form(AdminForm::class, [
            'model' => $admin,
            'enctype' => 'multipart/form-data',
            'url' => route('amigo.admins.update', $admin),
            'method' => 'PUT',
        ]);

        return view ('admin.admins.form', compact('form'));
    }

    public function update(Request $request, Admin $admin)
    {
        $form = $this->form(AdminForm::class, [
            'model' => $admin
        ]);
        $form->redirectIfNotValid();

        if ($request->input('password')) {
            $admin->changePassword($request['password']);
        }

        if ($admin->update($request->only(['name', 'email']))) {
            if ($request->get('save') === 'redirect') {
                return redirect()->route('amigo.admins.index')->with('success','User updated');
            } else {
                return redirect()->route('amigo.admins.edit', $admin->id)
                    ->with('success','User updated');
            }
        } else {
            return redirect()->route('amigo.admins.index',  $admin->id)->with('error','Looks like something went wrong');
        }
    }

    /**
     * @param Admin $admin
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Admin $admin)
    {
        if ($admin->canDeleteAdmin()) {
            $admin->delete();
        }

        return response()->json(['success' => true, 'message' => 'Team has been deleted']);
    }
}

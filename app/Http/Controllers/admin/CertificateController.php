<?php

namespace App\Http\Controllers\admin;

use Amigo\Cms\Http\Controllers\Controller as BaseController;
use App\Extensions\DataTable\DataTable;
use App\Forms\Admin\CertificateForm;
use App\Models\Certificate;
use App\Models\Event;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class CertificateController extends BaseController
{
    use DataTable, FormBuilderTrait;

    public function index()
    {
        return view ('admin.certificates.index');
    }

    /**
     * Get Data for DataTable
     *
     * @param Request $request
     *
     * @return array
     * @throws \Throwable
     */
    public function list(Request $request)
    {
        $listData = $this->getDataForTable(Certificate::query(), $request);

        $result = [];
        /** @var Event $admin */
        foreach ($listData['data'] as $certificate) {
            $result[] = [
                'id' => $certificate->id,
                'logo' => $certificate->getLogo(),
                'name' => $certificate->name,
                'points' => $certificate->points,
                'actions' => view('admin.certificates.actions', compact('certificate'))->render()
            ];
        }
        $listData['data'] = $result;
        $result = $listData;

        return $result;
    }

    /**
     * @param Certificate $certificate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Certificate $certificate)
    {
        $form = $this->form(CertificateForm::class, [
            'model' => $certificate,
            'enctype' => 'multipart/form-data',
            'url' => route('amigo.certificates.update', $certificate),
            'method' => 'PUT',
        ]);

        return view('admin.certificates.form', compact('form'));
    }

    public function update(Request $request, Certificate $certificate)
    {
        $form = $this->form(CertificateForm::class, [
            'model' => $certificate
        ]);
        $form->redirectIfNotValid();

        if ($certificate->update($request->except(['save', '_token', 'logo']))) {
            if ($request->get('save') === 'redirect') {
                return redirect()->route('amigo.certificates.index')->with('success','User updated');
            } else {
                return redirect()->route('amigo.certificates.edit', $certificate->id)
                    ->with('success','User updated');
            }
        } else {
            return redirect()->route('amigo.certificates.index',  $certificate->id)->with('error','Looks like something went wrong');
        }
    }

    /**
     * @param Event $event
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Event $event)
    {
        $event->delete();

        return response()->json(['success' => true, 'message' => 'Team has been deleted']);
    }
}

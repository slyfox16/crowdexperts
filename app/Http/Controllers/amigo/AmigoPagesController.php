<?php

namespace App\Http\Controllers\amigo;

use Amigo\Cms\Http\Controllers\Controller as BaseController;
use App\Models\amigo\AmigoPage;
use Illuminate\Http\Request;
use Validator;

class AmigoPagesController extends BaseController
{

    public function index()
    {
        $model = AmigoPage::where('parent_id', 0)->with('children')->orderByDesc('id')->get();

        return view('amigo-pub::amigo_pages.index')->with(['mainModel' => $model]);
    }

    public function create(Request $request)
    {
        $model = new AmigoPage;

        return view('amigo-pub::amigo_pages.form')->with(['mainModel' => $model]);
    }

    public function store(Request $request)
    {
        $model = new AmigoPage;

        $validator = Validator::make(
            request()->all(),
            [
                'title' => 'required',
                'slug'  => 'required',

            ],
            []
        );

        $validator->validate();

        if ($this->saveData($model, $request)) {
            return redirect()->route('amigo.amigo_pages.index')->with('success', 'amigo_pages created');
        } else {
            return redirect()->route('amigo.amigo_pages.index')->with('error', 'Looks like something went wrong');
        }
    }

    public function edit($id)
    {
        $model = AmigoPage::findOrFail($id);

        return view('amigo-pub::amigo_pages.form')->with(['mainModel' => $model]);
    }

    public function update(Request $request, $id)
    {
        $model = AmigoPage::findOrFail($id);

        $validator = Validator::make(
            request()->all(),
            [
                'title' => 'required',
                'slug'  => 'required',

            ],
            []
        );

        $validator->validate();

        if ($this->saveData($model, $request)) {
            return redirect()->route('amigo.amigo_pages.index')->with('success', 'amigo_pages updated');
        } else {
            return redirect()->route('amigo.amigo_pages.index')->with('error', 'Looks like something went wrong');
        }
    }

    public function destroy($id)
    {
        $model = AmigoPage::findOrFail($id);
        $model->delete();

        return response()->json(['success' => true, 'message' => 'amigo_pages has been deleted']);
    }

    public function saveData($model, $request)
    {
        try {
            $model->title = request('title');
            $model->slug = request('slug');
            $model->text = request('text');
            $model->seo_title = request('seo_title');
            $model->seo_keywords = request('seo_keywords');
            $model->seo_description = request('seo_description');

            $model->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function position(Request $request)
    {
        $categoryPositions = json_decode($request->input('position'));
        $this->orderCategories($categoryPositions, 0);
    }

    private function orderCategories(array $categories, $parentId)
    {

        foreach ($categories as $index => $category) {
            $item = AmigoPage::findOrFail($category->id);
            $item->position = $index + 1;
            $item->parent_id = $parentId;
            $item->save();

            if (isset($category->children)) {
                $this->orderCategories($category->children, $item->id);
            }
        }
    }


}

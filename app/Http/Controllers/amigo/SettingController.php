<?php

namespace App\Http\Controllers\amigo;

use App\Models\amigo\Setting;
use Illuminate\Http\Request;
use Amigo\Cms\Traits\SettingsTrait;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller as BaseController;

class SettingController extends BaseController
{
    use SettingsTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::orderBy('order', 'asc')->get();
        $tab = request('tab') ?? 1;
        return view('amigo-pub::settings.index', compact('settings', 'tab'));
    }

    public function create()
    {
        return view('amigo-pub::settings.create_settings');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'key'          => 'required|string|max:255|unique:settings',
            'display_name' => 'required|string|max:255',
            'type'         => 'required|string|max:255'
        ]);

        $lastSetting = Setting::orderBy('order', 'desc')->first();

        if (is_null($lastSetting)) {
            $order = 0;
        } else {
            $order = intval($lastSetting->order) + 1;
        }

        $request->merge(['order' => $order]);
        Setting::create($request->only(['key', 'display_name', 'type', 'order']));

        return back()->with('success', 'New setting has been created.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $settings = Setting::all();

        foreach ($settings as $setting) {
            $content = $this->getContentBasedOnType($request, 'settings', (object)[
                'type'  => $setting->type,
                'field' => str_replace('.', '_', $setting->key),
                // 'group'   => $setting->group,
            ]);

            if ($setting->type == 'image' && $content == null) {
                continue;
            }

            if ($setting->type == 'file' && $content == json_encode([])) {
                continue;
            }

//            $key = preg_replace('/^'.Str::slug($setting->group).'./i', '', $setting->key);
            // $setting->group = $request->input(str_replace('.', '_', $setting->key).'_group');
            $setting->key = $setting->key;
            $setting->value = $content;
            $setting->save();
        }

        return back()->with('success', 'Settings has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Setting::findOrFail($id)->delete();

        return back()->with('success', 'Setting has been deleted.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $setting = Setting::findOrFail($id);

        if (isset($setting->id)) {
            // If the type is an image... Then delete it
            if ($setting->type == 'image') {
                if (Storage::disk('public')->exists($setting->value)) {
                    Storage::disk('public')->delete($setting->value);
                }
            }
            $setting->value = '';
            $setting->save();
        }

        return back()->with('success', 'Settings has been updated.');
    }

    public function moveUp($id)
    {
        $setting = Setting::findOrFail($id);

        $swapOrder = $setting->order; // swap order
        $previousSetting = Setting::where('order', '<', $swapOrder)
            // ->where('group', $setting->group)
            ->orderBy('order', 'DESC')->first();

        if (isset($previousSetting->order)) {
            $setting->order = $previousSetting->order;
            $setting->save();
            $previousSetting->order = $swapOrder;
            $previousSetting->save();
        }

        return back()->with('success', 'Settings has been updated.');
    }

    public function moveDown($id)
    {
        $setting = Setting::findOrFail($id);

        $swapOrder = $setting->order; // swap order
        $previousSetting = Setting::where('order', '>', $swapOrder)
            // ->where('group', $setting->group)
            ->orderBy('order', 'DESC')->first();

        if (isset($previousSetting->order)) {
            $setting->order = $previousSetting->order;
            $setting->save();
            $previousSetting->order = $swapOrder;
            $previousSetting->save();
        }

        return back()->with('success', 'Settings has been updated.');
    }
}

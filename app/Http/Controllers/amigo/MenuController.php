<?php

namespace App\Http\Controllers\amigo;

use App\Http\Controllers\Controller as BaseController;
use App\Models\amigo\Menu;
use App\Models\amigo\MenuPages;
use App\Models\amigo\AmigoPage;
use Illuminate\Http\Request;
use Validator;

class MenuController extends BaseController
{

    public function index()
    {
        $model = Menu::orderBy('id')->paginate(20);

        return view('amigo-pub::menu.index')->with(['mainModel' => $model]);
    }

    public function create(Request $request)
    {
        $model = new Menu;

        return view('amigo-pub::menu.form')->with(['mainModel' => $model]);
    }

    public function store(Request $request)
    {
        $model = new Menu;

        $validator = Validator::make(
            request()->all(),
            [
                'title' => 'required',

            ],
            []
        );

        $validator->validate();

        if ($this->saveData($model, $request)) {
            return redirect()->route('amigo.Menu.index')->with('success', 'Menu created');
        } else {
            return redirect()->route('amigo.Menu.index')->with('error', 'Looks like something went wrong');
        }
    }

    public function edit($id)
    {
        $model = Menu::findOrFail($id);

        $page_list = AmigoPage::orderBy('id')->get();

        $pages = $model->list()->with(['children', 'page', 'children.page'])->orderBy('position')->get();

        return view('amigo-pub::menu.form')->with(['mainModel' => $model, 'page_list' => $page_list, 'pages' => $pages]);
    }

    public function update(Request $request, $id)
    {
        $model = Menu::findOrFail($id);

        $validator = Validator::make(
            request()->all(),
            [
                'title' => 'required',

            ],
            []
        );

        $validator->validate();

        if ($this->saveData($model, $request)) {
            if (!empty(request('add_page_id'))) {
                return redirect()->back();
            }
            return redirect()->route('amigo.Menu.index')->with('success', 'Menu updated');
        } else {
            return redirect()->route('amigo.Menu.index')->with('error', 'Looks like something went wrong');
        }
    }

    public function destroy($id)
    {
        $model = Menu::findOrFail($id);
        $model->delete();

        return response()->json(['success' => true, 'message' => 'Menu has been deleted']);
    }

    public function saveData($model, $request)
    {
        try {
            $model->title = request('title');
            $model->save();

            if (!empty(request('add_page_id'))) {
                if (!$menu_page = MenuPages::where('menu_id', $model->id)->where('page_id', request('add_page_id'))->first()) {
                    $menu_page = new MenuPages;
                };
                $menu_page->menu_id = $model->id;
                $menu_page->page_id = request('add_page_id');
                $menu_page->parent_id = 0;
                $menu_page->position = 1;
                $menu_page->save();
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function position()
    {
        $categoryPositions = json_decode(request('position'));
        $this->orderPages($categoryPositions, 0);
    }

    private function orderPages(array $categories, $parentId)
    {
        //dd($categories);
        foreach ($categories as $index => $category) {
            $item = MenuPages::findOrFail($category->itemId);
            $item->position = $index + 1;
            $item->parent_id = $parentId;
            $item->save();

            if (isset($category->children)) {
                $this->orderPages($category->children, $item->id);
            }
        }
    }

    public function unlinkPage($id)
    {
        MenuPages::findOrFail($id)->delete();

        return response()->json(['success' => true, 'message' => 'Menu pages has been deleted']);

    }

}

<?php

namespace App\Http\Controllers\amigo;

use Amigo\Cms\Http\Requests\PageFormRequest;
use App\Models\amigo\Page;
use App\Http\Controllers\Controller as BaseController;

class PageController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request('search');
        $pages = Page::when($search, function($q) use($search) {
            return $q->whereRaw("title like ?", "%{$search}%");
        })->paginate(config('amigo.pagination.items_per_page'));
        return view('amigo-pub::pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('amigo-pub::pages.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageFormRequest $request)
    {
        $page = new Page();
        return $this->savePage($page, $request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        return view('amigo-pub::pages.form', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageFormRequest $request,$id)
    {
        $page = Page::findOrFail($id);
        return $this->savePage($page, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();
        return response()->json(['status' => true, 'message' => 'Page has been deleted']);
    }

    private function savePage(Page $page, PageFormRequest $request)
    {
        $message = $page->exists ? 'Page has been updated' : 'Page has been created';

        foreach($this->languages as $lang) {
            foreach ($page->translatable as $field) {
                $page->setTranslation($field, $lang->locale, $request->get($field)[$lang->locale]);
            }
        }
        $page->published = $request->get('published') === 'on' ? 1 : 0;
        $page->layout = $request->get('layout');
        $page->save();

        if ($request->get('save') === 'redirect') {
            return redirect()->route('amigo.pages.index')
                ->with('success', $message);
        }

        return redirect()->route('amigo.pages.edit', $page->id)
            ->with('success', $message);
    }
}

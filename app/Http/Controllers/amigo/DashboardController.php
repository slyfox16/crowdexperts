<?php

namespace App\Http\Controllers\amigo;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Controller as BaseController;

class DashboardController extends BaseController
{
    public function index()
    {
        $dates = null;
        $page_views = null;
        $visitors = null;

        return view('amigo-pub::dashboard', compact('dates', 'page_views', 'visitors'));
    }

    public function cache()
    {
        Cache::flush();
        return redirect()->back()->with('success', 'Cache has been flushed out!');
    }

    public function maintenance()
    {
        if (app()->isDownForMaintenance()) {
            Artisan::call("up");
            return redirect()->back()->with('success', 'Website is no more in maintenance mode!');
        } else {
            Artisan::call("down");
            return redirect()->back()->with('success', 'Website is in maintenance mode!');
        }
    }
}

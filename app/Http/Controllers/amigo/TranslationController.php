<?php

namespace App\Http\Controllers\amigo;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\TranslationLoader\LanguageLine;
use App\Http\Controllers\Controller as BaseController;

class TranslationController extends BaseController
{
    public function index()
    {
        $search = request('search');
        $category = request('category');
        $translations = LanguageLine::when($search, function ($q) use ($search) {
            return $q->where("key", "like", "%{$search}%")
                ->orWhere("original_key", 'like', "%{$search}%")
                ->orWhere("text", 'like', "%{$search}%");
        })->when($category, function ($q) use ($category) {
            return $q->where('category_id', $category);
        })->orderBy('id', 'asc')->paginate(1000);

        $categories = [1 => 'Default'];

        return view('amigo-pub::translations.index', compact('translations', 'categories'));
    }

    public function create()
    {
        return view('amigo-pub::translations.form');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'key'    => 'required|string|min:2|max:255',
            'text.*' => 'required|string|max:64000'
        ]);

        $translation = LanguageLine::create([
            'key'  => $request->get('key'),
            'text' => $request->get('text')
        ]);

        if ($request->get('save') === 'redirect') {
            return redirect()->route('amigo.translations.index')
                ->with('success', 'Translation has been created');
        }

        return redirect()->route('amigo.translations.edit', $translation->id)
            ->with('success', 'Translation has been created');
    }

    public function edit($id)
    {
        $translation = LanguageLine::findOrFail($id);
        return view('amigo-pub::translations.form', compact('translation'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'key'    => 'required|string|min:2|max:255',
            'text.*' => 'required|string|min:2|max:64000'
        ]);

        $translation = LanguageLine::findOrFail($id);
        $translation->fill($request->only(['key', 'text']))->update();

        if ($request->get('save') === 'redirect') {
            return redirect()->route('amigo.translations.index')
                ->with('success', 'Translation has been updated');
        }

        return redirect()->route('amigo.translations.edit', $translation->id)
            ->with('success', 'Translation has been updated');
    }

    public function save()
    {
        $categories = request('category') ?? [];
        $translations = request('translation') ?? [];

        foreach ($categories as $key => $category_id) {
            if ($translation = LanguageLine::find($key)) {
                $translation->category_id = $category_id;
                $translation->text = $translations[$key];
                $translation->save();
            }
        }

        return redirect()->route('amigo.translations.index')
            ->with('success', 'Translation has been updated');
    }

    public function destroy($id)
    {
        LanguageLine::findOrFail($id)
            ->delete();

        return response()->json(['success' => true, 'message' => 'Translation has been deleted']);
    }

}

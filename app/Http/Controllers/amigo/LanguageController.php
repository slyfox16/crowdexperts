<?php

namespace App\Http\Controllers\amigo;

use Amigo\Cms\Http\Requests\LanguageFormRequest;
use App\Models\amigo\Language;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller as BaseController;

class LanguageController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $search = request('search');
        $items = Language::when($search, function ($q) use ($search) {
            return $q->whereRaw("title like ?", "%{$search}%");
        })->paginate(config('amigo.pagination.items_per_page'));
        return view('amigo-pub::languages.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('amigo-pub::languages.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LanguageFormRequest $request
     * @return Response
     */
    public function store(LanguageFormRequest $request)
    {
        $lang = Language::create([
            'title'  => $request->get('title'),
            'locale' => $request->get('locale'),
            'site'   => $request->get('site') === 'on' ? 1 : 0,
            'admin'  => $request->get('admin') === 'on' ? 1 : 0
        ]);

        \Cache::forget('languages');

        if ($request->get('save') === 'redirect') {
            return redirect('/admin/settings?tab=2')
                ->with('success', 'Languages has been created');
        }

        return redirect('/admin/settings?tab=2')
            ->with('success', 'Languages has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param Language $language
     * @return void
     */
    public function show(Language $language)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Language $language
     * @return Response
     */
    public function edit(Language $language)
    {
        return view('amigo-pub::languages.form', ['item' => $language]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LanguageFormRequest $request
     * @param Language $language
     * @return Response
     */
    public function update(LanguageFormRequest $request, Language $language)
    {
        $language->update([
            'title'  => $request->get('title'),
            'locale' => $request->get('locale'),
            'site'   => $request->get('site') === 'on' ? 1 : 0,
            'admin'  => $request->get('admin') === 'on' ? 1 : 0
        ]);

        \Cache::forget('languages');

        if ($request->get('save') === 'redirect') {
            return redirect('/admin/settings?tab=2')
                ->with('success', 'Languages has been updated');
        }

        return redirect('/admin/settings?tab=2')
            ->with('success', 'Languages has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Language $language
     * @return Response
     * @throws \Exception
     */
    public function destroy(Language $language)
    {
        $language->delete();

        \Cache::forget('languages');

        return response()->json(['success' => true, 'message' => 'Language has been deleted']);
    }
}

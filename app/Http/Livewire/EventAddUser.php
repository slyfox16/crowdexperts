<?php

namespace App\Http\Livewire;

use App\Models\Event;
use DB;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class EventAddUser extends Component
{
    public $event;
    public $alreadyUser = false;

    protected $listeners = ['changeButton'];

    public function mount()
    {
        $this->alreadyUser = DB::table('events_users')
            ->where('user_id', Auth::id())
            ->where('event_id', $this->event->id)
            ->exists();
    }

    public function render()
    {
        return view('livewire.event-add-user', ['alreadyUser' => $this->alreadyUser]);
    }

    public function addUser()
    {
        $alreadyUser = DB::table('events_users')
            ->where('user_id', Auth::id())
            ->where('event_id', $this->event->id)
            ->exists();

        if ($alreadyUser) {
            DB::table('events_users')
                ->where('user_id', Auth::id())
                ->where('event_id', $this->event->id)
                ->delete();

            $this->alreadyUser = false;
        } else {
            DB::table('events_users')->insert([
                'user_id' => Auth::id(),
                'event_id' => $this->event->id
            ]);

            $this->alreadyUser = true;
        }

        $this->emitTo('event-users', 'updateUsers');
    }

    public function changeButton($alreadyUser)
    {
        $this->alreadyUser = $alreadyUser;
    }
}

<?php

namespace App\Http\Livewire;

use App\ExpertSearch\ExpertSearch;
use App\Models\User;
use Livewire\Component;

class Experts extends Component
{
    protected $listeners = ['filterAdded'];

    public $orderList = [
        'most_certified' => 'Most Certified',
        'created_at' => 'Date',
    ];

    public $perPage = 15;
    public $amount = 0;
    public $order = '';
    public $filter = [];

    public function mount()
    {
        $this->order = array_key_first($this->orderList);
        $this->amount = $this->perPage;
    }

    public function render()
    {
        $showButton = true;
        $users = User::active();
        $users = ExpertSearch::apply($users, $this->filter);

        $allExperts = $users->count();

        $expertsList = $users
            ->withCount('certificates')
            ->take($this->amount)
            ->when($this->order == 'most_certified', function ($query) {
                return $query->orderBy('certificates_count', 'DESC');
            }, function ($query, $role) {
                return $query->orderBy($this->order, 'DESC');
            })
            ->get();

        if ($this->amount >= $allExperts ) {
            $showButton = false;
        }

        return view('livewire.experts', compact(
            'expertsList',
            'showButton',
        ))
            ->extends('frontend.layouts.app_main');
    }

    public function load()
    {
        $this->amount += $this->perPage;
    }

    public function order($order)
    {
        $this->order = $order;
    }

    public function filterAdded($filter)
    {
        $this->filter = $filter;
    }
}

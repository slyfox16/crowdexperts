<?php

namespace App\Http\Livewire;

use App\Forms\Cabinet\FilterForm;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Livewire\Component;

class Filter extends Component
{
    use FormBuilderTrait;

    public $filter = [];

    public function render()
    {
        $filterForm = $this->form(FilterForm::class, [
            'id' => 'filter-form',
            'wire:submit.prevent' => 'filterAdded'
        ]);

        return view('livewire.filter', compact(
            'filterForm',
        ));
    }

    public function filterAdded()
    {
        $this->emit('filterAdded', $this->filter);
    }
}

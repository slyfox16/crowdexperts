<?php

namespace App\Http\Livewire;

use App\ExpertSearch\ExpertSearch;
use App\Models\User;
use Livewire\Component;

class IndexPage extends Component
{
    protected $listeners = ['filterAdded'];

    public $orderList = [
        'rating' => 'Rating',
        'created_at' => 'Date',
    ];

    public $amount = 8;
    public $filter = [];

    public function render()
    {
        $expertsList = ExpertSearch::apply(User::active(), $this->filter);

        $users = $expertsList
            ->inRandomOrder()
            ->take($this->amount)
            ->get();

        return view('livewire.index-page', compact(
            'users',
        ))
            ->extends('frontend.layouts.app_main');
    }

    public function filterAdded($filter)
    {
        $this->filter = $filter;
    }
}

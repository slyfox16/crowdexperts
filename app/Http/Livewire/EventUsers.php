<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EventUsers extends Component
{
    public $event = null;
    public $users = [];

    protected $listeners = ['updateUsers'];

    public function mount()
    {
        $this->users = $this->event->users;
    }

    public function render()
    {
        return view('livewire.event-users', ['users' => $this->users]);
    }

    public function updateUsers()
    {
        $this->users = $this->event->users;
    }
}

<?php

namespace App\Http\Resources;

use App\Models\Session;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'avatar' => $this->resize('avatar', 476, 476),
            'email' => $this->email,
            'online' => false,
            'session' => $this->sessionDetails($this->id)
        ];
    }

    private function sessionDetails($id)
    {
        $session = Session::whereIn('user1_id', [Auth::id(), $id])
            ->whereIn('user2_id', [Auth::id(), $id])
            ->first();

        return new SessionResource($session);
    }
}

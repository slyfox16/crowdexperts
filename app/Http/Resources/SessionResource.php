<?php

namespace App\Http\Resources;

use App\Models\Chat;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class SessionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'open' => false,
            'users' => [
                $this->user1_id,
                $this->user2_id
            ],
            'unreadCount' => $this->chats
                ->whereNull('read_at')
                ->where('type', Chat::SEND)
                ->where('user_id', '!=', Auth::id())
                ->count(),
            'block' => $this->blocked_by > 0,
            'blocked_by' => $this->blocked_by
        ];
    }
}

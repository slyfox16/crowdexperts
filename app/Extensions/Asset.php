<?php

namespace App\Extensions;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Database\Query\Expression;

/**
 * Class Asset.
 */
class Asset
{
    /**
     * @var array
     */
    public static $script = [];

    /**
     * @var array
     */
    public static $css = [];

    /**
     * @var array
     */
    public static $js = [];

    /**
     * @var string
     */
    public static $jQuery = '';

    /**
     * @var array
     */
    public static $baseCss = [];

    /**
     * @var array
     */
    public static $baseJs = [];

    /**
     * @var array
     */
    protected static $globalParams = [];

    /**
     * @var array
     */
    protected static $_options = [];

    /**
     * Add css or get all css.
     *
     * @param null $css
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public static function css($css = null)
    {
        if (!is_null($css)) {
            self::$css = array_merge(self::$css, (array) $css);

            return;
        }

        static::$css = array_merge(static::$css, static::baseCss());

        return view('partials.css', ['css' => array_unique(static::$css)]);
    }

    /**
     * Rewrite baseCss list
     *
     * @param null $css
     *
     * @return array|void
     */
    public static function baseCss($css = null)
    {
        if (!is_null($css)) {
            static::$baseCss = $css;

            return;
        }

        return static::$baseCss;
    }

    /**
     * Add js or get all js.
     *
     * @param null $js
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public static function js($js = null)
    {
        if (!is_null($js)) {

            if ($js instanceof Htmlable) {
                $js = $js->toHtml();
            }

            self::$js = array_merge(self::$js, (array) $js);

            return;
        }

        static::$js = array_merge(static::baseJs(), static::$js);

        return view('partials.js', ['js' => array_unique(static::$js)]);
    }

    /**
     * Rewrite baseJs list
     *
     * @param null $js
     *
     * @return array|void
     */
    public static function baseJs($js = null)
    {
        if (!is_null($js)) {
            static::$baseJs = $js;

            return;
        }

        return static::$baseJs;
    }

    /**
     * Add script or get all scripts.
     *
     * @param string $script
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public static function script($script = '')
    {
        if (!empty($script)) {
            $script = preg_replace('#<script(.*?)>(.*?)</script>#is', '$2', $script);

            self::$script = array_merge(self::$script, (array) $script);

            return;
        }

        return view('partials.script', ['script' => array_unique(self::$script)]);
    }

    /**
     * @return string
     */
    public static function jQuery()
    {
        return schema_asset(static::$jQuery);
    }

    /**
     * @param array $params
     */
    public static function setParams($params)
    {
        if ($params) {
            foreach ($params as $key => $param) {
                self::$_options[$key] = $param;
            }
        }
    }

    /**
     * @return array
     */
    public static function getParams()
    {
        $expressions = [];
        $value = self::processData(self::$_options, $expressions,  uniqid('', true));
        $json = json_encode($value);
        self::$_options = null;
        return $expressions === [] ? $json : strtr($json, $expressions);
    }

    /**
     * Pre-processes the data before sending it to `json_encode()`.
     * @param mixed $data the data to be processed
     * @param array $expressions collection of JavaScript expressions
     * @param string $expPrefix a prefix internally used to handle JS expressions
     * @return mixed the processed data
     */
    protected static function processData($data, &$expressions, $expPrefix)
    {
        if (is_object($data)) {
            if ($data instanceof Expression) {
                $token = "!{[$expPrefix=" . count($expressions) . ']}!';
                $expressions['"' . $token . '"'] = $data->getValue();

                return $token;
            } elseif ($data instanceof \JsonSerializable) {
                return static::processData($data->jsonSerialize(), $expressions, $expPrefix);
            } elseif ($data instanceof Arrayable) {
                $data = $data->toArray();
            } elseif ($data instanceof \SimpleXMLElement) {
                $data = (array) $data;
            } else {
                $result = [];
                foreach ($data as $name => $value) {
                    $result[$name] = $value;
                }
                $data = $result;
            }

            if ($data === []) {
                return new \stdClass();
            }
        }

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (is_array($value) || is_object($value)) {
                    $data[$key] = static::processData($value, $expressions, $expPrefix);
                }
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public static function setGlobalParams($key, $value)
    {
        array_set_value(self::$globalParams, $key, $value);
    }

    /**
     * @return array
     */
    public static function getGlobalParams($key)
    {
        return array_get_value(self::$globalParams, $key);
    }
}

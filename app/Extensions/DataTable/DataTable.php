<?php
/**
 * Created by PhpStorm.
 * User: alex.cebotari@ourbox.org
 * Date: 2019-02-25
 * Time: 10:13
 */

namespace App\Extensions\DataTable;

use App\TokenShare;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Trait DataTable
 * @package App\Libraries\DataTable
 */
trait DataTable
{
    /**
     * Column array DataTable
     *
     * @var array $_dataTableColumns
     */
    protected $_dataTableColumns = [];

    /**
     * Events array DataTable
     * @var array $_dataTableEvents
     */
    protected $_dataTableEvents = [];

    /**
     * Method to add values to an array of DataTable columns
     *
     * @param string $field
     * @param string $title
     * @param int $width
     * @param bool $sortable
     * @param array $parameters
     */
    protected function addColumn(string $field, string $title, int $width, bool $sortable = false, array $parameters = []): void
    {
        $column = [
            'data' => $field,
            'title' => $title,
            'sortable' => $sortable,
            'type' => 'default'
        ];

        if ($width > 0) {
            $column['width'] = $width;
        }

        foreach ($parameters as $parameterTitle => $value) {
            $column[$parameterTitle] = $value;
        }

        $this->_dataTableColumns[] = $column;
    }

    /**
     * Method to add events to an array of DataTable events
     *
     * @param string $event
     * @param string $inputId
     */
    protected function addEvent(string $event, string $inputId): void
    {
        $this->_dataTableEvents[$event][] = $inputId;
    }

    /**
     * Method to get columns for view in JSON
     *
     * @return false|string
     */
    protected function getColumns()
    {
        return json_encode($this->_dataTableColumns);
    }

    /**
     * Method to get events for view in JSON
     * @return false|string
     */
    protected function getEvents()
    {
        return json_encode($this->_dataTableEvents);
    }

    /**
     * Method to init DataTable Columns
     */
    protected function initColumns(): void
    {
        // Put your addColumn here
    }

    /**
     * Method to init DataTable events
     */
    protected function initEvents(): void
    {
        // Put your addEvent here
    }

    /**
     * Get Data from Model
     *
     * @param Model|Builder $model
     * @param Request $request
     *
     * @return array
     */
    protected function getDataForTable($model, Request $request)
    {
        $draw = $request->input('draw', 0);
        $page = ($request->input('page'));
        $perpage = $request->input('length', 50);

        $data = $model->dataTable()->paginate($perpage, ['*'], 'page', $page);

        $result = [
            'draw' => $draw,
            'recordsTotal' => $data->total(),
            'recordsFiltered' => $data->total(),
            'data' => $data
        ];

        return $result;
    }

    /**
     * Get Data for DataTable
     *
     * @param Request $request
     *
     * @return array
     * @throws \Throwable
     */
    public function list(Request $request)
    {
        /**
         * An example of how to get data for DatTable
         */
        $listData = $this->getDataForTable(new Log(), $request);

        $result = [];
        foreach ($listData['data'] as $log) {
            $result[] = [
                'id' => $log->id,
                'user' => (is_null($log->user)) ? 'null' : $log->user->email,
                'user_type' => ($log->user_type) ?? 'null',
                'event' => $log->event,
                'auditable_type' => $log->auditable_type,
                'created_at' => $log->created_at->format('H:m Y-m-d'),
                'actions' => view('admin.logs.action', compact('log'))->render()
            ];
        }
        $listData['data'] = $result;

        return $listData;
    }
}

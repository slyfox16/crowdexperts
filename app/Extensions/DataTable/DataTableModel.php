<?php
/**
 * Created by PhpStorm.
 * User: alex.cebotari@ourbox.org
 * Date: 2019-02-25
 * Time: 10:13
 */

namespace App\Extensions\DataTable;

trait DataTableModel
{
    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeDataTable($query)
    {
        $searchData = request('searchQuery');

        $sortData = request('sort');

        if (!empty($searchData)) {
            foreach ($searchData as $row => $value) {
                if ($row == 'created_at' || $row == 'updated_at') {
                    $date = date('Y-m-d', strtotime($searchData['createdAt']));
                    $dateFrom = $date . ' 00:00:01';
                    $dateTo = $date . ' 23:59:59';
                    $query->where($row, '>=', $dateFrom)->where($row, '<=', $dateTo);
                } elseif ($row == 'deleted_at') {
                    $date = date('Y-m-d', strtotime($searchData['createdAt']));
                    $dateFrom = $date . ' 00:00:01';
                    $dateTo = $date . ' 23:59:59';
                    $query->where($row, '>=', $dateFrom)->where($row, '<=', $dateTo)->withTrashed();
                } else {
                    $query->where($row, '=', $value);
                }
            }
        }

        if (!empty($sortData)) {
            $query->orderBy($sortData['field'], $sortData['sort']);
        } else {
            $query->orderByDesc($this->primaryKey);
        }

        return $query;
    }
}

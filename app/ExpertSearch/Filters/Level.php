<?php

namespace App\ExpertSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Level implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->when(!empty($value), function ($query) use ($value) {
            return $query->where('level', $value);
        });
    }
}

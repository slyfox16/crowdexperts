<?php

namespace App\ExpertSearch;

use Illuminate\Database\Eloquent\Builder;

class ExpertSearch
{
    private static $filter;

    public static function apply(Builder $query, array $filter)
    {
        self::$filter = $filter;
        return static::applyDecoratorsFromRequest($query);
    }

    private static function applyDecoratorsFromRequest(Builder $query)
    {
        $searchData = self::$filter;

        if ($searchData) {
            foreach ($searchData as $filterName => $value) {

                $decorator = static::createFilterDecorator($filterName);

                if (static::isValidDecorator($decorator)) {
                    $query = $decorator::apply($query, $value);
                }
            }
        }

        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filters\\' . str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }
}

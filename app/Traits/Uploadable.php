<?php

namespace App\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use InvalidArgumentException;
use League\Flysystem\Util;
use MimeTyper\Repository\MimeDbRepository;

trait Uploadable
{
    use ImageManipulate;

    /**
     * @var string Model temp id
     */
    private $_tempID;

    /**
     * Recursive iterator
     * @var int
     */
    private $_recursiveIterator = false;

    /**
     * Recursive iterator
     * @var int
     */
    private $_imageRelation;

    /**
     * Uploadable constructor.
     */
    private function setUploadAttributes()
    {
        if (!is_array($this->uploadSettings) || empty($this->uploadSettings)) {
            throw new InvalidArgumentException('Invalid or empty attributes array.');
        } else {
            foreach ($this->uploadSettings as $attribute => $config) {

                if (is_numeric($attribute)) {
                    unset($this->uploadSettings[$attribute]);
                    $this->uploadSettings[$config] = [];
                    $attribute = $config;
                    $config = [];
                }

                $path = Arr::get($config, 'path', null);
                $url = Arr::get($config, 'url', null);

                if (!$path) {
                    $config['path'] = 'public/' . $this->getClass();
                }
                if (!$url) {
                    $config['url'] = 'storage/' . $this->getClass();
                }
                /*if (empty($config['tempPath'])) {
                    throw new InvalidArgumentException('Temporary path must be set for all attributes.');
                }*/

                $this->uploadSettings[$attribute]['path'] = Util::normalizePath($config['path']);
                $this->uploadSettings[$attribute]['url'] = rtrim($config['url'], '/');
                //$this->uploadSettings[$attribute]['tempPath'] = Util::normalizePath($config['tempPath']) . DIRECTORY_SEPARATOR;
            }
        }
    }

    public static function bootUploadable()
    {
        self::saved(function($model) {
            $attributes = array_keys($model->uploadSettings);

            if ($attributes) {
                foreach ($attributes as $attr) {
                    if ($file = $model->retrievefile($attr)) {

                        $file = is_array($file) ? data_get($file, 0) : $file;
                        $filename = Str::slug('image_' . time() . '_' . Str::random(10)) . '.' . $file->getClientOriginalExtension();

                        // Get all files in a directory
                        $files = Storage::allFiles('public/' . $model->getPath($attr));

                        // Delete Files
                        Storage::delete($files);

                        if ($file->storeAs($model->getPath($attr), $filename)) {
                            $model->$attr = str_replace('public/', '', $model->getPath($attr)) . $filename;
                            $model->saveQuietly();

                            File::deleteDirectory(storage_path('app/public/association-membership'));
                            File::deleteDirectory(storage_path('app/public/certificates'));
                        }
                    }
                }
            }
        });
    }

    private function retrievefile($attr)
    {
        if ($this->getRecursiveIterator() === false) {
            return request()->file($attr);
        }

        $allFiles = request()->allFiles();
        $attrFiles = data_get($allFiles, $this->getImageRelation() . '.' . $this->getRecursiveIterator() . '.' . $attr);

        return $attrFiles;
    }

    public function initializeUploadable()
    {
        $this->setUploadAttributes();
    }

    /**
     * @param string $attribute Attribute name
     *
     * @return string Path to file
     */
    public function getPath($attribute)
    {
        return $this->uploadSettings[$attribute]['path'] . DIRECTORY_SEPARATOR . $this->getFolderId() . DIRECTORY_SEPARATOR;
    }

    /**
     * @param string $attribute Attribute name
     *
     * @return string Url to file
     */
    public function getUrl($attribute)
    {
        return $this->uploadSettings[$attribute]['url'] . DIRECTORY_SEPARATOR . $this->getFolderId(). DIRECTORY_SEPARATOR;
    }

    /**
     * @param string $attribute Attribute name
     *
     * @return string Path to temporary file
     */
    public function tempPath($attribute)
    {
        return $this->uploadSettings[$attribute]['tempPath'];
    }

    /**
     * @return string Temporary file path
     */
    public function tempDir($attribute, $postID = null)
    {
        $postID = $postID ? : $this->getFolderId();
        return $this->tempPath($attribute) . DIRECTORY_SEPARATOR . $postID . DIRECTORY_SEPARATOR;
    }

    /**
     * @return string File path
     */
    public function fileDir($attribute)
    {
        return $this->getPath($attribute) . DIRECTORY_SEPARATOR . $this->getFolderId() . DIRECTORY_SEPARATOR;
    }

    /**
     * Delete specified file.
     *
     * @param string $file File path
     *
     * @return bool `true` if file was successfully deleted
     */
    protected function deleteFile($file)
    {
        if (is_file($file)) {
            return unlink($file);
        }
        return false;
    }

    /**
     * Get id of an element or temp id if an element is new
     */
    public function getFolderId()
    {
        if ($this->id) {
            return $this->id;
        } elseif ($this->_tempID) {
            return $this->_tempID;
        } elseif (empty($this->id) && empty($this->_tempID)) {
            $this->_tempID = Str::random(32);
            return $this->_tempID;
        }
    }

    /**
     * @param string $attribute Attribute name
     *
     * @return string Temporary file path
     */
    public function tempFile($attribute)
    {
        return $this->tempDir($attribute) . $this->$attribute;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        $class = class_basename(get_class($this));
        $class = strtolower($class);

        return $class;
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getFile($attribute)
    {
        if (Storage::exists('public/' . $this->$attribute)) {
            return 'storage/' . $this->$attribute;
        }

        return null;
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getFilePath($attribute)
    {
        if (Storage::exists($this->getPath($attribute) . $this->$attribute)) {
            return $this->getPath($attribute) . $this->$attribute;
        }

        return null;
    }

    /**
     * @param $attribute
     * @return array
     */
    public function getStoredFiles($attribute)
    {
        $imageList = [];
        $filePath = $this->getFilePath($attribute);

        if ($filePath && File::exists(storage_path('app/' . $filePath))) {
            $mimeRepository = new MimeDbRepository();
            $mimeType = Storage::mimeType($filePath);

            $imageList[] = [
                'id' => $this->id,
                'name' => $this->$attribute,
                'size' => Storage::size($filePath),
                'mimeType' => strpos($mimeType, 'image') === false ? $mimeRepository->findExtensions( $mimeType )[0] : 'image',
                'url' => schema_asset($this->getUrl($attribute) . $this->$attribute),
            ];
        }
        return $imageList;
    }

    /**
     * @return int
     */
    public function getRecursiveIterator()
    {
        return $this->_recursiveIterator;
    }

    /**
     * @param int $recursiveIterator
     */
    public function setRecursiveIterator($recursiveIterator)
    {
        $this->_recursiveIterator = $recursiveIterator;
    }

    /**
     * @return int
     */
    public function getImageRelation()
    {
        return $this->_imageRelation;
    }

    /**
     * @param int $relation
     */
    public function setImageRelation($relation)
    {
        $this->_imageRelation = $relation;
    }
}


<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

trait ImageManipulate
{
    /**
     * @param $attribute
     * @param $x
     * @param $y
     * @return string
     */
    public function resize($attribute, $x, $y)
    {
        $cropedFileName = str_replace('.', '_' . $x . '_' . $y . '.', $this->$attribute);
        $pathCropedFile = storage_path('app/public/' . $cropedFileName);

        if (!File::exists($pathCropedFile)) {
            Image::make($this->getFile('avatar'))->resize($x, $y, function ($constraint) {
                $constraint->aspectRatio();
            })->save($pathCropedFile);
        }

        return 'storage/' . $cropedFileName;
    }
}


<?php

namespace Database\Seeders;

use App\Services\ReadLargeCSV;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('cities.csv');
        $csv_reader = new ReadLargeCSV($file, ",");

        foreach($csv_reader->csvToArray() as $data) {
            DB::table('cities')->insert($data);
        }
    }
}

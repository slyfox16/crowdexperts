<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('title')->after('avatar')->nullable();
            $table->string('username')->after('title');
            $table->string('phone')->after('username')->nullable();
            $table->unsignedInteger('country_id')->after('phone')->nullable();
            $table->unsignedInteger('city_id')->after('country_id')->nullable();
            $table->string('address')->after('city_id')->nullable();
            $table->string('companyname')->after('address')->nullable();
            $table->string('position')->after('companyname')->nullable();
            $table->text('description')->after('position')->nullable();
            $table->string('facebook_url')->after('description')->nullable();
            $table->string('linkedin_url')->after('facebook_url')->nullable();
            $table->string('twitter_url')->after('linkedin_url')->nullable();
            $table->string('xing_url')->after('twitter_url')->nullable();
            $table->string('web_url')->after('xing_url')->nullable();
            $table->float('rating')->after('web_url')->default(0);
            $table->tinyInteger('level')->after('rating')->default(0);
            $table->tinyInteger('new_lavel')->after('level')->default(0);
            $table->boolean('expert_confirm')->after('new_lavel')->default(0);
            $table->boolean('is_active')->after('expert_confirm')->default(1);
            $table->boolean('is_staff')->after('is_active')->default(0);
            $table->boolean('new')->after('is_staff')->default(1);
            $table->boolean('email_sent')->after('new')->default(0);

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_country_id_foreign');
            $table->dropForeign('users_city_id_foreign');

            $table->dropColumn([
                'title',
                'username',
                'phone',
                'country_id',
                'city_id',
                'address',
                'companyname',
                'position',
                'description',
                'facebook_url',
                'linkedin_url',
                'twitter_url',
                'xing_url',
                'web_url',
                'rating',
                'level',
                'new_lavel',
                'expert_confirm',
                'is_active',
                'is_staff',
                'new',
                'email_sent',
            ]);
        });
    }
}

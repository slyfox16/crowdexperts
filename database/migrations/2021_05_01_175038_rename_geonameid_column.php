<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameGeonameidColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities', function(Blueprint $table) {
            $table->renameColumn('geonameid', 'id');
            $table->renameColumn('city_name_ASCII', 'ascii');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function(Blueprint $table) {
            $table->renameColumn('id', 'geonameid');
            $table->renameColumn('ascii', 'city_name_ASCII');
        });
    }
}

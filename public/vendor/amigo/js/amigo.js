$(document).ready(function () {

    $('.arrow').on("click", function (event) {
        if ($('#sidebar').hasClass('small')) {
            $('#sidebar').removeClass('small');
        } else {
            $('#sidebar').addClass('small');
        }
    });

    $('.edit-slug').on('click', function () {
        $(this).parents('.col-xl-6').find('input').removeAttr('readonly');
    });

    jQuery.fn.extend({
        autoHeight: function () {
            function autoHeight_(element) {
                return jQuery(element)
                    .css({'height': 'auto', 'overflow-y': 'hidden'})
                    .height(element.scrollHeight);
            }

            return this.each(function () {
                autoHeight_(this).on('input', function () {
                    autoHeight_(this);
                });
            });
        }
    });
    $('.autoExpand').autoHeight();
});

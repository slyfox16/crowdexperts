<?php

return [
    'pagination' => [
        'items_per_page' => 15
    ],

    'storage' => [
        'disk' => env('FILESYSTEM_DRIVER', 'public'),
    ],

    'pages' => [
        'layouts' => []
    ],

    'languages' => [
        'fallback_locale' => 'en',
        'default_locale'  => 'en',
        'debug'           => 0
    ],

    'media' => [
        'favicon'             => [
            [
                'width'  => 16,
                'height' => 16
            ],
            [
                'width'  => 32,
                'height' => 32
            ],
            [
                'width'  => 196,
                'height' => 196
            ]
        ],
        'allowed_mimetypes'   => '*',
        'path'                => '/',
        'show_folders'        => true,
        'allow_upload'        => true,
        'allow_move'          => true,
        'allow_delete'        => true,
        'allow_create_folder' => true,
        'allow_rename'        => true,
    ],
];
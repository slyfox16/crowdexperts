<?php

use App\Http\Controllers\Auth\SocialController;
use App\Http\Controllers\Cabinet\CertificateController;
use App\Http\Controllers\Cabinet\CompletedProjectController;
use App\Http\Controllers\Cabinet\ChatController;
use App\Http\Controllers\Cabinet\EventController;
use App\Http\Controllers\Cabinet\ContactController;
use App\Http\Controllers\Cabinet\MainController as CabinetController;
use App\Http\Controllers\Guest\MainController as GuestController;
use App\Http\Livewire\Experts;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\IndexPage;

Route::get('/', IndexPage::class)->name('home');
Route::get('/contact', [GuestController::class, 'contact'])->name('contact');
Route::post('/contact', [GuestController::class, 'messageSend']);
Route::post('/contactUnregistered', [GuestController::class, 'messageUnregisteredSend'])->name('messageUnregisteredSend');

Route::get('/suggest/country', [ContactController::class, 'suggestCountry'])->name('cabinet.suggestCountry');
Route::get('/suggest/city', [ContactController::class, 'suggestCity'])->name('cabinet.suggestCity');

Route::get('/profile/{user?}', [CabinetController::class, 'showProfile'])->name('profile');
Route::get('/impressum', [CabinetController::class, 'impressum'])->name('impressum');
Route::get('/agreement', [GuestController::class, 'getAgreement'])->name('agreement');

Route::group(
    [
        'as' => 'cabinet.',
        'middleware' => ['auth']
    ],
    function () {
        Route::get('/cabinet', [CabinetController::class, 'index'])->name('main');
        Route::get('/experts', Experts::class)->name('experts');
        Route::get('/chat', [ChatController::class, 'index'])->name('chat');
        Route::post('/cabinet', [CabinetController::class, 'cabinetStore']);
        Route::post('/contacts', [ContactController::class, 'contactStore'])->name('contactStore');
        Route::post('/certificate', [CertificateController::class, 'certificateStore'])->name('certificateStore');
        Route::post('/completed-projects', [CompletedProjectController::class, 'projectStore'])->name('projectStore');
        Route::post('/messageSend', [CabinetController::class, 'messageSend'])->name('messageSend');

        Route::get('/suggest/specialities', [CabinetController::class, 'suggestSpecialities'])->name('specialities');
        Route::post('/addAssociation', [CabinetController::class, 'addAssociationMembership'])->name('add.association');
        Route::post('/deleteAssociation', [CabinetController::class, 'deleteAssociationMembership'])->name('delete.association');
        Route::post('/deleteCertificate', [CertificateController::class, 'deleteCertificate'])->name('delete.certificate');
        Route::post('/deleteProject', [CompletedProjectController::class, 'deleteProject'])->name('delete.project');
        Route::post('/avatar', [CabinetController::class, 'avatarStore'])->name('avatar.store');

        Route::get('/events', [EventController::class, 'index'])->name('events');
        Route::get('/event/{event}', [EventController::class, 'show'])->name('event');
        Route::post('/events/store', [EventController::class, 'store'])->name('events.store');

        Route::get('/addToChat/{user}', [CabinetController::class, 'addToChat'])->name('user.addToChat');
        Route::get('/remove-chat/{user}', [CabinetController::class, 'removeChat'])->name('user.removeChat');

        Route::post('/getFriends', [ChatController::class, 'getFriends']);
        Route::post('/session/create', [ChatController::class, 'sessionCreate']);
        Route::post('/session/{session}/chats', [ChatController::class, 'chats']);
        Route::post('/session/{session}/read', [ChatController::class, 'read']);
        Route::post('/session/{session}/clear', [ChatController::class, 'clear']);
        Route::post('/session/{session}/block', [ChatController::class, 'block']);
        Route::post('/session/{session}/unblock', [ChatController::class, 'unblock']);
        Route::post('/send/{session}', [ChatController::class, 'send']);
    }
);

Route::get('/auth/facebook', [SocialController::class, 'facebook'])->name('auth.facebook');
Route::get('/auth/facebook/redirect', [SocialController::class, 'facebookRedirect']);

Route::get('/auth/linkedin', [SocialController::class, 'linkedin'])->name('auth.linkedin');
Route::get('/auth/linkedin/redirect', [SocialController::class, 'linkedinRedirect']);

Route::get('/auth/xing', [SocialController::class, 'xing'])->name('auth.xing');
Route::get('/auth/xing/redirect', [SocialController::class, 'xingRedirect']);

require __DIR__.'/auth.php';

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

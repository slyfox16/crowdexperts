const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .scripts([
        'resources/js/vendor.js',
        'resources/js/project.js',
        'node_modules/jquery-validation/dist/jquery.validate.js',
        'node_modules/sweetalert2/dist/sweetalert2.all.min.js',
        'resources/js/core.js',
        'node_modules/select2/dist/js/select2.min.js',
        'node_modules/moment/moment.js',
        'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
        'node_modules/bootstrap-daterangepicker/daterangepicker.js',
    ], 'public/js/app.js')
    .scripts([
        'node_modules/datatables.net/js/jquery.dataTables.js',
        'resources/js/backend/dataTables.bootstrap4.min.js',
        'resources/js/backend/dataTables.buttons.min.js',
        'resources/js/backend/data-table.js',
    ], 'public/js/datatable.js')
    .scripts([
        'resources/js/core.js',
        'node_modules/select2/dist/js/select2.min.js',
        'node_modules/moment/moment.js',
        'node_modules/bootstrap-daterangepicker/daterangepicker.js',
    ], 'public/js/backend.js')
    .js('resources/js/chat.js', 'public/js')
    .js('resources/js/login_register_validate.js', 'public/js')
    .js('resources/js/cabinet_forms_validate.js', 'public/js')
    .sass('resources/scss/backend_app.scss', 'public/css')
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
        require('autoprefixer'),
    ])
    .copyDirectory('resources/images', 'public/images')
    .version();

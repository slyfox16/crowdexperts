(function ($) {
    "use strict";

    var fn = {

        // launch functions
        Launch: function () {
            fn.Apps();
        },


        // apps
        Apps: function () {


            // popovers & tooltips
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover();
                $('.toast').toast();
            });



            // animate on scroll
            $(function () {
                AOS.init({
                    disable:  function msieversion() {
                        return !!(window.navigator.userAgent.indexOf("MSIE ") > 0 || navigator.userAgent.match(
                            /Trident.*rv\:11\./))
                    }
                });
            });

            // smooth scroll
            $(function () {
                var scroll = new SmoothScroll('[data-scroll]');
            });


            // navbar acive
            $(function() {
                $('.navbar .collapse').on('show.bs.collapse', function () {
                    setTimeout(() => {
                        $('body').addClass('navbar-expanded');
                    },0)
                })
                $('.navbar .collapse').on('hide.bs.collapse', function () {
                    $('body').removeClass('navbar-expanded');
                })
            });


            // overlay
            $(function() {
                const $toggler = $('.overlay-toggler'),
                    $sidebar = $('.overlay'),
                    $body = $('body');

                $toggler.on('click', function() {
                    $body.toggleClass("overlay-expanded");
                });

                $(document).on("click", function(event){
                    if(!$(event.target).closest(".overlay, .overlay-toggler").length){
                        $body.removeClass("overlay-expanded");
                    }
                });
            });

        }
    };

    $(document).ready(function () {
        fn.Launch();
    });


})(jQuery);

$( document ).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST'
    });
});

$(document).on('click', '.delete-item-button', function(e) {
    e.preventDefault();

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-primary mx-2',
            cancelButton: 'btn btn-danger mx-2'
        },
        buttonsStyling: false
    });

    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "DELETE",
                dataType: "json",
                url: $(this).data('delete-url'),
                success: data => {
                    $('#signupModal').modal('hide');
                    item.remove();

                    swalWithBootstrapButtons.fire({
                        title: 'Deleted!',
                        text: data.message,
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 1000
                    })
                },
                error: jqXHR => {
                    swalWithBootstrapButtons.fire({
                        title: jqXHR.responseJSON.error,
                        text: 'Something went wrong. Try again!',
                        icon: 'error',
                        showConfirmButton: false,
                        timer: 1000
                    })
                },
            });
        }
    })
});

'use strict';

window.DataTableConstruct = (tableId, options) => {
    try {
        let datatable = $('#' + tableId);
        window.reorder = datatable.data('reorder') ? datatable.data('reorder') : null;
        window.events = datatable.data('events') ? datatable.data('events') : null;

        PrimarySetup();
        window['jsTable' + tableId] = datatable.DataTable(options);
        DatabaseActions(window['jsTable' + tableId]);
    } catch (e) {
        console.log(e.toString());
    }
};

let PrimarySetup = () => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST'
    });
};

let DatabaseActions = table => {
    $('body').on('click', '.dt_delete', function (e) {
        e.preventDefault();

        let s = $(this);
        let url = s.attr('href');
        Swal.fire({
            title: s.data('delete-text'),
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: s.data('confirmButton-text') === 'undefined' ? "Yes, delete it!" : s.data('confirm-text'),
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            animation: !1,
            customClass: {
                container: 'restyle-delete-form',
                confirmButton: 'btn btn-pill btn-elevate-air btn-icon-lg',
                cancelButton: 'btn btn-pill btn-elevate-air btn-icon-lg'
            },
            reverseButtons: !0
        }).then(function (e) {
            if (!e.value) {
                return false;
            }
            $.ajax({
                url: url,
                type: 'DELETE',
                dataType: 'json',
                success: function (data) {
                    toastr.success('Node was Deleted');
                    table
                        .row( s.parents('tr') )
                        .remove()
                        .draw();
                },
                error: function(jqXHR) {
                    toastr.error(jqXHR.responseText);
                    console.log('An error has occured: ' + jqXHR.responseText);
                }
            });
        });
    });

    $('body').on('click', '.dt_pause', function (e) {
        e.preventDefault();

        let s = $(this);
        let url = s.attr('href');
        Swal.fire({
            title: s.data('delete-text'),
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: s.data('confirmButton-text') === 'undefined' ? "Yes, pause it!" : s.data('confirm-text'),
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            animation: !1,
            customClass: {
                container: 'restyle-delete-form',
                confirmButton: 'btn btn-pill btn-elevate-air btn-icon-lg',
                cancelButton: 'btn btn-pill btn-elevate-air btn-icon-lg'
            },
            reverseButtons: !0
        }).then(function (e) {
            if (!e.value) {
                return false;
            }
            $.ajax({
                url: url,
                type: 'PUT',
                dataType: 'json',
                success: function (data) {
                    toastr.success('Node was Deleted');
                    table.draw();
                },
                error: function(jqXHR) {
                    toastr.error(jqXHR.responseText);
                    console.log('An error has occured: ' + jqXHR.responseText);
                }
            });
        });
    });

    if (events) {
        $.each(events, function (eventType, inputs) {
            $.each(inputs, function (key, id) {
                var searchDelay = null;

                $('#' + id).on(eventType, function () {
                    clearTimeout(searchDelay);

                    searchDelay = setTimeout(function() {
                        table.ajax.reload();
                    }, 1000);
                });
            });
        });
    }

    if (reorder) {
        table.on( 'row-reorder', function ( e, diff) {
            var data = {};
            data['startPosition'] = table.page.info()['start'];
            data['reorder'] = {};

            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                data['reorder'][diff[i].oldData] = diff[i].newData;
            }

            $.post(reorder, JSON.stringify(data));
        } );
    }
}

let MassDeleteAction = (table, url) => {
    window.iDs = getSelectedIds(table);

    if (iDs.length == 0) {
        toastr.error('No row selected');
    } else {
        Swal.fire({
            title: 'Please confirm deletion',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            animation: !1,
            customClass: {
                confirmButton: 'btn btn-pill btn-elevate-air btn-icon-lg',
                cancelButton: 'btn btn-pill btn-elevate-air btn-icon-lg'
            },
            reverseButtons: !0
        }).then(function (e) {
            if (!e.value) {
                return false;
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: {iDs: JSON.stringify(iDs.join(','))}, // form.serialize()
                success: data => {
                    let tableId = table.tables().nodes().to$().attr('id');
                    $('input:checkbox:checked', '#' + tableId + ' tbody').parents('tr').attr('class', 'todelete');

                    jsTablew0
                        .rows( '.todelete' )
                        .remove()
                        .draw();

                    toastr.success(data);
                },
                error: jqXHR => {
                    toastr.error(jqXHR.responseText);
                },
            });
        });
    }
}

// Input types

let DatePickerController = function () {
    let DatePickerConstruct = () => {
        let t = {leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'};
        $('.datepicker').datepicker({
            rtl: !1,
            todayHighlight: !0,
            orientation: "bottom left",
            templates: t,
            format: 'yyyy-mm-dd'
        });
    };

    return {
        init() {
            if ($('.datepicker').length > 0) {
                DatePickerConstruct();
            }
        }
    }
}();

let BootstrapSelectController = function () {
    return {
        init() {
            let el = $('.bs_select');
            if (el.length > 0) {
                el.selectpicker();
            }
        }
    }
}();

window.onload = function () {
    if (window.jQuery) {
        jQuery(document).ready(() => {
            DatePickerController.init();
            BootstrapSelectController.init();
        });
    }
};

validation(
    '#register-form',
    {
        'title': {required: true, maxlength: 255},
        'name': {required: true, maxlength: 255},
        'email': {required: true, maxlength: 255, email: true},
        'country_id': {required: true, maxlength: 255},
        'city_id': {required: true, maxlength: 255},
        'address': {required: true, maxlength: 255},
        'password': {required: true, minlength: 8},
        'password_confirmation': {required: true, minlength : 8, equalTo : "#password"},
        'accept': {required: true}
    },
    function(data, valid_form, btn) {
        valid_form.trigger('reset');
        // toastr.success(data.message);
        btn.html(btn.data('original-text')).prop('disabled', false);

        // refreshReCaptchaV3('online_register_id','default');
        window.location.replace(data.redirect);
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.error);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

validation(
    '#login-form',
    {
        'email': {required: true, maxlength: 255, email: true},
        'password': {required: true, minlength: 8},
    },
    function(data, valid_form, btn) {
        valid_form.trigger('reset');
        // toastr.success(data.message);
        btn.html(btn.data('original-text')).prop('disabled', false);

        // refreshReCaptchaV3('online_register_id','default');
        window.location.replace(data.redirect);
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.errors);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

validation(
    '#forgot-password-form',
    {
        'email': {required: true, maxlength: 255, email: true},
    },
    function(data, valid_form, btn) {
        valid_form.trigger('reset');
        btn.html(btn.data('original-text')).prop('disabled', false);

        confmodal(data.message);
        // refreshReCaptchaV3('online_register_id','default');
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.error);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

validation(
    '#reset-password-form',
    {
        'email': {required: true, maxlength: 255, email: true},
        'password': {required: true, minlength: 8},
        'password_confirmation': {required: true, minlength : 8, equalTo : "#password"}
    },
    function(data, valid_form, btn) {
        valid_form.trigger('reset');
        btn.html(btn.data('original-text')).prop('disabled', false);

        // refreshReCaptchaV3('online_register_id','default');
        confmodal(data.message);
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.error);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

validation(
    '#message-form',
    {
        'message': {required: true, maxlength: 2000},
    },
    function(data, valid_form, btn) {
        valid_form.trigger('reset');
        btn.html(btn.data('original-text')).prop('disabled', false);

        // refreshReCaptchaV3('online_register_id','default');
        confmodal('Message was sent');
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.error);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

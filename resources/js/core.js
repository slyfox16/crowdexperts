$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    method: 'POST'
});

window.confmodal = (text) => {
    $('#modal-text').html(text);
    $('#confirmationModal').modal('show');
}

window.validation = (id, rules, success, error) => {
    const valid_form = $(id);

    const valid_val = valid_form.validate({
        errorClass: "text-danger",
        errorElement: 'span',
        wrapper: false,
        onkeyup: false,
        onchange: false,
        onfocusout: function (element) {
            this.element(element);
        },
        rules: rules,
        errorPlacement: function (error, element) {
            var fieldType = $(element).prop("type");

            if (fieldType == 'checkbox') {
                error.insertAfter(element.closest('.checkbox__custom'));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            if (element.type === "checkbox") {
                this.findByName(element.name).parent().removeClass(errorClass).parent().addClass(errorClass);
            } else {
                $(element).parent().addClass(errorClass).removeClass(validClass);
            }
        },
        unhighlight: function (element, errorClass, validClass) {
            if (element.type === "checkbox") {
                this.findByName(element.name).parent().removeClass(errorClass).addClass(validClass);
            } else {
                $(element).parent().removeClass(errorClass).addClass(validClass);
            }
        },
        submitHandler: function (form) {
            const btn = $('.form_submit_button', valid_form);
            const loadingText = btn.attr('loadingText');
            btn.data('original-text', btn.html());
            btn.html(loadingText).prop('disabled', true);

            var formData = new FormData($(form)[0]);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: $(form).attr('action'),
                data: formData,
                processData: false,
                contentType: false,
                success: data => {
                    success(data, valid_form, btn);
                },
                error: jqXHR => {
                    error(jqXHR, valid_val, btn);
                },
            });
        }
    });
}

function formatRepo (repo) {
    return repo.speciality;
}

function formatRepoSelection(repo) {
    if (!repo.speciality) {
        return repo.text;
    }

    return repo.speciality;
}

function countryFormatSelection(repo) {
    if (!repo.namede) {
        return repo.text;
    }

    return repo.namede;
}

function countryFormatResult(repo) {
    if (!repo.namede) {
        return repo.text;
    }

    return repo.namede;
}

function cityFormatSelection(repo) {
    return repo.ascii || repo.text;
}

function cityFormatResult(repo) {
    if (!repo.ascii) {
        return repo.text;
    }

    return repo.ascii;
}

require('./bootstrap');
// require('./project');

// require('alpinejs');

window.Vue = require('vue');
Vue.use(require('vue-chat-scroll'));

Vue.component('chat-component', require('./components/ChatComponent.vue').default);

const app = new Vue({
    el: '#app',
});

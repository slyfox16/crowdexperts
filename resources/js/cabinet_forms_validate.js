validation(
    '#user-info-form',
    {
        'name': {required: true, maxlength: 255},
        'username': {required: true, maxlength: 255},
        'email': {required: true, maxlength: 255, email: true},
    },
    function(data, valid_form, btn) {
        btn.html(btn.data('original-text')).prop('disabled', false);
        $('#username').html(data.name);

        // refreshReCaptchaV3('online_register_id','default');
        confmodal(data.message);
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.error);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

validation(
    '#contacts-form',
    {
        'facebook_url': {maxlength: 255, url: true},
        'linkedin_url': {maxlength: 255, url: true},
        'twitter_url': {maxlength: 255, url: true},
        'xing_url': {maxlength: 255, url: true},
    },
    function(data, valid_form, btn) {
        btn.html(btn.data('original-text')).prop('disabled', false);

        // refreshReCaptchaV3('online_register_id','default');
        confmodal(data.message);
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.error);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

validation(
    '#certificates-form',
    {
        'certificate_id': {required: true},
        'date': {required: true},
    },
    function(data, valid_form, btn) {
        valid_form.trigger('reset');
        btn.html(btn.data('original-text')).prop('disabled', false);
        $('#certificates').append(data.cert);

        // refreshReCaptchaV3('online_register_id','default');
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.error);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

validation(
    '#completed-projects-form',
    {
        'name': {required: true, maxlength: 255},
        'image': {required: true, maxlength: 255},
        'description': {required: true, maxlength: 2000},
        'date': {required: true},
        'link': {maxlength: 255, url: true},
    },
    function(data, valid_form, btn) {
        valid_form.trigger('reset');
        btn.html(btn.data('original-text')).prop('disabled', false);
        $('#projects').append(data.cert);

        confmodal(data.message);

        // refreshReCaptchaV3('online_register_id','default');
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.error);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

validation(
    '#event-form',
    {
        'title': {required: true, maxlength: 255},
        'description': {required: true, maxlength: 2000},
        'facebook_url': {maxlength: 255, url: true},
        'twitter_url': {maxlength: 255, url: true},
        'xing_url': {maxlength: 255, url: true},
        'web_url': {maxlength: 255, url: true},
        'daterange': {required: true},
        'address': {maxlength: 255},
    },
    function(data, valid_form, btn) {
        valid_form.trigger('reset');
        btn.html(btn.data('original-text')).prop('disabled', false);

        // refreshReCaptchaV3('online_register_id','default');
        confmodal(data.message);
    },
    function(jqXHR, valid_val, btn) {
        valid_val.showErrors(jqXHR.responseJSON.error);
        btn.html(btn.data('original-text')).prop('disabled', false);
    },
);

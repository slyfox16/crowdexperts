<?php

return [
    'facebook' => '<a href=":url" class="social-button :class" id=":id" title=":title">Facebook</a>',
    'twitter' => '<a href=":url" class="social-button :class" id=":id" title=":title">Twitter</a>',
    'xing' => '<a target="_blank" href=":url" class="social-button :class" id=":id" title=":title">Xing</a>',
    'linkedin' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-linkedin"></span></a></li>',
    'whatsapp' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-whatsapp"></span></a></li>',
    'pinterest' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-pinterest"></span></a></li>',
    'reddit' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-reddit"></span></a></li>',
    'telegram' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><span class="fab fa-telegram"></span></a></li>',
];

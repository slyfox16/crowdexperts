<div>
    @if($users->isNotEmpty())
        <ul class="avatar-list text-light">
            @foreach($users as $user)
                <li>
                    <a href="{{ route('profile', $user) }}">
                        <img src="{{ schema_asset($user->resize('avatar', 476, 476)) }}" alt="Avatar" class="avatar" data-toggle="tooltip"
                             data-placement="top" data-original-title="{{ $user->name }}">
                    </a>
                </li>
            @endforeach
        </ul>
    @else
        No users yet
    @endif
</div>

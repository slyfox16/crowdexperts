@section('sidebar')
    @livewire('filter')
@endsection

<div id="app" class="main">

    @include('frontend.partials.layouts.header')

    <section class="page-title">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                    <h1 class="font-weight-bold">Jetzt die besten Crowd Experten für Ihr Projekt finden</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Experts</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <ul class="page-options">
                        <li>
                            <div class="dropdown">
                                <button class="btn btn-primary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ data_get($orderList, $order) }} <i class="icon-chevron-down"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach($orderList as $key => $value)
                                        <a wire:click.prevent="order('{{ $key }}')" class="dropdown-item" href="#">{{ $value }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                        <li>
                            <button class="btn btn-icon btn-primary overlay-toggler" type="button">
                                <i class="icon-search"></i>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-light">
        <div class="container mt-0">
            <div class="row">
                <div class="col">
                    <ul class="list-group list-group-lg">
                        @forelse ($expertsList as $expert)
                            <li class="list-group-item">
                                <div class="row gutter-3 gutter-lg-4 justify-content-center align-items-center">
                                    <div class="col-6 col-md-3 col-lg-2">
                                        <a href="{{ route('profile', $expert) }}" class="card circle equal hover-opacity">
                                            <figure class="image" style="background-image: url('{{ schema_asset($expert->resize('avatar', 476, 476)) }}')"></figure>
                                        </a>
                                    </div>
                                    <div class="col-10 col-md-6 col-lg-5 text-center text-md-left">
                                        <h2 class="mb-0 font-weight-bold">
                                            <a href="{{ route('profile', $expert) }}">
                                                {{ $expert->name }}
                                            </a>
                                        </h2>
                                        <small>{{ $expert->getCountryCity() }}</small>
                                        <p class="mt-1 text-muted">
                                            {{ $expert->description }}
                                        </p>
                                    </div>
                                    @if($specialities = $expert->specialities)
                                        <div class="col-md-3 col-lg-5 text-center text-lg-right">
                                            @foreach($specialities as $speciality)
                                                <div class="pin" data-toggle="tooltip" data-placement="top" title="{{ $speciality->speciality }}">
                                                    {{ $speciality->abbr }}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </li>
                        @empty
                            <h1>No users</h1>
                        @endforelse
                    </ul>
                </div>
            </div>

            @if($showButton)
                <div class="row">
                    <div class="col text-center">
                        <a wire:click="load" class="btn btn-outline-primary">Load More</a>
                    </div>
                </div>
            @endif
        </div>
    </section>
</div>

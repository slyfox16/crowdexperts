<aside class="overlay">
    <button class="overlay-toggler burger text-black" type="button">
        <span></span>
    </button>
    <div class="overlay-body">
        <div>
            <div class="row">
                <div class="col">
                    <h2 class="h1 mb-2 font-weight-bold">Search</h2>
                </div>
            </div>
            {!! form_start($filterForm) !!}
            <fieldset class="row gutter-2">
                <div class="col-md-6 col-lg-12">
                    {!! form_row($filterForm->name) !!}
                </div>
                <div wire:ignore class="col-md-6 col-lg-12">
                    {!! form_row($filterForm->city_id) !!}
                </div>
                <div wire:ignore class="col-12">
                    {!! form_row($filterForm->certificate_id) !!}
                </div>
{{--                <div class="col-12">--}}
{{--                    {!! form_row($filterForm->rating) !!}--}}
{{--                </div>--}}
            </fieldset>

            <input type="submit" hidden id="submit-hidden">
            {!! form_end($filterForm, false) !!}
        </div>
    </div>
    <div class="overlay-footer">
        <div>
            {!! form_row($filterForm->send) !!}
        </div>
    </div>
</aside>

@script
<script>
    $('#filter-form-submit').on('click', function (e) {
        $('#submit-hidden').click();
    });

    $('#city-id').on('change', function (e) {
        let elementName = $(this).attr('name');
        @this.set('filter.' + elementName, e.target.value);
    });

    $('#certificate-id').on('change', function (e) {
        let elementName = $(this).attr('name');
        @this.set('filter.' + elementName, e.target.value);
    });
</script>
@endscript


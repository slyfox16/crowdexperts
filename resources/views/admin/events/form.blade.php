@extends('amigo-pub::layouts.admin')

@section('buttons')
    <button class="pre_click1 btn btn-secondary" name="save" value="redirect">
        Save and exit
    </button>
    <button class="pre_click2 btn btn-primary" name="save" value="refresh">Save</button>
@endsection

@section('head-container')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    @if(isset($user))
                        {{ $user->name }}
                    @else
                        Create new Users
                    @endif
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/admin/menu">Menu</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">{{ isset($mainModel->id) ? 'Edit' : 'Create' }} menu</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!--begin::Form-->
    {!! form_start($form) !!}

    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="block block-fx-shadow">
                <div class="block-content">
                    {!! form_until($form, 'active') !!}
                </div>
            </div>
        </div>
    </div>

    <div style="display:none;">
        <button class="click1 btn btn-outline-primary mr-3 ml-4" name="save" value="redirect">
            Save and exit
        </button>
        <button class="click2 btn btn-primary mr-3" name="save" value="refresh">Save</button>
    </div>

    {!! form_end($form, false) !!}
    <!--end::Form-->
@stop

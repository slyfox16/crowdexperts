<div class="btn-group">
    <a href="{{ route('amigo.events.edit', $event) }}" class="btn btn-sm btn-alt-primary js-tooltip-enabled">
        <i class="fa fa-fw fa-pencil-alt"></i>
    </a>
    <a class="btn btn-sm btn-alt-primary js-tooltip-enabled dt_delete" href="{{ route('amigo.events.destroy', $event) }}" data-delete-text="Remove item">
        <i class="fa fa-fw fa-times"></i>
    </a>
</div>

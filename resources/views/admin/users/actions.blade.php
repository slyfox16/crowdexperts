<div class="btn-group">
    <a href="{{ route('amigo.users.edit', $user) }}" class="btn btn-sm btn-alt-primary js-tooltip-enabled">
        <i class="fa fa-fw fa-pencil-alt"></i>
    </a>
    <a class="btn btn-sm btn-alt-primary js-tooltip-enabled dt_delete" href="{{ route('amigo.users.destroy', $user) }}" data-delete-text="Remove item">
        <i class="fa fa-fw fa-times"></i>
    </a>
</div>

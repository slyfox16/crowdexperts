@extends('amigo-pub::layouts.admin')

@section('buttons', '')

@section('head-container')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Administrators
                </h1>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded">
                <div class="block-content block-content-full">
                    <?php Table::widget([
                        'url' => route('amigo.users.list'),
                        'dom' => "<'row'<'col-sm-12'tr>><'row'<'col-sm-6'i><'col-sm-6'p>>",
                        'lengthChange' => false,
                        'pageLength' => 50,
                        'columns' => [
                            [
                                'data' => 'id',
                                'title' => 'ID',
                                'sortable' => false,
                            ],
                            [
                                'data' => 'name',
                                'title' => 'Name',
                                'sortable' => false,
                            ],
                            [
                                'data' => 'email',
                                'title' => 'Email',
                                'sortable' => false,
                            ],
                            [
                                'data' => 'avatar',
                                'title' => 'Avatar',
                                'sortable' => false,
                            ],
                            [
                                'data' => 'username',
                                'title' => 'Username',
                                'sortable' => false,
                            ],
                            [
                                'data' => 'expert_confirm',
                                'title' => 'Expert confirm',
                                'sortable' => false,
                            ],
                            [
                                'data' => 'active',
                                'title' => 'Active',
                                'sortable' => false,
                            ],
                            [
                                'data' => 'date_joined',
                                'title' => 'Date joined',
                                'sortable' => false,
                            ],
                            [
                                'data' => 'actions',
                                'title' => 'Actions',
                                'width' => '180px',
                                'sortable' => false,
                            ],
                        ],
                        "language" => [
                            "emptyTable" => "There are no tickets here!",
                            "oPaginate" => [
                                "sNext" => '<i class="fa fa-angle-right"></i>',
                                "sPrevious" => '<i class="fa fa-angle-left"></i>',
                                "sFirst" => '<i class="fa fa-angle-double-left"></i>',
                                "sLast" => '<i class="fa fa-angle-double-right"></i>'
                            ],
                        ],
                        'select' => false,
                        'tableOptions' => [
                            'class' => "table table-bordered table-striped table-vcenter js-dataTable-full dataTable no-footer"
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
@stop

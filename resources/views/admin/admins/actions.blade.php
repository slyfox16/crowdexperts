<div class="btn-group">
    <a href="{{ route('amigo.admins.edit', $admin) }}" class="btn btn-sm btn-alt-primary js-tooltip-enabled">
        <i class="fa fa-fw fa-pencil-alt"></i>
    </a>
    @if ($admin->canDeleteAdmin())
        <a class="btn btn-sm btn-alt-primary js-tooltip-enabled dt_delete" href="{{ route('amigo.admins.destroy', $admin) }}" data-delete-text="Remove item">
            <i class="fa fa-fw fa-times"></i>
        </a>
    @endif
</div>

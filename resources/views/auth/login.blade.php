@extends('frontend.layouts.guest')

@section('content')
    @include('frontend.partials.layouts.header')


    <!-- page title -->
    <section class="page-title">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Login</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>


    <section class="bg-light overflow-hidden">
        <div class="container">
            <div class="row gutter-6 justify-content-between">
                <div class="col-lg-6">
                    <div class="row gutter-4">
                        <div class="col-12">
                            <h1>Einloggen mit Ihrer E-Mail Adresse</h1>
                        </div>

                        <div class="col-12">
                            {!! form_start($formLogin) !!}

                                <div id="login_form_id"></div>

                                <fieldset class="row gutter-2">
                                    <div class="col-12">
                                        {!! form_row($formLogin->email) !!}
                                    </div>
                                    <div class="col-12">
                                        {!! form_row($formLogin->password) !!}
                                    </div>
                                    <input type="submit" hidden id="submit-hidden">
                                </fieldset>

                            {!! form_end($formLogin, false) !!}
                        </div>
                        <div class="col-12">
                            <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                                {{ __('Forgot your password?') }}
                            </a>
                        </div>

                        <div class="col-12">
                            <a id="filter-form-submit" href="" class="btn btn-primary btn-with-icon">Login <i class="icon-arrow-right"></i></a>
                        </div>
                    </div>

                </div>

                <div class="col-lg-4 split">
                    <h3>Einfach anmelden über ihren Social Account</h3>
                    <ul class="list-group list-group-minimal list-group-xs">
                        <li class="list-group-item">
                            <a href="{{ route('auth.facebook') }}" class="btn btn-block btn-social btn-lg btn-facebook">
                                <i class="fa fa-facebook"></i>Sign in with Facebook
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('auth.linkedin') }}" class="btn btn-block btn-social btn-lg btn-linkedin">
                                <i class="fa fa-linkedin"></i>Sign in with Linkedin
                            </a>
                        </li>
{{--                        <li class="list-group-item">--}}
{{--                            <a href="{{ route('auth.xing') }}" class="underline text-xing font-weight-bold">via Xing</a>--}}
{{--                        </li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </section>


    @php Asset::js(mix('/js/login_register_validate.js')) @endphp

    {!! GoogleReCaptchaV3::renderOne('login_form_id', 'default') !!}
@endsection

@script
<script>
    $('#filter-form-submit').on('click', function (e) {
        e.preventDefault();
        $('#submit-hidden').click();
    });
</script>
@endscript

<style>
    .btn-group-lg>.btn, .btn-lg {
        padding: 0.5rem 2.5rem !important;
        padding-left: 61px !important;
    }
</style>

@extends('frontend.layouts.guest')

@section('content')
    @include('frontend.partials.layouts.header')


    <!-- page title -->
    <section class="page-title">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Reset password</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>


    <section class="bg-light overflow-hidden">
        <div class="container">
            <div class="row gutter-6 justify-content-between">
                <div class="col-lg-6">
                    <div class="row gutter-4">
                        <div class="col-12">
                            <h1>Einloggen mit Ihrer E-Mail Adresse</h1>
                        </div>

                        <div class="col-12">
                            {!! form_start($resetLogin) !!}
                            <fieldset class="row gutter-2">
                                <div class="col-12">
                                    {!! form_row($resetLogin->email) !!}
                                </div>
                                <input type="submit" hidden id="submit-hidden">
                            </fieldset>

                            {!! form_end($resetLogin, false) !!}
                        </div>

                        <div class="col-12">
                            <a id="filter-form-submit" href="" class="btn btn-primary btn-with-icon">Login <i class="icon-arrow-right"></i></a>
                        </div>
                    </div>

                </div>

                <div class="col-lg-4 split">
                    <h3>Einfach anmelden über ihren Social Account</h3>
                </div>
            </div>
        </div>
    </section>

    @php Asset::js(mix('/js/login_register_validate.js')) @endphp
@endsection

@script
<script>
    $('#filter-form-submit').on('click', function (e) {
        e.preventDefault();
        $('#submit-hidden').click();
    });
</script>
@endscript


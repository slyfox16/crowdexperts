@extends('frontend.layouts.guest')

@section('content')
    @include('frontend.partials.layouts.header')

    <!-- page title -->
    <section class="page-title">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Register</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>


    <section class="bg-light overflow-hidden">
        <div class="container">
            <div class="row gutter-6 justify-content-between">
                <div class="col-lg-6">
                    <div class="row gutter-4">
                        <div class="col-12">
                            <h1>Melden Sie sich jetzt kostenfrei als Experte(in) an</h1>
                        </div>

                        <div class="col-12">
                            {!! form_start($formRegister) !!}

                                <div id="register_form_id"></div>

                                <fieldset class="row gutter-2">
                                    <div class="col-md-4">
                                        {!! form_row($formRegister->title) !!}
                                    </div>
                                    <div class="col-md-8">
                                        {!! form_row($formRegister->name) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($formRegister->email) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row($formRegister->country_id) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row($formRegister->city_id) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($formRegister->address) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row($formRegister->password) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row($formRegister->password_confirmation) !!}
                                    </div>
                                    <div class="col-12">
                                        <div class="custom-control custom-checkbox checkbox__custom">
                                            {!! form_widget($formRegister->accept) !!}
{{--                                            <input type="checkbox" class="custom-control-input" id="customCheck1">--}}
                                            <label class="custom-control-label small text-muted" for="customCheck1">I accept the <a href="" class="underline">Terms & Conditions</a> and acknowledge that my information will be used in accordance with Privacy Policy.</label>
                                        </div>
                                    </div>
                                    <input type="submit" hidden id="submit-hidden">
                                </fieldset>
                            {!! form_end($formRegister, false) !!}
                        </div>

                        <div class="col-12">
                            <a id="filter-form-submit" href="" class="btn btn-primary btn-with-icon">Register <i class="icon-arrow-right"></i></a>
                        </div>
                    </div>

                </div>

                <div class="col-lg-4 split">
                    <h3>Sie können auch Ihren existierenden social account nutzen, um sich anzumelden</h3>
                    <ul class="list-group list-group-minimal list-group-xs">
                        <li class="list-group-item">
                            <a href="{{ route('auth.facebook') }}" class="btn btn-block btn-social btn-lg btn-facebook">
                                <i class="fa fa-facebook"></i>Sign in with Facebook
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('auth.linkedin') }}" class="btn btn-block btn-social btn-lg btn-linkedin">
                                <i class="fa fa-linkedin"></i>Sign in with Linkedin
                            </a>
                        </li>
{{--                        <li class="list-group-item">--}}
{{--                            <a href="{{ route('auth.xing') }}" class="underline text-xing font-weight-bold">via Xing</a>--}}
{{--                        </li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </section>

    @php Asset::js(mix('/js/login_register_validate.js')) @endphp

    {!! GoogleReCaptchaV3::renderOne('register_form_id', 'default') !!}
@endsection

@script
<script>
    $('#filter-form-submit').on('click', function (e) {
        e.preventDefault();
        $('#submit-hidden').click();
    });
</script>
@endscript

<style>
    .btn-group-lg>.btn, .btn-lg {
        padding: 0.5rem 2.5rem !important;
        padding-left: 61px !important;
    }
</style>

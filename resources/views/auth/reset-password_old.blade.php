@extends('frontend.layouts.guest')

@section('content')
    <!-- hero -->
    <section class="cover overflow-hidden">
        <div class="container foreground">
            <div class="row align-items-center justify-content-left justify-content-lg-end vh-lg-100">
                <div class="col-md-8 col-lg-6">
                    <h1 class="font-weight-bold mb-8">Melden Sie sich jetzt kostenfrei als Experte(in) an</h1>
                    {!! form_start($resetPassword) !!}

                    {!! form_row($resetPassword->token) !!}

                    <fieldset class="row gutter-2">
                        <div class="col-md-12">
                            {!! form_row($resetPassword->email) !!}
                        </div>
                    </fieldset>

                    <fieldset class="row gutter-2">
                        <div class="col-md-12">
                            {!! form_row($resetPassword->password) !!}
                        </div>

                        <div class="col-md-12">
                            {!! form_row($resetPassword->password_confirmation) !!}
                        </div>
                    </fieldset>

                    <div class="row">
                        {!! form_row($resetPassword->send) !!}
                    </div>

                    {!! form_end($resetPassword, false) !!}
                </div>
            </div>
        </div>
        <div class="container-fluid background vh-lg-100">
            <div class="row">
                <div class="col-lg-5 vh-lg-100 px-0" data-aos="fade-in" data-aos-delay="150">
                    <div class="card card-fill equal text-white">
                        <figure class="image image-overlay" style="background-image: url('/images/portal.jpg')"></figure>
                        <div class="card-header">
                            <div>
                                <a href="{{ route('home') }}" class="navbar-brand"><img src="{{ schema_asset('images/logo.svg') }}" alt="Logo"></a>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div>
                                <p>
                                    Innovation <br>
                                    Finanzierung <br>
                                    Sourcing
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php Asset::js(mix('/js/login_register_validate.js')) @endphp
@endsection

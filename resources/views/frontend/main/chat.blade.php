@extends('frontend.layouts.app')

@section('header')
    <!-- page title -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="font-weight-bold">Messages</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')

    <!-- inbox -->
    <section class="bg-light">
        <div class="container mt-0">
            <div class="row gutter-1 gutter-lg-4">
                <div class="col-lg-4">
                    <div class="row gutter-1">
                        <div class="col-2 col-md-1 col-lg-12">
                            <div class="chat active">
                                <img class="chat-photo" src="{{ schema_asset('images/team-square-1.jpg') }}" alt="">
                                <span class="chat-name">Karin Schmidt</span>
                            </div>
                        </div>
                        <div class="col-2 col-md-1 col-lg-12">
                            <div class="chat">
                                <img class="chat-photo" src="{{ schema_asset('images/team-square-3.jpg') }}" alt="">
                                <span class="chat-name">Andreas Pages</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">

                    <div class="chat-options">
                        <div class="row align-items-center">
                            <div class="col">
                                <span class="p-2">Karin Schmidt</span>
                            </div>
                            <div class="col text-right">
                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button" class="btn btn-icon btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon-more-vertical"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                            <a class="dropdown-item" href="#">Block</a>
                                            <a class="dropdown-item" href="#">Clear Chat</a>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-icon btn-light"><i class="icon-x"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chat-panel">
                        <div class="chat-body">
                            <div class="message">
                                <div class="message-content">
                                    <p>Hello, your reservation is confirmed!</p>
                                    <time datetime="2019-08-24 11:00">11:00 pm</time>
                                </div>
                            </div>

                            <div class="message message-flip">
                                <div class="message-content">
                                    <p>Sounds good!</p>
                                    <time datetime="2019-08-24 11:00">11:00 pm</time>
                                </div>
                            </div>

                            <div class="message">
                                <div class="message-content">
                                    <p>Hello, your reservation is confirmed!</p>
                                    <time datetime="2019-08-24 11:00">11:00 pm</time>
                                </div>
                            </div>

                            <div class="message">
                                <div class="message-content">
                                    <p>Hello, your reservation is confirmed!</p>
                                    <time datetime="2019-08-24 11:00">11:00 pm</time>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="new-message">
                        <div class="form-group text-right">
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="1" placeholder="Your message here.."></textarea>
                            <a href="" class="btn btn-block btn-primary">Send Message</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

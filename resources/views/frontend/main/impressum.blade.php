@extends('frontend.layouts.app')

@section('header', '')

@section('content')
    <!-- page title -->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="font-weight-bold mb-1">Impressum</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Impressum</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>


    <!-- inbox -->
    <section class="bg-light">
        <div class="container mt-0">
            <div class="row gutter-1 gutter-lg-4">
                <div class="col-md-6 col-lg-4">
                    <h3>Anbieterkennzeichung i.S.d. § 5 Telemediengesetz (TMG):</h3>
                    <p class="text-secondary">Deutscher Crowdsourcing Verband e.V.</p>
                    <p class="text-secondary">
                        Vereinsanschrift:<br>
                        DCV e.V. &amp; German Crowdfunding Network<br>
                        Martin-Luther-Straße 8<br>
                        10777 Berlin
                    </p>
                    <p class="text-secondary">E-Mail: info@crowdsourcingverband.de</p>
                    <p class="text-secondary">Der DCV e.V. ist beim Amtsgericht Charlottenburg unter der VR Nummer 34911
                        eingetragen.</p>
                </div>
                <div class="col-md-6 col-lg-4">
                    <h3>Vorstände Deutscher Crowdsourcing Verband e.V.</h3>
                    <p class="text-secondary">
                        Vorstandsvorsitzender: Dr. Michael Gebert<br>
                        Stellv. Vorstandsvorsitzender: Roland Szabados<br>
                        Vorständin: Ines Zimzinski

                    </p>
                </div>
                <div class="col-md-6 col-lg-4">
                    <h3>Kontoverbindung:</h3>
                    <p class="text-secondary">Deutscher Crowdsourcing Verband e.V.</p>
                    <p class="text-secondary">
                        IBAN: DE80 1005 0000 0190 3084 43<br>
                        BIC: BELADEBEXXX<br>
                        Berliner Sparkasse
                    </p>
                    <a href="https://www.crowdsourcingverband.de/datenschutz/">Datenschutzerklärung</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="accordion accordion-minimal accordion-minimal-sm" id="accordion-2">
                        <div class="card">
                            <div class="card-header" id="heading-2-1">
                                <h5 class="mb-0">
                                    <button class="btn btn-block" type="button" data-toggle="collapse" data-target="#collapse-2-1"
                                            aria-expanded="false" aria-controls="collapse-2-1">
                                        Inhalt des Onlineangebotes
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-2-1" class="collapse" aria-labelledby="heading-2-1" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Die Autoren übernehmen keinerlei Gewähr für die Aktualität, Korrektheit,
                                        Vollständigkeit oder Qualität der bereitgestellten Informationen, der angebotenen Online-Scans
                                        oder Downloads, die nicht aus eigenen Quellen stammen. Haftungsansprüche gegen die Autoren, welche
                                        sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung
                                        der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger
                                        Informationen verursacht wurden, sind grundsätzlich ausgeschlossen, sofern seitens des Autors kein
                                        nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt. Alle Angebote sind
                                        freibleibend und unverbindlich. Die Autoren behält es sich ausdrücklich vor, Teile der Seiten oder
                                        das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die
                                        Veröffentlichung zeitweise oder endgültig einzustellen.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-2">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-2" aria-expanded="false" aria-controls="collapse-2-2">
                                        Verweise und Links
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-2" class="collapse" aria-labelledby="heading-2-2" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Bei direkten oder indirekten Verweisen auf fremde Webseiten
                                        (“Hyperlinks”), die außerhalb des Verantwortungsbereiches des Autors liegen, würde eine
                                        Haftungsverpflichtung ausschließlich in dem Fall in Kraft treten, in dem die Autoren von den
                                        Inhalten Kenntnis hat und es ihm technisch möglich und zumutbar wäre, die Nutzung im Falle
                                        rechtswidriger Inhalte zu verhindern. Die Autoren erklärt hiermit ausdrücklich, dass zum Zeitpunkt
                                        der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die
                                        aktuelle und zukünftige Gestaltung, die Inhalte oder die Urheberschaft der gelinkten/verknüpften
                                        Seiten hat der Autor keinerlei Einfluss. Deshalb distanziert er sich hiermit ausdrücklich von
                                        allen Inhalten aller gelinkten /verknüpften Seiten, die nach der Linksetzung verändert wurden. Für
                                        illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der
                                        Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der
                                        Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige
                                        Veröffentlichung lediglich verweist.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-3">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-3" aria-expanded="false" aria-controls="collapse-2-3">
                                        Datenschutz
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-3" class="collapse" aria-labelledby="heading-2-3" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Sofern innerhalb des Internetangebotes die Möglichkeit zur Eingabe
                                        persönlicher oder geschäftlicher Daten (Emailadressen, Namen, Anschriften) besteht, so erfolgt die
                                        Preisgabe dieser Daten seitens des Nutzers auf ausdrücklich freiwilliger Basis. Die
                                        Inanspruchnahme und Bezahlung aller angebotenen Dienste ist – soweit technisch möglich und
                                        zumutbar – auch ohne Angabe solcher Daten bzw. unter Angabe anonymisierter Daten oder eines
                                        Pseudonyms gestattet. Die Nutzung der im Rahmen des Impressums oder vergleichbarer Angaben
                                        veröffentlichten Kontaktdaten wie Postanschriften, Telefon- und Faxnummern sowie Emailadressen
                                        durch Dritte zur Übersendung von nicht ausdrücklich angeforderten Informationen ist nicht
                                        gestattet. Rechtliche Schritte gegen die Versender von sogenannten Spam-Mails bei Verstößen gegen
                                        dieses Verbot sind ausdrücklich vorbehalten.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-4">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-4" aria-expanded="false" aria-controls="collapse-2-4">
                                        Urheberrecht
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-4" class="collapse" aria-labelledby="heading-2-4" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Der Betreiber der Seiten ist bemüht, stets die Urheberrechte anderer zu
                                        beachten bzw. auf selbst
                                        erstellte sowie lizenzfreie Werke zurückzugreifen.

                                        Die durch den Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem
                                        deutschen Urheberrecht. Beiträge Dritter sind als solche gekennzeichnet. Die Vervielfältigung,
                                        Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes
                                        bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien
                                        meiner Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet und dürfen nicht
                                        anderen öffentlich (ohne meine vorherige schriftliche Zustimmung) zugänglich gemacht werden.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-5">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-5" aria-expanded="false" aria-controls="collapse-2-5">
                                        Webanalysedienst – Google Analytics – Datenschutzpolicy
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-5" class="collapse" aria-labelledby="heading-2-5" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Diese Website benutzt Google Analytics, einen Webanalysedienst der Google
                                        Inc. (“Google”).

                                        Google Analytics verwendet sog. “Cookies”, Textdateien, die auf Ihrem Computer gespeichert werden
                                        und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie
                                        erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) wird
                                        an einen Server von Google in den USA übertragen und dort gespeichert.

                                        Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über
                                        die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der
                                        Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google
                                        diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben
                                        oder soweit Dritte diese Daten im Auftrag von Google verarbeiten.

                                        Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten von Google in Verbindung bringen. Sie
                                        können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software
                                        verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht
                                        sämtliche Funktionen dieser Website vollumfänglich nutzen können.

                                        Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen
                                        Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck
                                        einverstanden.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-6">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-6" aria-expanded="false" aria-controls="collapse-2-6">
                                        Verwendung von Facebook Social Plugins
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-6" class="collapse" aria-labelledby="heading-2-6" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Mein Internetauftritt verwendet Social Plugins (“Plugins”) des sozialen
                                        Netzwerkes facebook.com, welches von der Facebook Inc., 1601 S. California Ave, Palo Alto, CA
                                        94304, USA betrieben wird (“Facebook”). Die Plugins sind an einem der Facebook Logos erkennbar
                                        (weißes „f“ auf blauer Kachel oder ein „Daumen hoch“-Zeichen) oder sind mit dem Zusatz “Facebook
                                        Social Plugin” gekennzeichnet. Die Liste und das Aussehen der Facebook Social Plugins kann hier
                                        eingesehen werden: <a href="http://developers.facebook.com/plugins."
                                                              class="underline">http://developers.facebook.com/plugins.</a>

                                        Wenn Sie eine Webseite meines Internetauftritts aufrufen, die ein solches Plugin enthält, baut Ihr
                                        Browser eine direkte Verbindung mit den Servern von Facebook auf. Der Inhalt des Plugins wird von
                                        Facebook direkt an Ihren Browser übermittelt und von diesem in die Webseite eingebunden. Ich haben
                                        daher keinen Einfluss auf den Umfang der Daten, die Facebook mit Hilfe dieses Plugins erhebt und
                                        informiere Sie daher entsprechend meinem Kenntnisstand:

                                        Durch die Einbindung der Plugins erhält Facebook die Information, dass Sie die entsprechende Seite
                                        meines Internetauftritts aufgerufen haben. Sind Sie bei Facebook eingeloggt, kann Facebook den
                                        Besuch Ihrem Facebook-Konto zuordnen. Wenn Sie mit den Plugins interagieren, zum Beispiel den Like
                                        Button betätigen oder einen Kommentar abgeben, wird die entsprechende Information von Ihrem
                                        Browser direkt an Facebook übermittelt und dort gespeichert. Falls Sie kein Mitglied von Facebook
                                        sind, besteht trotzdem die Möglichkeit, dass Facebook Ihre IP-Adresse in Erfahrung bringt und
                                        speichert.

                                        Zweck und Umfang der Datenerhebung und die weitere Verarbeitung und Nutzung der Daten durch
                                        Facebook sowie Ihre diesbezüglichen Rechte und Einstellungsmöglichkeiten zum Schutz Ihrer
                                        Privatssphäre entnehmen Sie bitte den Datenschutzhinweisen von Facebook: <a
                                            href="http://www.facebook.com/policy.php"
                                            class="underline">http://www.facebook.com/policy.php</a>
                                        .

                                        Wenn Sie Facebookmitglied sind und nicht möchten, dass Facebook über unseren Internetauftritt
                                        Daten über Sie sammelt und mit Ihren bei Facebook gespeicherten Mitgliedsdaten verknüpft, müssen
                                        Sie sich vor Ihrem Besuch unseres Internetauftritts bei Facebook ausloggen.

                                        Ebenfalls ist es möglich Facebook-Social-Plugins mit Add-ons für Ihren Browser zu blocken, zum
                                        Beispiel mit dem “Facebook Blocker“. – (Quelle: spreerecht.de)</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-7">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-7" aria-expanded="false" aria-controls="collapse-2-7">
                                        Verwendung der Social-Funktionen von Shareholic („Weitersagen heißt Unterstützen Slogan“) bzw. der
                                        dazugehörigen Social-Network Schaltflächen
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-7" class="collapse" aria-labelledby="heading-2-7" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Mein Internetauftritt benutzt das Social Plugin der Firma Shareaholic Inc.
                                        und die
                                        Shareaholic-Funktionen, welche von der Shareaholic, c/o Performable Inc., 486 Green Street,
                                        Cambridge, MA 02139, USA angeboten werden.</p>
                                    <p class="text-secondary">
                                        Mit dem Social Plugin der Firma Shareaholic Inc. ist es dem Besucher meiner Webseiten möglich,
                                        meine Artikel bzw. die Webseiten meines Internetauftritts in diverse soziale Netzwerke zu teilen,
                                        bzw. anderen Personen mit diesem Diensten zur Verfügung zu stellen.
                                        Wenn Sie eine Webseite meines Internetauftritts aufrufen, die eine solche Funktion enthält, baut
                                        Ihr Browser eine direkte Verbindung mit den Servern von Shareaholic, Inc. auf. Der Inhalt des
                                        genannten Social Plugins wird direkt an Ihren Browser übermittelt und von diesem in meine
                                        Webseiten mit eingebunden. Ich habe daher keinen Einfluss auf den Umfang der Daten, die
                                        Shareaholic Inc. mit Hilfe dieses Plugins erhebt und informiere Sie daher entsprechend meinem
                                        Kenntnisstand. Nach diesem wird lediglich die IP-Adresse Ihrer Verbindung sowie die URL meiner
                                        Seite beim Bezug der „Weitersagen heißt Unterstützen“ Schaltflächen mit übermittelt, aber nicht
                                        für andere Zwecke, als die Darstellung der diversen Buttons und des Sogans („Weitersagen heißt
                                        Unterstützen“), genutzt.
                                    </p>
                                    <p class="text-secondary">
                                        Wenn Sie als Nutzer der jeweiligen sozialen Netzwerke „angemeldet, bzw. dort eingeloggt“ sind,
                                        wird diese Interaktion auf dem jeweiligen sozialen Netzwerk zu Ihrem Profil zugeordnet und
                                        angezeigt. Folgende Daten werden als „eingeloggter User“ übertragen:
                                    </p>
                                    <ul>
                                        <li>Als Referrer die URL unserer zuvor von Ihnen geöffneten Seite, auf die das „Social Plugin“ der
                                            Firma Shareaholic Inc. Bezug nimmt.</li>
                                        <li>Ihre IP-Adresse</li>
                                        <li>Ihre User-ID des jeweiligen sozialen Netzwerks</li>
                                        <li>Datum und Uhrzeit des Besuchs</li>
                                    </ul>
                                    <p class="text-secondary">Eine weitergehende Speicherung und Weitergabe Ihrer persönlichen Daten
                                        durch mich findet nicht
                                        statt.
                                        Weitere Details zum Social Plugin der Firma Shareaholic Inc. können Sie auf den folgenden Seiten
                                        des Herstellers entnehmen:</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-8">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-8" aria-expanded="false" aria-controls="collapse-2-8">
                                        Verwendung des Google Plus 1 Buttons (“+1″ Schaltfläche)
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-8" class="collapse" aria-labelledby="heading-2-8" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Mein Internetauftritt verwendet die „+1“-Schaltfläche des sozialen
                                        Netzwerkes Google, welches von
                                        der Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043 betrieben wird. Die
                                        Schaltfläche ist mit einem „+1″ gekennzeichnet. Die „+1“ Schaltfläche ist ein Kürzel für “das ist
                                        ziemlich cool” oder “schau dir das an”. Die Schaltfläche wird nicht verwendet, um Ihre Besuche im
                                        Web zu erfassen.</p>
                                    <p class="text-secondary">
                                        Enthält eine Webseite meines Internetauftrittes die „+1“-Schaltfläche, dann wird Ihr
                                        Internetbrowser diese Schaltfläche vom Google-Server laden und darstellen. Die von Ihnen besuchte
                                        Website meines Internetauftrittes wird dem Google-Server automatisch mitgeteilt. Beim Anzeigen
                                        einer +1-Schaltfläche protokolliert Google nicht dauerhaft Ihren Browserverlauf, sondern lediglich
                                        für die Dauer von bis zu zwei Wochen.
                                    </p>
                                    <p class="text-secondary">
                                        Google hält diese Daten über Ihren Besuch für diesen Zeitraum zu Systemwartungs- und
                                        Fehlerbehebungszwecken gespeichert. Diese Daten sind jedoch nicht nach individuellen Profilen,
                                        Nutzernamen oder URLs strukturiert. Diese Informationen sind auch nicht für Website-Publisher oder
                                        Inserenten zugänglich. Die Verwendung dieser Informationen dient nur zur Wartung und
                                        Fehlerbeseitigung in internen Systemen bei Google. Es wird Ihr Besuch auf einer Seite mit
                                        +1-Schaltfläche auch nicht in anderer Weise von Google ausgewertet.
                                    </p>
                                    <p class="text-secondary">Eine weitergehende Auswertung Ihres Besuchs der Websites meines
                                        Internetauftrittes mit einer
                                        „+1“-Schaltfläche erfolgt nicht.</p>
                                    <p class="text-secondary">Die Vergabe von +1 selbst ist ein öffentlicher Vorgang, d.h. jeder, der
                                        eine Google-Suche
                                        ausführt oder Inhalte im Web aufruft, denen Sie +1 geben, kann potenziell sehen, dass Sie dem
                                        betreffenden Inhalt ein +1 gegeben haben. Geben Sie daher nur dann +1, wenn Sie sich ganz sicher
                                        sind, dass Sie diese Empfehlung mit der ganzen Welt teilen möchten.</p>
                                    <p class="text-secondary">Ein Klick auf diesen +1-Button dient als Empfehlung für andere Nutzer in
                                        Googles Suchergebnissen.
                                        Sie können öffentlich mitteilen, dass Ihnen meine Webseite gefällt, meine Webseite Ihre Zustimmung
                                        findet oder dass Sie meine Webseite empfehlen können. Haben Sie sich für Google+ registriert und
                                        sind eingeloggt, dann färbt sich die +1-Schaltfläche bei einem Klick blau. Zudem wird das +1 zu
                                        dem +1-Tab in Ihrem Google-Profil hinzugefügt. Auf diesem Tab können Sie Ihre +1 verwalten und
                                        entscheiden, ob Sie den +1-Tab öffentlich machen möchten.
                                        Um Ihre +1-Empfehlung zu speichern und öffentlich zugänglich machen zu können erfasst Google über
                                        Ihr Profil Informationen über die von Ihnen empfohlene URL, Ihre IP-Adresse und andere
                                        browserbezogene Informationen. Wenn Sie Ihre +1 zurücknehmen, werden diese Informationen gelöscht.
                                        Sämtliche +1-Empfehlungen von Ihnen sind auf dem +1- Tab in Ihrem Profil aufgelistet.</p>
                                    <p class="text-secondary">Weitere Hinweise zum Datenschutz bei der Nutzung der +1-Schaltfläche
                                        finden Sie unter dem Link
                                        <a href="https://www.google.com/intl/de/+/policy/+1button.html"
                                           class="underline">https://www.google.com/intl/de/+/policy/+1button.html</a>.
                                    </p>
                                    <p class="text-secondary">Die Klausel zur Verwendung der Google „+1“-Schaltfläche wurde von der
                                        Kanzlei Gerstel erstellt
                                        und ist unter <a href="http://www.abmahnberatung.de"
                                                         class="underline">http://www.abmahnberatung.de</a> zu finden.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-9">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-9" aria-expanded="false" aria-controls="collapse-2-9">
                                        Verwendung von Twitter Re-Tweet-Funktionen („Tweet“ Schaltfläche)
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-9" class="collapse" aria-labelledby="heading-2-9" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Mein Internetauftritt benutzt Twitter und die Twitter Re-Tweet-Funktionen,
                                        welche von der Twitter Inc., 795 Folsom St., Suite 600, San Francisco, CA 94107, USA angeboten
                                        werden. Mit dem Re-Tweet-Button („Tweet“) ist es Twitternutzern möglich, meine Artikel bzw. die
                                        Webseiten meines Internetauftritts über Twitter zu teilen, bzw. anderen Personen auf Twitter zur
                                        Verfügung zu stellen.</p>
                                    <p class="text-secondary">
                                        Wenn Sie eine Webseite meines Internetauftritts aufrufen, die einen solchen Button enthält, baut
                                        Ihr Browser eine direkte Verbindung mit den Servern von Twitter Inc. auf. Der Inhalt des
                                        Re-Tweet-Buttons („Tweet“) wird von Twitter Inc. direkt an Ihren Browser übermittelt und von
                                        diesem in die Webseite eingebunden. Ich habe daher keinen Einfluss auf den Umfang der Daten, die
                                        Twitter Inc. mit Hilfe dieses Plugins erhebt und informieren Sie daher entsprechend meinem
                                        Kenntnisstand. Nach diesem wird lediglich die IP-Adresse Ihrer Verbindung sowie die URL meiner
                                        Seite beim Bezug des „Tweet“ Buttons mit übermittelt, aber nicht für andere Zwecke, als die
                                        Darstellung des Buttons, genutzt.
                                    </p>
                                    <p class="text-secondary">
                                        Wenn Sie zum Zeitpunkt der Datenübermittlung bei Twitter angemeldet sind, wird diese Interaktion
                                        auf dem Twitter-Server zu Ihrem Profil zugeordnet und angezeigt. Folgende Daten werden als
                                        „eingeloggter User bei Twitter“, bzw. wenn Sie den oben stehenden Re-Tweet-Button, bzw. „Tweet“
                                        anklicken an den Dienst Twitter übertragen:
                                    </p>
                                    <ul>
                                        <li>Als Referrer die URL unserer zuvor von Ihnen geöffneten Seite, auf die das „Re-Tweet“-Plugin
                                            Bezug nimmt.</li>
                                        <li>Ihre IP-Adresse</li>
                                        <li>Ihre User-ID</li>
                                        <li>Datum und Uhrzeit des Besuchs</li>
                                    </ul>
                                    <p class="text-secondary">Details zum Umgang mit Ihren Daten durch Twitter sowie zu Ihren Rechten
                                        und Einstellungsmöglichkeiten zum Schutz Ihrer persönlichen Daten können Sie den
                                        Datenschutzhinweisen von Twitter entnehmen.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-10">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-10" aria-expanded="false" aria-controls="collapse-2-10">
                                        Rechtswirksamkeit dieses Haftungsausschlusses
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-10" class="collapse" aria-labelledby="heading-2-10" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">Dieser Haftungsausschluss ist als Teil des Internetangebotes zu
                                        betrachten, von dem aus auf diese Seite verwiesen wurde. Sofern Teile oder einzelne Formulierungen
                                        dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen
                                        sollten, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon
                                        unberührt.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2-11">
                                <h5 class="mb-0">
                                    <button class="btn btn-block collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse-2-11" aria-expanded="false" aria-controls="collapse-2-11">
                                        Dienstangebot , Datenbank und Nutzungsrecht
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-2-11" class="collapse" aria-labelledby="heading-2-11" data-parent="#accordion-2">
                                <div class="card-body">
                                    <p class="text-secondary">
                                        Anbieter i.S.d. § 5 Telemediengesetz (TMG), sowie Inhaber und Eigentümer der Domain <a
                                            href="http://crowdexperts.de">crowdexperts.de</a> ist der Deutscher Crowdsourcing Verband e.V.
                                    </p>
                                    <p class="text-secondary">
                                        Inhaber des technischen Dienstangebotes, der Datenbank, Datenbankstruktur und deren Inhalte, die
                                        HTML- und PHP Programmierung, das grafische Konzept, Design und Umsetzungskonzept, sowie sämtliche
                                        Nutzungsrechte für
                                        die Datenbank liegen bei:
                                    </p>
                                    <p class="text-secondary">
                                        Crowd Mentor Network <br>
                                        Dr. Michael Gebert <br>
                                        Sollnerstrasse 21 <br>
                                        82049 Pullach <br>
                                        <a href="http://www.crowdmentor.de" title="Crowd Mentor" class="underline">www.crowdmentor.de</a>
                                    </p>
                                    <p class="text-secondary">
                                        Das technische Dienstangebot der Datenbank von Experten zu CrowdSourcing, CrowdFunding und
                                        CrowdInnovation wird dem Deutschen CrowdSourcing Verband e.V. kostenfrei auf unbestimmte Zeit vom
                                        Inhaber des Dienstangebots zu Verfügung gestellt.
                                        Crowd Mentor Network räumt dem Deutschen CrowdSourcing Verband e.V. das ausschließliche, zeitlich
                                        und räumlich unbeschränkte Recht ein, das entwickelte Design und die Datenbank in allen denkbaren
                                        Nutzungsarten zu nutzen.
                                        Der Deutschen CrowdSourcing Verband e.V. ist berechtigt, einzelne Elemente wie Grafiken und
                                        Schriftzüge der Oberflächengestaltung des Designs gesondert für seine Werbung zu verwerten und das
                                        Design zu bearbeiten und auf seine aktuellen geschäftlichen Zwecke anzupassen.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

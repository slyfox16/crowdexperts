<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    @if(Auth::check())
        <script>
            window.authID = {{ Illuminate\Support\Facades\Auth::id() }}
        </script>
    @endif

    @livewireStyles
</head>
<body>
    @yield('sidebar')

    <div id="app" class="main">
        @include('frontend.partials.layouts.header')

        @yield('header')

        @yield('content')
    </div>

    @include('frontend.partials.layouts.footer')
    @include('frontend.partials.layouts.conf_modal')

    <script src="{{ mix('js/app.js') }}"></script>

    @livewireScripts

    <script>
        document.addEventListener("livewire:load", () => {
            Livewire.hook('message.processed', (message, component) => {
                $('[data-toggle="tooltip"]').tooltip();
            });
        });
    </script>

    {!! Asset::css() !!}
    {!! Asset::js() !!}
    {!! Asset::script() !!}

</body>
</html>

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}" />

    <title>{{ config('app.name', 'Portal') }}</title>
</head>
<body>

@yield('content')

@include('frontend.partials.layouts.footer')
@include('frontend.partials.layouts.conf_modal')

<script src="{{ mix('js/app.js') }}"></script>

{!! Asset::css() !!}
{!! Asset::js() !!}
{!! Asset::script() !!}

{!!  GoogleReCaptchaV3::init() !!}

</body>
</html>

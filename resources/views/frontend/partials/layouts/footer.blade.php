<footer class="bg-black text-white">
    <div class="container">
        <div class="row justify-content-between mb-20">
            <div class="col-lg-2">
                <a href="{{ route('home') }}" class="navbar-brand"><img src="{{ schema_asset('images/logo.svg') }}" alt="Logo"></a>
            </div>
            <div class="col-lg-4">
                <p>DCV e.V. & German Crowdfunding Network Martin-Luther-Straße 8 <br>10777 Berlin</p>
            </div>
            <div class="col-lg-4">
                <ul class="nav nav-minimal flex-lg-column">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('contact') }}">Nehmen Sie jetzt Kontakt auf</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('impressum') }}">Impressum</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('agreement') }}">Mitglied werden beim DCV</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.crowdsourcingverband.de/mitglied-werden/">Mitglied werden</a>
                    </li>
                    @if (!Auth::check())
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Als Experte(in) registrieren</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cabinet.experts') }}">Experten finden</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <div class="row align-items-center gutter-1 gutter-lg-6 text-muted">
            <div class="col-lg-6">
                <p>© Crowdexperts. All rights reserved.</p>
            </div>
            <div class="col-lg-6 text-lg-right">
                <a data-scroll="" href="#top" class="text-muted">Back to top <i class="icon-arrow-up"></i></a>
            </div>
        </div>
    </div>
</footer>

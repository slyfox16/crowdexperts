<header class="header header-light">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="mr-8">
                <a href="{{ route('home') }}" class="navbar-brand"><img src="{{ schema_asset('images/logo-black.svg') }}" alt="Logo"></a>
                <small class="d-block text-secondary">Innovation / Finanzierung / Sourcing</small>
            </div>

            <button class="navbar-toggler burger" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
            </button>

            <div class="collapse navbar-collapse header-collapse mt-2 mt-lg-0" id="navbarSupportedContent">
                @if(Auth::check())
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cabinet.experts') }}">
                                Experten
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cabinet.events') }}">
                                Events
                            </a>
                        </li>
                    </ul>

                    <ul class="navbar-nav navbar-nav-fill">
                        <li class="nav-item">
                            <a id="username" class="nav-link" href="{{ route('profile') }}">
                                {{ Auth::user()->name }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cabinet.chat') }}">
                                <i class="d-none d-lg-inline-block icon-mail"></i> <span class="d-inline-block d-lg-none">Messages</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cabinet.main') }}">
                                <i class="d-none d-lg-inline-block icon-user"></i> <span class="d-inline-block d-lg-none">Settings</span>
                            </a>
                        </li>
                        @if (Auth::user()->role_id == 1)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('voyager.dashboard') }}">
                                <i class="d-none d-lg-inline-block icon-sliders"></i> <span class="d-inline-block d-lg-none">Dashboard</span>
                            </a>
                        </li>
                        @endif
                        <li class="nav-item">
                            <a href="#" class="nav-link"  onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="d-none d-lg-inline-block icon-log-out"></i> <span class="d-inline-block d-lg-none">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                @else
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cabinet.experts') }}">
                                Experten
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cabinet.events') }}">
                                Events
                            </a>
                        </li>
                    </ul>

                    <ul class="navbar-nav navbar-nav-fill">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">
                                Login
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">
                                Register
                            </a>
                        </li>
                    </ul>
                @endif
            </div>
        </nav>
    </div>
</header>

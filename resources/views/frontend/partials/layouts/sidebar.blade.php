<!-- sidebar -->
<aside class="overlay">
    <button class="overlay-toggler burger text-black" type="button">
        <span></span>
    </button>
    <div class="overlay-body">
        <div>
            <div class="row">
                <div class="col">
                    <h2 class="h1 mb-2 font-weight-bold">Search</h2>
                </div>
            </div>
            <form action="">
                <fieldset class="row gutter-2">
                    <div class="col-md-6 col-lg-12">
                        <div class="form-group">
                            <label for="inputText1">Text</label>
                            <input type="text" class="form-control" id="inputText1" aria-describedby="inputText">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-12">
                        <div class="form-group">
                            <label for="inputText6">Custom select</label>
                            <select class="custom-select">
                                <option selected>Choose a location</option>
                                <option value="1">New York</option>
                                <option value="2">London</option>
                                <option value="3">Stockholm</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="inputText3">Message</label>
                            <textarea class="form-control" id="inputText3" rows="4"></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">Check this custom checkbox</label>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="overlay-footer">
        <div>
            <button class="btn btn-block btn-primary">Search</button>
        </div>
    </div>

</aside>

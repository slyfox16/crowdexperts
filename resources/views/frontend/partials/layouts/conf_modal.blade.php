<div class="modal fade" id="confirmationModal" tabindex="-1" aria-labelledby="confirmationModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-primary text-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icon-x"></i></button>
            <div class="modal-body text-center">
                <i class="icon-check fs-40 text-white icon-box bg-opaque-black icon-rounded mb-4"></i>
                <h2 class="h3 font-weight-bold">Complete!</h2>
                <p id="modal-text" class="text-secondary">Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
            </div>
        </div>
    </div>
</div>

@extends('frontend.layouts.app')

@section('header')
    <!-- partners -->
    <section class="pb-10">
        <div class="container">
            <div class="row">
                <div class="col-6 col-lg-3">
                    <span class="eyebrow d-block mb-1 text-muted">präsentiert von</span>
                    <div class="logo d-inline-block">
                        <img src="{{ schema_asset('images/dcv.png') }}" alt="">
                    </div>
                </div>
                <div class="col-6 col-lg-3">
                    <span class="eyebrow d-block mb-1 text-muted">gefördert durch</span>
                    <div class="logo d-inline-block">
                        <img src="{{ schema_asset('images/ihk.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('sidebar')
    @livewire('filter')
@endsection

@section('content')

    <section class="pb-4">
        <div class="container">
            <div class="row">
                <div class="col-6 col-lg-3">
                    <span class="eyebrow d-block mb-1 text-muted">präsentiert von</span>
                    <div class="logo d-inline-block">
                        <img src="assets/images/dcv.png" alt="">
                    </div>
                </div>
                <div class="col-6 col-lg-3">
                    <span class="eyebrow d-block mb-1 text-muted">gefördert durch</span>
                    <div class="logo d-inline-block">
                        <img src="assets/images/ihk.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-title">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                </div>
                <div class="col-lg-6 text-lg-right">
                    <ul class="page-options">
                        <li>
                            <button class="btn btn-icon btn-primary overlay-toggler" type="button">
                                <i class="icon-search"></i>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- users -->
    <section class="py-0">
        <div class="container-full">
            <div class="row gutter-0">
                @forelse ($users as $user)
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <a href="{{ route('profile', $user) }}" class="card equal hover-gradient hover-pin text-white">
                            <figure class="image" style="background-image: url('{{ schema_asset($user->resize('avatar', 467, 476)) }}')"></figure>
                            <div class="card-footer text-shadow">
                                <div>
                                    <h5 class="mb-0">{{ $user->name }}</h5>
                                    <span class="text-muted">{{ $user->position }}</span>
                                </div>
                            </div>

                            <ul class="card-pin">
                                @if($specialities = $user->specialities)
                                    @foreach($specialities as $speciality)
                                        <li class="pin" data-toggle="tooltip" data-placement="left" data-original-title="{{ $speciality->speciality }}">
                                            {{ $speciality->abbr }}
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </a>
                    </div>
                @empty
                    <p>No users</p>
                @endforelse
            </div>
        </div>
    </section>


    <!-- cta -->
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <span class="eyebrow text-secondary mb-2">About Crowd Experts</span>
                    <h2>Schnell und einfach Crowd Experten finden. Damit Ihr Crowd-X Projekt die beste Unterstützung erfährt. <a href="{{ route('cabinet.experts') }}" class="action">Experten finden <i class="icon-arrow-right"></i></a></h2>
                </div>
            </div>
        </div>
    </section>

    <!-- features -->
    <section class="bg-light">
        <div class="container">
            <div class="row gutter-6">
                <div class="col-md-4">
                    <span class="fs-32">01</span>
                    <h2 class="mt-3">Wie es funktioniert</h2>
                    <p class="text-secondary">Unabhängige Crowd-X Experten können sich mit ihren jeweiligen Spezialgebieten registrieren. Dies erleichtert Unternehmen und Projektstartern die Arbeit nach der Suche für den jeweils passenden Berater, Coach oder geistigen Sparringspartner.</p>
                </div>
                <div class="col-md-4">
                    <span class="fs-32">02</span>
                    <h2 class="mt-3">Zertifizierter Experte Werden</h2>
                    <p class="text-secondary">Um Crowdfunding Manager/in (IHK) werden zu können, gibt es derzeit zwei IHKs, die dafür entsprechende Kurse anbieten: IHK München und IHK Gießen-Friedberg. Da der DCV die Curricula erarbeitet hat, ist dies die Lizenz zum effektiven Durchführen einer Crowdfinancing-Kampagne.</p>
                </div>
                <div class="col-md-4">
                    <span class="fs-32">03</span>
                    <h2 class="mt-3">Sich aktiv in den DCV einbringen</h2>
                    <p class="text-secondary">Der Deutschen Crowdsourcing Verbandes ist die füherende Interessensvertretung für Praktiker und Industrie. Unsere derzeitigen Arbeitsgruppen sind Crowdinnovation, Crowdfunding, Crowdworking und Crowdmarketing. Mehr dazu auf unserer Website.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- cta -->
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <h2 class="mb-1">Jetzt als Experte anmelden und gefunden werde</h2>
                    <a href="{{ route('cabinet.experts') }}" class="action">Experten Suchen <i class="icon-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>

@endsection

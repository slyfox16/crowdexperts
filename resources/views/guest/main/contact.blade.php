@extends('frontend.layouts.app')

@section('header', '')

@section('sidebar', '')

@section('content')

    <!-- page title -->
    <section class="page-title">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contact</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>


    <section class="bg-light overflow-hidden">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-6">
                    <div class="row gutter-4">
                        <div class="col-12">
                            <h1>Wir freuen uns auf Ihr Feedback</h2>
                        </div>
                        <div class="col-12">
                            {!! form_start($messageForm) !!}
                                <div class="col-md-12">
                                    {!! form_row($messageForm->name) !!}
                                </div>
                                <div class="col-md-12">
                                    {!! form_row($messageForm->email) !!}
                                </div>
                                <div class="col-md-12">
                                    {!! form_row($messageForm->message) !!}
                                </div>

                                <input type="submit" hidden id="submit-hidden">
                            {!! form_end($messageForm, false) !!}
                        </div>
                        <div class="col-12">
                            {!! form_widget($messageForm->send, ['attr' => ['class' => 'btn btn-primary'], 'label' => 'Get in touch']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 split">
                    <h3>Werden Sie Mitglied beim DCV e.V</h3>
                    <ul class="list-group list-group-minimal list-group-xs">
                        <li class="list-group-item">
                            <a href="{{ setting('site.facebook_url') }}" class="underline text-facebook font-weight-bold">via Facebook</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ setting('site.linkedin_url') }}" class="underline text-linkedin font-weight-bold">via Linkedin</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ setting('site.xing_url') }}" class="underline text-xing font-weight-bold">via Xing</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    @php Asset::js(mix('/js/login_register_validate.js')) @endphp
@endsection

@script
<script>
    $('#filter-form-submit').on('click', function (e) {
        $('#submit-hidden').click();
    });
</script>
@endscript


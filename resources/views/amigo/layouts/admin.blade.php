<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $meta_title }} - {{ settings('site.title') ?? env('APP_NAME') }}</title>
    <meta name="description" content="Admin area">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('vendor/amigo/css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('css/backend_app.css') }}">
    {{--
    <link rel="stylesheet" href="{{ asset('vendor/amigo/css/amigo.css') }}">
     --}}
</head>
<body>
<div id="page-container" class="sidebar-o sidebar-dark enable-page-overlay side-scroll page-header-fixed">
    @include('amigo-pub::shared.nav')
    @include('amigo-pub::shared.header')

    <main id="main-container">
        @yield('head-container')
        <div class="content">
            @yield('content')
        </div>
    </main>

    @if($version)
        <footer id="page-footer" class="bg-body-light">
            <div class="content py-3">
                <div class="row font-size-sm">
                    <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">
                        AmigoCMS v. {{$version}}
                    </div>
                </div>
            </div>
        </footer>
    @endif
</div>

<script src="{{ asset('vendor/amigo/js/oneui.core.min.js') }}"></script>
<script src="{{ asset('vendor/amigo/js/oneui.app.min.js') }}"></script>


<script src="{{ asset('vendor/amigo/js/plugins/ckeditor/ckeditor.js') }}"></script>

<script src="{{ asset('vendor/amigo/js/_app.js') }}"></script>
<script src="{{ asset('vendor/amigo/js/amigo.js') }}"></script>
<script src="{{ asset('js/backend.js') }}"></script>

{!! Asset::css() !!}
{!! Asset::js() !!}
{!! Asset::script() !!}

<style>
    .badge {
        position: absolute;
        padding: 4px 8px;
        color: #E34242;
        font-weight: bold;
        top: 40px;
        right: 23px;
        text-transform: lowercase;
    }

    .pre_click1 {
        margin: 0 20px;
    }

    .lang-button.btn-danger {
        color: #fff !important;
    }
</style>
<script>
    jQuery(function () {
        //One.helpers(['ckeditor', 'notify', 'datepicker', 'select2']);
        //console.log(window);
    });
    window.EventBus = new Vue();

    $(document).ready(function () {
        $('.pre_click1').click(function () {
            $('.click1').click();
        })

        $('.pre_click2').click(function () {
            $('.click2').click();
        })

        $('.lang__switcher .btn-danger').click();
    })
</script>
@stack('scripts')
@include('amigo-pub::shared.alert')
@yield('vue-components')
@yield('javascript')
@yield('js')

</body>
</html>

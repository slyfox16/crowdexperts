<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login - {{env('APP_NAME')}}</title>
    <meta name="description" content="Amigo CMS">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" href="{{ asset('vendor/amigo/css/app.css') }}">
</head>
<body>
<div id="page-container">
    <main id="main-container">
        <div class="hero-static d-flex align-items-center">
            <div class="w-100">
                <div class="content content-full bg-white">
                    <div class="row justify-content-center">
                        <div class="col-md-8 col-lg-6 col-xl-4 py-4">
                            @yield('content')
                        </div>
                    </div>
                </div>
                <div class="font-size-sm text-center text-muted py-3">
                    &copy; <span data-toggle="year-copy"></span> <strong>CMS by <a href="https://amigo.studio" target="_blank">Amigo Studio</a></strong>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="{{ asset('vendor/amigo/js/app.js') }}"></script>
</body>
</html>

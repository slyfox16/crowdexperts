<nav id="sidebar" aria-label="Main Navigation">
    <div class="content-header bg-white-5">
        <a class="font-w600 text-dual" href="{{ route('amigo.dashboard') }}">
            <span class="smini-hide">{{ env('APP_NAME') }}</span>
        </a>
        <a class="d-lg-none btn btn-sm btn-dual ml-1" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
            <i class="fa fa-fw fa-times"></i>
        </a>
    </div>
    <div class="js-sidebar-scroll">
        <div class="content-side content-side-full">
            <ul class="nav-main">
                <li class="nav-main-item {{ setActive(['admin/certificates*'], 'open') }}">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" href="#">
                        <i class="nav-main-link-icon si si-settings"></i>
                        <span class="nav-main-link-name">Certificates</span>
                    </a>
                    <ul class="nav-main-submenu">
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ setActive('admin/certificates*') }}" href="{{ route('amigo.certificates.index') }}">
                                <span class="nav-main-link-name">Certificates</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-main-item {{ setActive(['admin/events*'], 'open') }}">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" href="#">
                        <i class="nav-main-link-icon si si-settings"></i>
                        <span class="nav-main-link-name">Event list</span>
                    </a>
                    <ul class="nav-main-submenu">
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ setActive('admin/events*') }}" href="{{ route('amigo.events.index') }}">
                                <span class="nav-main-link-name">Events</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-main-item {{ setActive(['admin/users*', 'admin/admins*'], 'open') }}">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" href="#">
                        <i class="nav-main-link-icon si si-settings"></i>
                        <span class="nav-main-link-name">Users</span>
                    </a>
                    <ul class="nav-main-submenu">
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ setActive('admin/admins*') }}" href="{{ route('amigo.admins.index') }}">
                                <span class="nav-main-link-name">Administrators</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ setActive('admin/users*') }}" href="{{ route('amigo.users.index') }}">
                                <span class="nav-main-link-name">Clients</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-main-item {{ setActive(['admin/settings*', 'admin/translations*', 'admin/languages*', 'admin/modules*'], 'open') }}">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" href="#">
                        <i class="nav-main-link-icon si si-settings"></i>
                        <span class="nav-main-link-name">Settings</span>
                    </a>
                    <ul class="nav-main-submenu">
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ setActive('admin/settings*') }}" href="{{ route('amigo.settings.index') }}">
                                <span class="nav-main-link-name">Site Settings</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ setActive('admin/translations*') }}" href="{{ route('amigo.translations.index') }}">
                                <span class="nav-main-link-name">Translations</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ setActive('admin/modules*') }}" href="{{ route('amigo.modules.index') }}">
                                <span class="nav-main-link-name">Modules</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

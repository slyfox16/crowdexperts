<script>
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-primary mx-2',
            cancelButton: 'btn btn-danger mx-2'
        },
        buttonsStyling: false
    });

@if(session()->has('success'))
    swalWithBootstrapButtons.fire({
        title: 'Success!',
        text: '{{ session()->get('success') }}',
        icon: 'success',
        showConfirmButton: false,
        timer: 2000
    });
@elseif(session()->has('error') || $errors->all())
    swalWithBootstrapButtons.fire({
        title: 'Error!',
        text: '{{ session()->get('error') }}',
        icon: 'error',
        showConfirmButton: false,
        timer: 2000
    });
@elseif (session()->has('warning'))
    swalWithBootstrapButtons.fire({
        title: 'Warning!',
        text: '{{ session()->get('warning') }}',
        icon: 'warning',
        showConfirmButton: false,
        timer: 2000
    });
@endif
</script>
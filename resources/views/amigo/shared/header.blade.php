<header id="page-header">
    <div class="content-header">
        <div class="d-flex align-items-center">
            <button type="button" class="btn btn-sm btn-dual mr-2 d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                <i class="fa fa-fw fa-bars"></i>
            </button>
            @yield('crumbs')

        </div>
        <div class="d-flex align-items-center">
            @yield('buttons')

            <div class="dropdown d-inline-block ml-2">
                <button type="button" class="btn d-flex align-items-center" id="page-header-user-dropdown" data-toggle="dropdown">
                    <span class="d-none d-sm-inline-block">
                        <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.5938 0.65625C5.63672 0.65625 0 6.29297 0 13.25C0 20.207 5.63672 25.8438 12.5938 25.8438C19.5508 25.8438 25.1875 20.207 25.1875 13.25C25.1875 6.29297 19.5508 0.65625 12.5938 0.65625ZM12.5938 5.53125C15.0312 5.53125 17.0625 7.5625 17.0625 10C17.0625 12.4883 15.0312 14.4688 12.5938 14.4688C10.1055 14.4688 8.125 12.4883 8.125 10C8.125 7.5625 10.1055 5.53125 12.5938 5.53125ZM12.5938 23C9.59766 23 6.90625 21.6797 5.12891 19.5469C6.09375 17.7695 7.97266 16.5 10.1562 16.5C10.2578 16.5 10.3594 16.5508 10.5117 16.6016C11.1719 16.8047 11.832 16.9062 12.5938 16.9062C13.3047 16.9062 14.0156 16.8047 14.625 16.6016C14.7773 16.5508 14.8789 16.5 15.0312 16.5C17.1641 16.5 19.043 17.7695 20.0078 19.5469C18.2305 21.6797 15.5391 23 12.5938 23Z" fill="#3F4ADF"/>
                        </svg>
                    </span>
                    <svg class="ml-2" width="8" height="5" viewBox="0 0 8 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 1L4 4L7 1" stroke="#3F4ADF" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>

                    {{--                    <i class="fa fa-fw fa-angle-down d-none d-sm-inline-block"></i>--}}
                </button>
                <div class="dropdown-menu dropdown-menu-right p-0 border-0 font-size-sm"
                     aria-labelledby="page-header-user-dropdown">
                    <div class="p-2">
                        <h5 class="dropdown-header text-uppercase">Site Options</h5>
                        <a class="dropdown-item d-flex align-items-center justify-content-between" href="{{ route('amigo.cache') }}">
                            <span class="d-none d-sm-inline-block font-w600">Clear cache</span>
                            <i class="si si-trash ml-1"></i>
                        </a>
                        <a class="dropdown-item d-flex align-items-center justify-content-between" href="{{ config('app.url') }}" target="_blank">
                            <span>Visit site</span>
                            <i class="si si-action-redo ml-1"></i>
                        </a>
                        <div role="separator" class="dropdown-divider"></div>

                        <h5 class="dropdown-header text-uppercase">Account Options</h5>
                        <a class="dropdown-item d-flex align-items-center justify-content-between" href="{{ route('amigo.edit_password') }}">
                            <span>Profile</span>
                            <i class="si si-settings"></i>
                        </a>
                        <a class="dropdown-item d-flex align-items-center justify-content-between"
                           href="{{ route('amigo.logout') }}">
                            <span>Log Out</span>
                            <i class="si si-logout ml-1"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="page-header-search" class="overlay-header bg-white">
        <div class="content-header">
            @if (isset($module) && (Route::has('amigo.' . Str::plural($module) . '.index')))
                <form class="w-100" action="{{ route('amigo.' . Str::plural($module) . '.index') }}" method="get">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <button type="button" class="btn btn-danger" data-toggle="layout"
                                    data-action="header_search_off">
                                <i class="fa fa-fw fa-times-circle"></i>
                            </button>
                        </div>
                        <input type="text" class="form-control" placeholder="Search or hit ESC.."
                               id="page-header-search-input" name="search" value="{{ Request::get('search') }}">
                    </div>
                </form>
            @endif
        </div>
    </div>
    <div id="page-header-loader" class="overlay-header bg-white">
        <div class="content-header">
            <div class="w-100 text-center">
                <i class="fa fa-fw fa-circle-notch fa-spin"></i>
            </div>
        </div>
    </div>
</header>

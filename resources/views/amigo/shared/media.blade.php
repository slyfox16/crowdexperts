@section('media')
    <div class="row block d-none manager-block mb-0" id="manager-block">
        <div class="overlay"></div>
        <div id="filemanager">
            <media-manager
                    base-path="{{ config('amigo.media.path', '/') }}"
                    :show-folders="{{ config('amigo.media.show_folders', true) ? 'true' : 'false' }}"
                    :allow-upload="{{ config('amigo.media.allow_upload', true) ? 'true' : 'false' }}"
                    :allow-move="{{ config('amigo.media.allow_move', true) ? 'true' : 'false' }}"
                    :allow-delete="{{ config('amigo.media.allow_delete', true) ? 'true' : 'false' }}"
                    :allow-create-folder="{{ config('amigo.media.allow_create_folder', true) ? 'true' : 'false' }}"
                    :allow-rename="{{ config('amigo.media.allow_rename', true) ? 'true' : 'false' }}"
                    :allow-crop="{{ config('amigo.media.allow_crop', true) ? 'true' : 'false' }}"
                    :select-image="true"
                    :details="{{ json_encode(['thumbnails' => config('amigo.media.thumbnails', []), 'watermark' => config('amigo.media.watermark', (object)[])]) }}"
            ></media-manager>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        new Vue({
            el: '#filemanager'
        });
    </script>
@stop
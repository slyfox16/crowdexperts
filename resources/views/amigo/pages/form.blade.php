@extends('amigo-pub::layouts.admin')

@section('content')
    <form action="{{ isset($page->id) ? route('amigo.pages.update', $page->id) : route('amigo.pages.store' )}}"
          method="POST">
        @csrf
        @if (isset($page->id))
            @method('PUT')
        @else
            @method('POST')
        @endif
        <div class="row">
            <div class="col-lg-7 col-xl-8">
                <div class="block block-fx-shadow">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">{{ isset($page->id) ? 'Edit' : 'Create' }} Attribute</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group row">
                            @foreach ($languages as $lang)
                                <div class="col-12" data-lang="{{ $lang['locale'] }}">
                                    <label for="title-{{ $lang['locale'] }}">Title <span
                                                class="badge badge-danger">{{ strtoupper($lang['locale']) }}</span></label>
                                    <input
                                            type="text"
                                            name="title[{{ $lang['locale'] }}]"
                                            class="form-control @error('title.' . $lang['locale']) is-invalid @enderror"
                                            id="title{{ $lang['locale'] }}"
                                            value="{{ isset($page) ? $page->getTranslation('title', $lang['locale']) : old('title.' . $lang['locale']) }}"
                                            data-slug
                                    >
                                    @error('title.' . $lang['locale'])
                                    <div class="text-danger">
                                        <small><strong>{{ str_replace("." . $lang['locale'], "", $message) }}</strong></small>
                                    </div>
                                    @enderror
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group row">
                            @foreach ($languages as $lang)
                                <div class="col-12 position-relative" data-lang="{{ $lang['locale'] }}">
                                    <label for="slug-{{ $lang['locale'] }}">Slug <span class="badge badge-danger">{{ strtoupper($lang['locale']) }}</span></label>
                                    <input
                                            type="text"
                                            name="slug[{{ $lang['locale'] }}]"
                                            class="form-control @error('slug.' . $lang['locale']) is-invalid @enderror"
                                            id="slug-{{ $lang['locale'] }}"
                                            value="{{ isset($page) ? $page->getTranslation('slug', $lang['locale']) : old('slug.' . $lang['locale']) }}"
                                            readonly
                                            data-generate-slug="{{ isset($page) ? 'false' : 'true' }}"
                                    >
                                    <div class="position-absolute" style="top:0;right:10px">
                                        <button type="button" class="btn-block-option edit-slug">
                                            <i class="far fa-fw fa-edit"></i>
                                        </button>
                                    </div>
                                    @error('slug.' . $lang['locale'])
                                    <div class="text-danger">
                                        <small><strong>{{ str_replace('.' . $lang['locale'], '', $message) }}</strong></small>
                                    </div>
                                    @enderror
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group row">
                            @foreach ($languages as $lang)
                                <div class="col-12 @error('body.'.$lang['locale']) is-invalid @enderror" data-lang="{{ $lang['locale'] }}">
                                    <label for="body-{{ $lang['locale'] }}">Body <span class="badge badge-danger">{{ strtoupper($lang['locale']) }}</span></label>
                                    <textarea
                                            name="body[{{ $lang['locale'] }}]"
                                            id="body-{{ $lang['locale'] }}"
                                            class="ckeditor">{{ isset($page) && $page->getTranslation('body', $lang['locale']) ? $page->getTranslation('body', $lang['locale']) : old('body[' . $lang['locale'] . ']') }}</textarea>
                                    @error('body.'.$lang['locale'])
                                    <div class="text-danger">
                                        <small><strong>{{str_replace('.' . $lang['locale'], "", $message)}}</strong></small>
                                    </div>
                                    @enderror
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="layout">Layout</label>
                                <select name="layout" id="layout" class="form-control">
                                    <option value="">No layout</option>
                                    @foreach(config('amigo.pages.layouts') as $layout)
                                        <option value="{{ $layout }}" {{ isset($page) && $page->layout === $layout ? 'selected' : '' }}>{{ ucfirst($layout) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @php
                            $className = 'block-mode-hidden';
                            foreach ($languages as $lang) {
                                if (!empty($errors->get('meta_keywords.'.$lang['locale'])) || !empty($errors->get('meta_description.'.$lang['locale']))) {
                                    $className = '';
                                    break;
                                }
                            }
                        @endphp
                        <div class="block border border-secondary rounded {{$className}}">
                            <div class="block-header">
                                <h3 class="block-title">Meta</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle">
                                        <i class="si si-arrow-down"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="form-group row">
                                    @foreach($languages as $lang)
                                        <div class="col-12" data-lang="{{ $lang['locale'] }}">
                                            <label for="meta-description-{{ $lang['id'] }}">
                                                Meta Description <span class="badge badge-danger">{{ strtoupper($lang['locale']) }}</span>
                                            </label>
                                            <input
                                                    type="text"
                                                    name="meta_description[{{ $lang['locale'] }}]"
                                                    id="meta-description-{{ $lang['id'] }}"
                                                    class="form-control @error('meta_description.'.$lang['locale']) is-invalid @enderror"
                                                    value="{{ isset($page) && $page->getTranslation('meta_description', $lang['locale']) ? $page->getTranslation('meta_description', $lang['locale']) : old('meta_description[' . $lang['locale'] . ']') }}"
                                            >
                                            @error('meta_description.'.$lang['locale'])
                                            <div class="text-danger">
                                                <small><strong>{{str_replace('.'.$lang['locale'], '', str_replace('_', ' ', $message))}}</strong></small>
                                            </div>
                                            @enderror
                                        </div>
                                    @endforeach
                                </div>
                                <div class="form-group row">
                                    @foreach($languages as $lang)
                                        <div class="col-12" data-lang="{{ $lang['locale'] }}">
                                            <label for="meta-keywords-{{ $lang['id'] }}">
                                                Meta Keywords <span class="badge badge-danger">{{ strtoupper($lang['locale']) }}</span>
                                            </label>
                                            <input
                                                    type="text"
                                                    name="meta_keywords[{{ $lang['locale'] }}]"
                                                    id="meta-keywords-{{ $lang['id'] }}"
                                                    class="form-control @error('meta_keywords.'.$lang['locale']) is-invalid @enderror"
                                                    value="{{ isset($page) && $page->getTranslation('meta_keywords', $lang['locale']) ? $page->getTranslation('meta_keywords', $lang['locale']) : old('meta_keywords[' . $lang['locale'] . ']') }}"
                                            >
                                            @error('meta_keywords.'.$lang['locale'])
                                            <div class="text-danger">
                                                <small><strong>{{str_replace('.'.$lang['locale'], '', str_replace('_', ' ', $message))}}</strong></small>
                                            </div>
                                            @enderror
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-xl-4">
                <div id="page-sidebar">
                    <div class="sidebar__inner">
                        <div class="block block-fx-shadow">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Page Options</h3>
                            </div>
                            <div class="block-content">

                                <div class="btn-group btn-group-sm mb-2 d-flex" id="language-switcher" role="group">
                                    @foreach ($languages as $key => $lang)
                                        <button type="button" class="btn {{ $key === 0 ? 'btn-danger' : 'btn-light'}}"
                                                data-locale="{{ $lang['locale'] }}">{{ $lang['title'] }}</button>
                                    @endforeach
                                </div>

                                <div class="form-group row ml-0 mr-0">
                                    <div class="custom-control custom-switch mt-4 mb-4 mb-sm-0 col-12">
                                        <input type="checkbox" class="custom-control-input" id="published"
                                               name="published"
                                                {{ isset($page) && !$page->published ? '' : 'checked' }}>
                                        <label class="custom-control-label" for="published">Publish page</label>
                                    </div>
                                </div>

                                <hr>
                                <div class="form-group my-4 d-sm-flex justify-content-sm-between align-items-sm-center">
                                    <div class="btn-group w-100">
                                        <button class="btn btn-outline-primary px-2 mr-4" name="save" value="redirect">
                                            Save and exit
                                        </button>
                                        <button class="btn btn-primary px-2" name="save" value="refresh">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
@stop

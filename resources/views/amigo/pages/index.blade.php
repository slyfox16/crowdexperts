@extends('amigo-pub::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="col-xl-12">
                <div class="block block-fx-shadow">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Pages</h3>
                    </div>
                    <div class="block-content">
                        @if (!$pages->isEmpty())
                            <table class="table table-striped table-vcenter">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">#</th>
                                    <th>Name</th>
                                    <th class="text-center" style="width: 150px;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($pages as $page)
                                    <tr class="publish-item {{ !$page->published ? 'table-danger' : '' }}">
                                        <td>{{ $page->id }}</td>
                                        <td>{{ $page->getTranslation('title', $languages[0]['locale']) }}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ route('amigo.pages.edit', $page->id) }}"
                                                   class="btn btn-sm text-info" data-toggle="tooltip"
                                                   data-animation="true" data-placement="top" title="Edit">
                                                    <i class="fa fa-fw fa-pencil-alt"></i>
                                                </a>
                                                <button type="button"
                                                        class="btn btn-sm text-success js-tooltip-enabled toggle-publish"
                                                        data-toggle="tooltip" data-animation="true" data-placement="top"
                                                        title="Publish" data-id="{{ $page->id }}"
                                                        data-model="{{ get_class($page) }}">
                                                    <i class="fa fa-fw fa-eye"></i>
                                                </button>
                                                <button type="submit"
                                                        class="btn btn-sm text-danger js-swal-warning delete-item"
                                                        data-action="{{ route('amigo.pages.destroy', $page->id) }}"
                                                        data-toggle="tooltip" data-animation="true" data-placement="top"
                                                        title="Remove">
                                                    <i class="fa fa-fw fa-times"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center mt-5">
                                {{ $pages->links() }}
                            </div>
                        @else
                            <div class="alert alert-info text-center">
                                <h3>Wow, such empty!</h3>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

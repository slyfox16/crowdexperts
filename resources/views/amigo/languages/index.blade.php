@extends('amigo-pub::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="col-xl-12">
                <div class="block block-fx-shadow">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Languages</h3>
                    </div>
                    <div class="block-content">
                        @if (!$items->isEmpty())
                            <table class="table table-striped table-vcenter">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">#</th>
                                    <th>Name</th>
                                    <th class="text-center" style="width: 150px;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($items as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ route('amigo.languages.edit', $item->id) }}" class="btn btn-sm text-info" data-toggle="tooltip" data-animation="true" data-placement="top" title="Edit">
                                                    <i class="fa fa-fw fa-pencil-alt"></i>
                                                </a>
                                                <button type="submit" class="btn btn-sm text-danger js-swal-warning delete-item" data-action="{{ route('amigo.languages.destroy', $item->id) }}" data-toggle="tooltip" data-animation="true" data-placement="top" title="Remove">
                                                    <i class="fa fa-fw fa-times"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center mt-5">
                                {{ $items->links() }}
                            </div>
                        @else
                            <div class="alert alert-info text-center">
                                <h3>Wow, such empty!</h3>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@extends('amigo-pub::layouts.admin')

@section('buttons')
    <div class="btn-group w-100">
        <button class="pre_click1 btn btn-secondary" name="save" value="redirect">
            Save and exit
        </button>
        <button class="pre_click2 btn btn-primary" name="save" value="refresh">Save</button>
    </div>
@endsection

@section('head-container')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Create language
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/admin/settings">Settings</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Create language</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <form action="{{ isset($item->id) ? route('amigo.languages.update', $item->id) : route('amigo.languages.store' )}}"
          method="POST">
        @csrf
        @if (isset($item->id))
            @method('PUT')
        @else
            @method('POST')
        @endif
        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div class="block block-rounded">
                    <div class="block-content">
                        <div class="form-group row">
                            <div class="col-xl-3 col-md-1"></div>
                            <div class="col-xl-6 col-md-10">
                                <label for="title">Name</label>
                                <input type="text" name="title" class="form-control @error('title') is-invalid @enderror"
                                       id="title" value="{{ $item->title ?? old('title') }}">
                                @error('title')
                                <div class="text-danger">
                                    <small><strong>{{ $message }}</strong></small>
                                </div>
                                @enderror
                            </div>
                            <div class="col-xl-3 col-md-1"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xl-3 col-md-1"></div>
                            <div class="col-xl-6 col-md-10">
                                <label for="locale">Locale</label>
                                <input type="text" name="locale" class="form-control @error('locale') is-invalid @enderror"
                                       id="locale" value="{{ $item->locale ?? old('locale') }}">
                                @error('locale')
                                <div class="text-danger">
                                    <small><strong>{{ $message }}</strong></small>
                                </div>
                                @enderror
                            </div>
                            <div class="col-xl-3 col-md-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-xl-3 col-md-1"></div>
                            <div class="form-group col-xl-3 col-md-5">
                                <div class="custom-control custom-switch mb-4 mb-sm-0">
                                    <input type="checkbox" class="custom-control-input" id="site" name="site" {{ isset($item) && !$item->site ? '' : 'checked' }}>
                                    <label class="custom-control-label" for="site">Enable on site</label>
                                </div>
                            </div>
                            <div class="form-group col-xl-3 col-md-5">
                                <div class="custom-control custom-switch mb-4 mb-sm-0">
                                    <input type="checkbox" class="custom-control-input" id="admin" name="admin" {{ isset($item) && !$item->admin ? '' : 'checked' }}>
                                    <label class="custom-control-label" for="admin">Enable on admin</label>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-1"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-xl-4" style="display:none;">

                <button class="click1 btn btn-outline-primary mr-3 ml-4" name="save" value="redirect">
                    Save and exit
                </button>
                <button class="click2 btn btn-primary mr-3" name="save" value="refresh">Save</button>
            </div>
        </div>
    </form>
@stop

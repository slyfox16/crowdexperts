<div class="modal show" id="modal-settings-remove" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Are you sure you want to delete this setting?</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm mb-2">
                    <form action="#" method="post" class="d-flex justify-content-end">
                        @csrf
                        @method('DELETE')
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-danger ml-3">Yes, delete this setting</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
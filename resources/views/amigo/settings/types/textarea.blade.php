<div class="col-xl-3 col-md-1"></div>
<div class="col-xl-6 col-md-10">
    <div class="add__delete_item">
        <div class="full-item">
            <div class="d-flex justify-content-between align-items-center">
                <label for="{{ $setting->key }}">{{ $setting->display_name }}</label>
            </div>
            <textarea
                name="{{ $setting->key }}"
                class="form-control"
                id="{{ $setting->key }}">{{ $setting->value }}</textarea>
        </div>
        @include('amigo-pub::settings.types.actions')
    </div>
</div>
<div class="col-xl-3 col-md-1"></div>

<div class="col-xl-3 col-md-1"></div>
<div class="form-group col-xl-6 col-md-10 position-relative">
    <label>{{ $setting->display_name }} </label>
    <input type="file" name="{{ $setting->key }}" class="form-control">

    @if (!empty($setting->value))
        <a href="{{ route('amigo.settings.delete', $setting->id) }}">
            <i class="si si-ban text-danger"></i>
        </a>
        <img class="mt-3" src="{{ getUploadedImage($setting->value) }}" alt="{{ $setting->display_name }}" style="max-width:150px">
    @endif

    @include('amigo-pub::settings.types.actions')
</div>
<div class="col-xl-3 col-md-1"></div>

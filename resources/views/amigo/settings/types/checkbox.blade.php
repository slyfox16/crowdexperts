<div class="col-xl-3 col-md-1"></div>
<div class="form-group col-xl-6 col-md-10 position-relative">
    <label for="{{ $setting->key }}">{{ $setting->display_name }} </label>
    <div class="custom-control custom-switch custom-control-lg mb-2">
        <input type="checkbox" class="custom-control-input" id="{{ $setting->key }}" name="{{ $setting->key }}" {{ $setting->value == 1 ? 'checked' : '' }}>
        <label class="custom-control-label" for="{{ $setting->key }}"></label>
    </div>

    @include('amigo-pub::settings.types.actions')
</div>
<div class="col-xl-3 col-md-1"></div>

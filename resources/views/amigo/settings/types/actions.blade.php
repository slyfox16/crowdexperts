<div class="utilities position-absolute" style="top:0;right:10px">
    <a href="{{ route('amigo.settings.move_up', $setting->id) }}" class="btn-block-option">
        <i class="si si-arrow-up"></i>
    </a>
    <a href="{{ route('amigo.settings.move_down', $setting->id) }}" class="btn-block-option" >
        <i class="si si-arrow-down"></i>
    </a>
{{--    @can('delete', $setting)--}}
        <button type="button" class="btn-block-option remove-setting" data-id="{{ $setting->id }}">
            <i class="far fa-fw fa-trash-alt"></i>
        </button>
{{--    @endcan--}}
</div>
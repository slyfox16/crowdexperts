@extends('amigo-pub::layouts.admin')

@section('buttons')
    <div class="btn-group w-100">
        <button class="pre_click1 btn btn-primary" name="save" value="redirect">
            Save
        </button>
    </div>
@endsection

@section('head-container')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Site settings
                </h1>
            </div>
        </div>
    </div>
@endsection

@section('content')


    <div class="row">
        <form action="{{ route('amigo.settings.update') }}" method="post" enctype="multipart/form-data" class="d-block w-100">
            @csrf
            <div style="display:none;">
                <button class="click1 btn btn-outline-primary mr-3 ml-4" name="save" value="redirect">
                    Save and exit
                </button>
                <button class="click2 btn btn-primary mr-3" name="save" value="refresh">Save</button>
            </div>
            <div class="row m-0">
                <div class="col-12">
                    <div class="block tabs__block">
                        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">

                            <li class="nav-item">
                                <a class="nav-link @if($tab == 1) active show @endif" href="#btabs-animated-slideup-main">Main settings</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @if($tab == 2) active show @endif" href="#btabs-animated-slideup-language">Languages</a>
                            </li>
                            {{--
                            <li class="nav-item">
                                <a class="nav-link @if($tab == 3) active show @endif" href="#btabs-animated-slideup-color">Color scheme</a>
                            </li>
                            --}}
                        </ul>

                        <div class="block-content tab-content overflow-hidden">


                            <div class="tab-pane fade fade-up @if($tab == 3) active show @endif" id="btabs-animated-slideup-color" role="tabpanel">
                                <!-- Preview Color Themes -->
                                <!-- Toggle Themes functionality initialized in Template._uiHandleTheme() -->
                                <div class="row text-center">
                                    <div class="col-6 col-xl-2 py-4">
                                        <a class="item item-link-pop  bg-default text-white-75 mx-auto mb-3" data-toggle="theme" data-theme="default" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Default</div>
                                    </div>
                                    <div class="col-6 col-xl-2 py-4">
                                        <a class="item item-link-pop  bg-cyan text-white-75 mx-auto mb-3" data-toggle="theme" data-theme="assets/css/themes/amethyst.min.css" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Cyan</div>
                                    </div>
                                    <div class="col-6 col-xl-2 py-4">
                                        <a class="item item-link-pop  bg-orange text-white-75 mx-auto mb-3" data-toggle="theme" data-theme="assets/css/themes/city.min.css" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Orange</div>
                                    </div>
                                    <div class="col-6 col-xl-2 py-4">
                                        <a class="item item-link-pop  bg-green text-white-75 mx-auto mb-3" data-toggle="theme" data-theme="assets/css/themes/flat.min.css" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Green</div>
                                    </div>
                                    <div class="col-6 col-xl-2 py-4">
                                        <a class="item item-link-pop  bg-pink text-white-75 mx-auto mb-3" data-toggle="theme" data-theme="assets/css/themes/modern.min.css" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Pink</div>
                                    </div>
                                    <div class="col-6 col-xl-2 py-4">
                                        <a class="item item-link-pop  bg-grass text-white-75 mx-auto mb-3" data-toggle="theme" data-theme="assets/css/themes/smooth.min.css" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Grass</div>
                                    </div>
                                </div>
                                <hr>
                                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                <div class="row text-center">
                                    <div class="col-6 col-xl-2 offset-xl-1 py-4">
                                        <a class="item item-link-pop  bg-sidebar-light text-muted mx-auto mb-3" data-toggle="layout" data-action="sidebar_style_light" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Light Sidebar</div>
                                    </div>
                                    <div class="col-6 col-xl-2 py-4">
                                        <a class="item item-link-pop  bg-sidebar-dark text-white-75 mx-auto mb-3" data-toggle="layout" data-action="sidebar_style_dark" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Dark Sidebar</div>
                                    </div>
                                    <div class="col-6 col-xl-2 offset-xl-2 py-4">
                                        <a class="item item-link-pop  bg-header-light text-muted mx-auto mb-3" data-toggle="layout" data-action="header_style_light" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Light Header</div>
                                    </div>
                                    <div class="col-6 col-xl-2 py-4">
                                        <a class="item item-link-pop bg-header-dark text-white-75 mx-auto mb-3" data-toggle="layout" data-action="header_style_dark" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                        <div class="font-w600">Dark Header</div>
                                    </div>
                                </div>
                                <!-- END Preview Color Themes -->
                            </div>
                            <div class="tab-pane fade fade-up  @if($tab == 1) active show @endif" id="btabs-animated-slideup-main" role="tabpanel">
                                @if ($settings->isEmpty())
                                    <div class="form-group row">
                                        <div class="col-12 text-center">
                                            <h3>No settings created yet.</h3>
                                        </div>
                                    </div>
                                @else


                                    @csrf


                                    @foreach ($settings as $setting)
                                        <div class="form-group row">
                                            @include('amigo-pub::settings.types.'.$setting->type, compact($setting))
                                        </div>
                                    @endforeach


                                @endif


                                <div class="form-group row">
                                    <div class="col-12">
                                        <h5 class="add-atribut">
                                            <a href="/admin/settings/create">
                                                Add settings
                                            </a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade fade-up @if($tab == 2) active show @endif" id="btabs-animated-slideup-language" role="tabpanel">
                                <div class="form-group row">
                                    <div class="col-12 lang__table">
                                        <table class="table table-hover table-vcenter">
                                            <thead>
                                            <tr>
                                                <th class="text-left" style="min-width: 250px;">
                                                    <span for=""> Language</span>
                                                </th>
                                                <th>Code</th>
                                                <th>On site</th>
                                                <th>In admin</th>
                                                <th class="text-right" style="width: 150px;">Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($languages as $lang)
                                                <tr>
                                                    <td>
                                                        <span for=""> {{$lang['title']}}</span>
                                                    </td>
                                                    <td> {{$lang['locale']}}</td>
                                                    <td>
                                                        <div class="custom-control custom-switch mr-3">
                                                            <input type="checkbox" class="custom-control-input" checked id="lang1_{{$lang['locale']}}">
                                                            <label class="custom-control-label" for="lang1_{{$lang['locale']}}"></label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="custom-control custom-switch mr-3">
                                                            <input type="checkbox" class="custom-control-input" checked id="lang2_{{$lang['locale']}}"
                                                                   name=lang2_{{$lang['locale']}}">
                                                            <label class="custom-control-label" for="lang2_{{$lang['locale']}}"></label>
                                                        </div>
                                                    </td>
                                                    <td class="text-right" style="width: 150px;">
                                                        <div class="btn-group">
                                                            <a href="{{ route('amigo.languages.edit', $lang['id']) }}"
                                                               class="btn btn-sm btn-light" data-toggle="tooltip"
                                                               data-animation="true" data-placement="top" title="Edit">
                                                                <i class="fa fa-fw fa-pencil-alt"></i>
                                                            </a>
                                                            <button type="submit"
                                                                    class="btn btn-sm btn-light js-swal-warning delete-item"
                                                                    data-action="{{ route('amigo.languages.destroy', $lang['id']) }}"
                                                                    data-toggle="tooltip" data-animation="true" data-placement="top"
                                                                    title="Remove">
                                                                <i class="fa fa-fw fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <h5 class="add-atribut">
                                            <a href="/admin/languages/create">
                                                Add language
                                            </a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- Vertically Centered Block Modal -->
    <div class="modal fade center" id="modal-block-vcenter" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Add value</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="field1">Field1</label>
                                <span class="badge">EN</span>
                                <input
                                    type="text"
                                    name="field1"
                                    class="form-control @error('field1') is-invalid @enderror"
                                    id="field1"
                                    value="{{ $mainModel->field1 ?? old('field1') }}"
                                >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="field1">Field1</label>
                                <span class="badge">RU</span>
                                <input
                                    type="text"
                                    name="field1"
                                    class="form-control @error('field1') is-invalid @enderror"
                                    id="field1"
                                    value="{{ $mainModel->field1 ?? old('field1') }}"
                                >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="field1">Field1</label>
                                <span class="badge">RO</span>
                                <input
                                    type="text"
                                    name="field1"
                                    class="form-control @error('field1') is-invalid @enderror"
                                    id="field1"
                                    value="{{ $mainModel->field1 ?? old('field1') }}"
                                >
                            </div>
                        </div>
                    </div>
                    <div class="pb-5 text-center">
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Vertically Centered Block Modal -->

    @include('amigo-pub::settings.modal')
@endsection

@section('javascript')
    <script>
        $(function () {
            $('.remove-setting').on('click', function (event) {
                event.preventDefault();

                var settingId = $(this).attr('data-id');

                if (settingId) {
                    $('#modal-settings-remove').find('form').attr('action', '/admin/settings/' + settingId);

                    $('#modal-settings-remove').modal('show');
                }
            });
        });
    </script>
@endsection

@extends('amigo-pub::layouts.admin')

@section('buttons')
    <div class="btn-group w-100">
        <button class="pre_click1 btn btn-secondary" name="save" value="redirect">
            Save and exit
        </button>
        <button class="pre_click2 btn btn-primary" name="save" value="refresh">Save</button>
    </div>
@endsection

@section('head-container')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Create settings
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/admin/settings">Settings</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Create settings</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="block block-rounded">
                <div class="block-content">
                    <form action="{{ route('amigo.settings.store') }}" method="post" class="d-block w-100">
                        @csrf

                        <div class="row form-group">
                            <div class="col-xl-3 col-md-1"></div>
                            <div class="col-xl-6 col-md-10">
                                <label>Display name</label>
                                <input type="text" name="display_name" class="form-control @error('display_name') is-invalid @enderror" placeholder="Ex: Site Title" value="{{ old('display_name') }}">
                            </div>
                            <div class="col-xl-3 col-md-1"></div>
                        </div>

                        <div class="row form-group">
                            <div class="col-xl-3 col-md-1"></div>
                            <div class="col-xl-6 col-md-10">
                                <label>Key</label>
                                <input type="text" name="key" class="form-control @error('key') is-invalid @enderror" placeholder="Ex: site.title" value="{{ old('key') }}">
                            </div>
                            <div class="col-xl-3 col-md-1"></div>
                        </div>

                        <div class="row form-group">
                            <div class="col-xl-3 col-md-1"></div>
                            <div class="col-xl-6 col-md-10">
                                <label>Type</label>
                                <select name="type" class="form-control @error('type') is-invalid @enderror">
                                    <option value="text">Text Box</option>
                                    <option value="textarea">Text Area</option>
                                    <option value="checkbox">Check Box</option>
                                    <option value="image">Image</option>
                                </select>
                            </div>
                            <div class="col-xl-3 col-md-1"></div>
                        </div>

                        <div style="display:none;">
                            <button class="click1 btn btn-outline-primary mr-3 ml-4" name="save" value="redirect">
                                Save and exit
                            </button>
                            <button class="click2 btn btn-primary mr-3" name="save" value="refresh">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('amigo-pub::settings.modal')
@endsection


@extends('amigo-pub::layouts.admin')

@section('content')
    <div class="row block py-4">
        @if ($settings->isEmpty())
            <div class="col-12 text-center">
                <h3>No settings created yet.</h3>
            </div>
        @else
            <form action="{{ route('amigo.settings.update') }}" method="POST" class="d-block w-100" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                @foreach ($settings as $setting)
                    @include('amigo-pub::settings.types.'.$setting->type, compact($setting))
                @endforeach

                <div class="form-group d-flex justify-content-end mt-5 mr-3">
                    <button class="btn btn-primary px-5">
                        Save settings
                    </button>
                </div>
            </form>
        @endif
    </div>

    <div class="row block py-4">
        <form action="{{ route('amigo.settings.store') }}" method="post" class="d-block w-100">
            @csrf

            <div class="row m-0">
                <div class="col-md-4">
                    <label>Display name</label>
                    <input type="text" name="display_name" class="form-control @error('display_name') is-invalid @enderror" placeholder="Ex: Site Title" value="{{ old('display_name') }}">
                </div>

                <div class="col-md-4">
                    <label>Key</label>
                    <input type="text" name="key" class="form-control @error('key') is-invalid @enderror" placeholder="Ex: site.title" value="{{ old('key') }}">
                </div>

                <div class="col-md-4">
                    <label>Type</label>
                    <select name="type" class="form-control @error('type') is-invalid @enderror">
                        <option value="text">Text Box</option>
                        <option value="checkbox">Check Box</option>
                        <option value="image">Image</option>
                    </select>
                </div>
            </div>

            <div class="form-group col-12 d-flex justify-content-end mt-4">
                <button type="submit" class="btn btn-primary">Add setting</button>
            </div>
        </form>
    </div>

    @include('amigo-pub::settings.modal')
@endsection

@section('javascript')
    <script>
        $(function() {
            $('.remove-setting').on('click', function(event) {
                event.preventDefault();

                var settingId = $(this).attr('data-id');

                if (settingId) {
                    $('#modal-settings-remove').find('form').attr('action', '/admin/settings/' + settingId);

                    $('#modal-settings-remove').modal('show');
                }
            });
        });
    </script>
@endsection

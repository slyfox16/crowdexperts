@extends('amigo-pub::layouts.admin')

@section('buttons')
    <div class="btn-group w-100">
        <button class="pre_click1 btn btn-secondary" name="save" value="redirect">
            Save and exit
        </button>
        <button class="pre_click2 btn btn-primary" name="save" value="refresh">Save</button>
    </div>
@endsection

@section('head-container')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    @if($mainModel->id)
                        {{$mainModel->title}}
                    @else
                        Create new menu
                    @endif
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/admin/menu">Menu</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">{{ isset($mainModel->id) ? 'Edit' : 'Create' }} menu</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <form action="{{ isset($mainModel->id) ? route('amigo.Menu.update', $mainModel->id) : route('amigo.Menu.store' )}}"
          method="POST">
        @csrf
        @if (isset($mainModel->id))
            @method('PUT')
        @else
            @method('POST')
        @endif
        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div class="block block-rounded">
                    <div class="block-content">
                        <div class="form-group row">
                            <div class="col-xl-3 col-md-1"></div>
                            <div class="col-xl-6 col-md-10">
                                <label for="title">Title</label>
                                <input
                                    type="text"
                                    name="title"
                                    class="form-control @error('title') is-invalid @enderror"
                                    id="title"
                                    value="{{ $mainModel->title ?? old('title') }}"
                                >
                                @error('title')
                                <div class="text-danger">
                                    <small><strong>{{ $message }}</strong></small>
                                </div>
                                @enderror
                            </div>
                            <div class="col-xl-3 col-md-1"></div>
                        </div>
                        @if($mainModel->id)
                            <div class="form-group row">
                                <div class="col-xl-3 col-md-1"></div>
                                <div class="col-xl-4 col-md-7">
                                    <label for="title">Select page</label>
                                    <select name="add_page_id" class="form-control">
                                        @foreach($page_list as $page)
                                            <option value="{{$page->id}}">{{$page->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xl-2 col-md-3">
                                    <label for="title" style="width:100%;">&nbsp;</label>
                                    <button class="btn btn-primary mr-3" name="save">Add page</button>
                                </div>
                                <div class="col-xl-3 col-md-1"></div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <div style="display:none;">
            <button class="click1 btn btn-outline-primary mr-3 ml-4" name="save" value="redirect">
                Save and exit
            </button>
            <button class="click2 btn btn-primary mr-3" name="save" value="refresh">Save</button>
        </div>
    </form>
    @if($mainModel->id)

        <div class="row">

            <div class="col-12">
                <div class="block block-rounded">
                    <div class="block-content">
                        @if (!$pages->isEmpty())
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">#</th>
                                    <th>Title</th>
                                    <th class="text-center" style="width: 150px;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pages as $page)
                                    <tr>
                                        <td>{{$page->page->id}}</td>
                                        <td>{{$page->page->title}}</td>
                                        <td class="text-center" style="width: 150px;">
                                            <div class="btn-group">
                                                <a href="{{ route('amigo.amigo_pages.edit', $page->page->id) }}"
                                                   class="btn btn-sm btn-light js-tooltip-enabled" data-toggle="tooltip"
                                                   data-animation="true" data-placement="top" title="Edit">
                                                    <i class="fa fa-fw fa-pencil-alt"></i>
                                                </a>
                                                <button type="submit"
                                                        class="btn btn-sm btn-light js-tooltip-enabled js-swal-warning delete-item"
                                                        data-action="{{ route('amigo.Menu.unlinkPage', $page->id) }}"
                                                        data-toggle="tooltip" data-animation="true" data-placement="top"
                                                        title="Remove">
                                                    <i class="fa fa-fw fa-times"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                <h3>No pages yet</h3>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
    <style>
        .dd {
            position: relative;
            display: block;
            margin: 0;
            padding: 0;
            list-style: none;
            font-size: 13px;
            line-height: 20px;
        }

        .dd-list {
            display: block;
            position: relative;
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .dd-list .dd-list {
            padding-left: 30px;
        }

        .dd-collapsed .dd-list {
            display: none;
        }

        .dd-item,
        .dd-empty,
        .dd-placeholder {
            display: block;
            position: relative;
            margin: 0;
            padding: 0;
            min-height: 20px;
            font-size: 13px;
            line-height: 20px;
        }

        .dd-handle {
            display: block;
            height: 30px;
            margin: 5px 0;
            padding: 5px 10px;
            color: #333;
            text-decoration: none;
            font-weight: bold;
            border: 1px solid #ccc;
            background: #fafafa;
            background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
            background: -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
            background: linear-gradient(top, #fafafa 0%, #eee 100%);
            -webkit-border-radius: 3px;
            border-radius: 3px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        .dd-handle:hover {
            color: #2ea8e5;
            background: #fff;
        }

        .dd-item > button {
            display: block;
            position: relative;
            cursor: pointer;
            float: left;
            width: 25px;
            height: 20px;
            margin: 5px 0;
            padding: 0;
            text-indent: 100%;
            white-space: nowrap;
            overflow: hidden;
            border: 0;
            background: transparent;
            font-size: 12px;
            line-height: 1;
            text-align: center;
            font-weight: bold;
        }

        .dd-item > button:before {
            content: '+';
            display: block;
            position: absolute;
            width: 100%;
            text-align: center;
            text-indent: 0;
        }

        .dd-item > button[data-action="collapse"]:before {
            content: '-';
        }

        .dd-placeholder,
        .dd-empty {
            margin: 5px 0;
            padding: 0;
            min-height: 30px;
            background: #f2fbff;
            border: 1px dashed #b6bcbf;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        .dd-empty {
            border: 1px dashed #bbb;
            min-height: 100px;
            background-color: #e5e5e5;
            background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
            -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
            background-image: -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
            -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
            background-image: linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
            linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
            background-size: 60px 60px;
            background-position: 0 0, 30px 30px;
        }

        .dd-dragel {
            position: absolute;
            pointer-events: none;
            z-index: 9999;
        }

        .dd-dragel > .dd-item .dd-handle {
            margin-top: 0;
        }

        .dd-dragel .dd-handle {
            -webkit-box-shadow: 2px 4px 6px 0 rgba(0, 0, 0, .1);
            box-shadow: 2px 4px 6px 0 rgba(0, 0, 0, .1);
        }

        /**
         * Nestable Extras
         */

        .nestable-lists {
            display: block;
            clear: both;
            padding: 30px 0;
            width: 100%;
            border: 0;
            border-top: 2px solid #ddd;
            border-bottom: 2px solid #ddd;
        }

        #nestable-menu {
            padding: 0;
            margin: 20px 0;
        }

        #nestable-output,
        #nestable2-output {
            width: 100%;
            height: 7em;
            font-size: 0.75em;
            line-height: 1.333333em;
            font-family: Consolas, monospace;
            padding: 5px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        #nestable2 .dd-handle {
            color: #fff;
            border: 1px solid #999;
            background: #bbb;
            background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
            background: -moz-linear-gradient(top, #bbb 0%, #999 100%);
            background: linear-gradient(top, #bbb 0%, #999 100%);
        }

        #nestable2 .dd-handle:hover {
            background: #bbb;
        }

        #nestable2 .dd-item > button:before {
            color: #fff;
        }

        .dd-hover > .dd-handle {
            background: #2ea8e5 !important;
        }

        /**
         * Nestable Draggable Handles
         */

        .dd3-content {
            display: block;
            height: 30px;
            margin: 5px 0;
            padding: 5px 10px 5px 40px;
            color: #333;
            text-decoration: none;
            font-weight: bold;
            border: 1px solid #ccc;
            background: #fafafa;
            background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
            background: -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
            background: linear-gradient(top, #fafafa 0%, #eee 100%);
            -webkit-border-radius: 3px;
            border-radius: 3px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        .dd3-content:hover {
            color: #2ea8e5;
            background: #fff;
        }

        .dd-dragel > .dd3-item > .dd3-content {
            margin: 0;
        }

        .dd3-item > button {
            margin-left: 30px;
        }

        .dd3-handle {
            position: absolute;
            margin: 0;
            left: 0;
            top: 0;
            cursor: pointer;
            width: 30px;
            text-indent: 100%;
            white-space: nowrap;
            overflow: hidden;
            border: 1px solid #aaa;
            background: #ddd;
            background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
            background: -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
            background: linear-gradient(top, #ddd 0%, #bbb 100%);
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .dd3-handle:before {
            content: '≡';
            display: block;
            position: absolute;
            left: 0;
            top: 3px;
            width: 100%;
            text-align: center;
            text-indent: 0;
            color: #fff;
            font-size: 20px;
            font-weight: normal;
        }

        .dd3-handle:hover {
            background: #ddd;
        }
    </style>
@stop

@section('javascript')
    <script>
        $(function () {
            $('.categories-dd').nestable({
                expandBtnHTML: '',
                collapseBtnHTML: ''
            });

            $('.categories-dd').on('change', event => {
                $.post('{{ route('amigo.Menu.position') }}', {
                    position: JSON.stringify($('.categories-dd').nestable('serialize')),
                    _token: '{{ csrf_token() }}'
                }).done(() => {
                    console.log('Pages item reorder');
                }).fail(err => {
                    console.error('Pages item reorder: ', err);
                });
            });
        });
    </script>
@stop

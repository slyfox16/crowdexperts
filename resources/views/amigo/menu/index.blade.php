@extends('amigo-pub::layouts.admin')

@section('buttons')
    <a class="btn btn-primary" href="{{ route('amigo.Menu.create') }}">New Menu</a>
@endsection

@section('head-container')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Menu
                </h1>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">

        <div class="col-12">
            <div class="block block-rounded">
                <div class="block-content">
                    @if (!$mainModel->isEmpty())
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">#</th>
                                <th>Title</th>
                                <th class="text-center" style="width: 150px;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($mainModel as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->title }}</td>
                                    <td class="text-center" style="width: 150px;">
                                        <div class="btn-group">
                                            <a href="{{ route('amigo.Menu.edit', $row->id) }}"
                                               class="btn btn-sm btn-light js-tooltip-enabled" data-toggle="tooltip"
                                               data-animation="true" data-placement="top" title="Edit">
                                                <i class="fa fa-fw fa-pencil-alt"></i>
                                            </a>
                                            <button type="submit"
                                                    class="btn btn-sm btn-light js-tooltip-enabled js-swal-warning delete-item"
                                                    data-action="{{ route('amigo.Menu.destroy', $row->id) }}"
                                                    data-toggle="tooltip" data-animation="true" data-placement="top"
                                                    title="Remove">
                                                <i class="fa fa-fw fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-center mt-5">
                            {{ $mainModel->links() }}
                        </div>
                    @else
                        <div class="alert text-center">
                            <h3>Nothing has been found!</h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

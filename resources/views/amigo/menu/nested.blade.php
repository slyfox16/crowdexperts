<ol class="dd-list">
    @foreach ($categories as $category)
        <li class="dd-item d-flex flex-wrap justify-content-between align-items-center publish-item" data-item-id="{{ $category->id }}">
            <div class="dd-handle"><i class="fa fa-arrows-alt fa-1x"></i></div>
            <div class="font-w600 font-size-sm flex-fill">
                {{ $category->page->title }}
            </div>
            <div class="text-center" style="width: 150px;">
                <div class="btn-group">
                    <a href="{{ route('amigo.amigo_pages.edit', $category->page->id) }}"
                       class="btn btn-sm btn-light js-tooltip-enabled" data-toggle="tooltip"
                       data-animation="true" data-placement="top" title="Edit">
                        <i class="fa fa-fw fa-pencil-alt"></i>
                    </a>
                    <button type="submit"
                            class="btn btn-sm btn-light js-tooltip-enabled js-swal-warning delete-item"
                            data-action="{{ route('amigo.Menu.unlinkPage', $category->id) }}"
                            data-toggle="tooltip" data-animation="true" data-placement="top"
                            title="Remove">
                        <i class="fa fa-fw fa-times"></i>
                    </button>
                </div>
            </div>
            @if(!empty($category->children))

                @include('amigo-pub::menu.nested', ['categories' => $category->children])
            @endif
        </li>
    @endforeach
</ol>

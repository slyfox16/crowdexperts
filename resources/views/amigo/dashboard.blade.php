@extends('amigo-pub::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-6">
            @if (!empty($dates))
                <div class="row">
                    <div class="col-12">
                        <div class="block">
                            <div class="block-header">
                                <h3 class="block-title">Google Analytics</h3>
                            </div>
                            <div class="block-content block-content-full text-center">
                                <div class="py-3">
                                    <canvas id="analytics-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@if (!empty($dates))
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
@stop

@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script>
        var ctx = document.getElementById('analytics-chart').getContext('2d');
        new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: {!! json_encode($dates) !!},
                datasets: [{
                    label: "Page views",
                    backgroundColor: 'rgba(73, 190, 170, 0.24)',
                    borderColor: '#49BEAA',
                    data: {!! json_encode($page_views) !!}
                },
                    {
                        label: "Visitors",
                        backgroundColor: 'rgba(69, 105, 144, 0.24)',
                        borderColor: '#456990',
                        data: {!! json_encode($visitors) !!}
                    }
                ]},

            // Configuration options go here
            options: {
                responsive: true
            }
        });
    </script>
@stop
@endif

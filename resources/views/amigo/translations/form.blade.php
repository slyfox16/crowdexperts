@extends('amigo-pub::layouts.admin')

@section('content')
    <form action="{{ isset($translation->id) ? route('amigo.translations.update', $translation->id) : route('amigo.translations.store' )}}"
          method="POST">
        @csrf
        @if (isset($translation->id))
            @method('PUT')
        @else
            @method('POST')
        @endif
        <div class="row">
            <div class="col-lg-7 col-xl-8">
                <div class="block block-fx-shadow">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">{{ isset($translation->id) ? 'Edit' : 'Create' }} Translation</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group row">
                            <div class="col-12">
                                <label> Key </label>
                                <input
                                    type="text"
                                    name="key"
                                    id="key"
                                    class="form-control py-4"
                                    value="{{ isset($translation->key) ? $translation->key : old('key') }}"
                                >
                                @error('key')
                                <div class="text-danger">
                                    <small><strong>{{ $message }}</strong></small>
                                </div>
                                @enderror
                            </div>
                        </div>

                        @foreach($languages as $lang)
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="text-{{ $lang['locale'] }}">
                                        Text <span class="badge badge-danger">{{ strtoupper($lang['locale']) }}</span>
                                    </label>
                                    <input
                                            type="text"
                                            name="text[{{ $lang['locale'] }}]"
                                            id="text-{{ $lang['locale'] }}"
                                            class="form-control py-4"
                                            value="{{ isset($translation) && $translation->getTranslation($lang['locale'], 'text') ? $translation->getTranslation($lang['locale'], 'text') : old('text[' . $lang['locale'] . ']') }}"
                                    >
                                    @error('text.' . $lang['locale'])
                                    <div class="text-danger">
                                        <small><strong>{{ str_replace('.', ' ', $message) }}</strong></small>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-xl-4">
                <div id="page-sidebar">
                    <div class="sidebar__inner">
                        <div class="block block-fx-shadow">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Translation Options</h3>
                            </div>
                            <div class="block-content">
                                <div class="form-group my-4 d-sm-flex justify-content-sm-between align-items-sm-center">
                                    <div class="btn-group w-100">
                                        <button class="btn btn-outline-primary px-2 mr-4" name="save" value="redirect">
                                            Save and exit
                                        </button>
                                        <button class="btn btn-primary px-2" name="save" value="refresh">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
@stop

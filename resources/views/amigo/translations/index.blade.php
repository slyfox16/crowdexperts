@extends('amigo-pub::layouts.admin')

@section('buttons')
    <div class="btn-group w-100">
        <button class="pre_click2 btn btn-primary mr-3" name="save" value="refresh">Save</button>
    </div>
@endsection

@section('crumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-alt">
            <li class="breadcrumb-item active" aria-current="page">Translations</li>
        </ol>
    </nav>
@endsection

@section('content')

    <form method="GET" id="search-form">
        <div class="row pt-4 pb-4 pr-3 pl-2">
            <div class="col-12 col-md-6 col-lg-4 col-xl-3 d-flex align-items-center">

                <div class="input-group ">
                    <input type="text" name="search" class="form-control bg-white border-right-0" placeholder="Search...">
                    <div class="input-group-append">
                        <button class="input-group-text bg-white">
                            <i class="si si-magnifier"></i>
                        </button>
                    </div>
                </div>

            </div>
            <div class="col-12 col-md-4 col-lg-2 offset-md-2  offset-lg-6 offset-xl-7">
                <select name="category" class="form-control category-select">
                    <option value="0">All categories</option>
                    @foreach($categories as $id=>$category)
                        <option @if(request('category') == $id) selected @endif value="{{$id}}">{{$category}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </form>

    <form method="POST" action="/admin/translations/save">
        @csrf
        <div class="table-out translations__table">
            @if ($translations->isEmpty())
                <div class="col-12 text-center py-4">
                    <h3>No transaltions found.</h3>
                </div>
            @else
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr>
                        <th style="width: 200px;">Key</th>
                        <th style="width: 200px;">Category</th>
                        @foreach($languages as $lang)
                            <th class="">{{ $lang['locale'] }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($translations as $translation)
                        <tr>
                            <td style="max-width: 220px" class="font-w600 font-size-sm">
                                {{ $translation->key }}
                            </td>
                            <th scope="row">
                                <select name="category[{{$translation->id}}]" class="form-control">
                                    @foreach($categories as $id=>$category)
                                        <option @if($translation->category_id == $id) selected @endif value="{{$id}}">{{$category}}</option>
                                    @endforeach
                                </select>
                            </th>

                            @foreach($languages as $lang)
                                <td class="font-w600 font-size-sm txt2">
                                    {{--
                                    Заменяем ID категории на нужный и подставляем вместо текущего варианта чтобы вывести текстовый редактор
                                    <textarea rows='1' data-min-rows='3' name="translation[{{$translation->id}}][{{$lang['locale']}}]"
                                              class="form-control autoExpand @if($translation->category_id ==3) ckeditor @endif">
                                    --}}
                                    <textarea rows='1' data-min-rows='3' name="translation[{{$translation->id}}][{{$lang['locale']}}]"
                                              class="form-control autoExpand @if($translation->category_id ==3) ckeditor @endif">
                                        {{isset($translation) && $translation->getTranslation($lang['locale'], 'text') ? $translation->getTranslation($lang['locale'], 'text') : old('text[' . $lang['locale'] . ']')}}
                                    </textarea>
                                </td>
                            @endforeach
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
        <div style="display:none;">
            <button class="click2 btn btn-primary mr-3" name="save" value="refresh">Save</button>
        </div>
    </form>
@endsection

@push('scripts')
    <script>
        $('.category-select').change(function () {
            $('#search-form').submit();
        })
    </script>
@endpush

<div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="delete-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Are you sure?</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <form action="#" id="delete-form" method="post">
                    @csrf
                    @method('DELETE')
                    <div class="block-content font-size-sm">
                        <h4 class="text-danger">Are you sure you want to delete this menu item?</h4>
                    </div>

                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-light" data-dismiss="modal">No, Cancel</button>
                        <button type="submit" class="btn btn-danger">
                            <i class="fa fa-times mr-1"></i> Yes, Delete
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="edit-model-modal" tabindex="-1" role="dialog" aria-labelledby="edit-model-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-success">
                    <h3 class="block-title">Edit menu item</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <form action="#" id="edit-model-form" method="post">
                    @method('POST')
                    <input type="hidden" name="type" value="model">
                    <input type="hidden" id="item-id" name="id" value="">

                    <div class="block-content font-size-sm">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" class="form-control" value="">
                        </div>
                    </div>

                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-light" data-dismiss="modal">No, Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="edit-link-modal" tabindex="-1" role="dialog" aria-labelledby="edit-model-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-success">
                    <h3 class="block-title">Edit menu item</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <form action="#" id="edit-link-form" method="post">
                    @method('POST')
                    <input type="hidden" name="type" value="link">
                    <input type="hidden" id="item-id" name="id" value="">

                    <div class="block-content font-size-sm">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" class="form-control" value="">
                        </div>
                    </div>

                    <div class="block-content font-size-sm">
                        <div class="form-group">
                            <label for="title">URL</label>
                            <input type="text" name="url" id="url" class="form-control" value="">
                        </div>
                    </div>

                    <div class="block-content font-size-sm">
                        <div class="form-group">
                            <label for="target">Target</label>
                            <select name="target" id="target" class="form-control">
                                <option value="_self">Self</option>
                                <option value="_blank">Blank</option>
                            </select>
                        </div>
                    </div>

                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-light" data-dismiss="modal">No, Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

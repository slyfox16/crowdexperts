@extends('amigo-pub::layouts.admin')

@section('buttons')
    <div class="btn-group lang__switcher" id="language-switcher" role="group">
        @foreach ($languages as $key => $lang)
            <button type="button" class="btn lang-button btn-outline-danger {{ $key === 0 ? 'btn-danger' : 'btn-light'}}"
                    data-locale="{{ $lang['locale'] }}">{{ $lang['title'] }}</button>
        @endforeach
    </div>
    <button class="pre_click1 btn btn-secondary" name="save" value="redirect">
        Save and exit
    </button>
    <button class="pre_click2 btn btn-primary" name="save" value="refresh">Save</button>
@endsection

@section('head-container')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    @if($mainModel->id)
                        {{$mainModel->title}}
                    @else
                        Create new page
                    @endif
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/admin/amigo_pages">Pages</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">{{ isset($mainModel->id) ? 'Edit' : 'Create' }} page</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <form action="{{ isset($mainModel->id) ? route('amigo.amigo_pages.update', $mainModel->id) : route('amigo.amigo_pages.store' )}}"
          method="POST">
        @csrf
        @if (isset($mainModel->id))
            @method('PUT')
        @else
            @method('POST')
        @endif
        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div class="block block-rounded">
                    <div class="menu_tabs">

                        <div class="block tabs__block">
                            <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                <li class="nav-item">
                                    <a style="padding-right: 25px" class="nav-link active" href="#btabs-animated-slideup-main">Main block</a>
                                </li>
                                <li class="nav-item">
                                    <a style="padding-right: 25px" class="nav-link" href="#btabs-animated-slideup-seo">SEO block</a>
                                </li>
                            </ul>

                            <div class="block-content tab-content overflow-hidden">
                                <div class="tab-pane fade fade-up show active" id="btabs-animated-slideup-main" role="tabpanel">

                                    <div class="form-group row">
                                        <div class="col-xl-3 col-md-1"></div>
                                        <div class="col-xl-6 col-md-10 position-relative">
                                            <label for="page_type">Page type</label>
                                            <select class="form-control" name="page_type" id="page_type">
                                                <option value="1">Page</option>
                                                <option value="2" @if($mainModel->page_type == 2) selected @endif>External URL</option>
                                            </select>
                                        </div>
                                        <div class="col-xl-3 col-md-1"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xl-3 col-md-1"></div>
                                        @foreach ($languages as $lang)
                                            <div class="col-xl-6 col-md-10" data-lang="{{ $lang['locale'] }}">
                                                <label for="title-{{ $lang['locale'] }}">Title <span class="badge">{{ strtoupper($lang['locale']) }}</span></label>
                                                <input
                                                        type="text"
                                                        name="title[{{ $lang['locale'] }}]"
                                                        class="form-control @error('title.' . $lang['locale']) is-invalid @enderror"
                                                        id="title-{{ $lang['locale'] }}"
                                                        value="{{ isset($mainModel) ? $mainModel->getTranslation('title', $lang['locale'] ) : old('title.' . $lang['locale']) }}"
                                                >
                                                @error('title.' . $lang['locale'])
                                                <div class="text-danger">
                                                    <small><strong>{{ str_replace('.' . $lang['locale'], '', $message) }}</strong></small>
                                                </div>
                                                @enderror
                                            </div>
                                        @endforeach
                                        <div class="col-xl-3 col-md-1"></div>
                                    </div>
                                    <div class="form-group row page_elements">
                                        <div class="col-xl-3 col-md-1"></div>
                                        <div class="col-xl-6 col-md-10 position-relative">
                                            <label for="slug">Slug</label>
                                            <input
                                                    type="text"
                                                    name="slug"
                                                    class="form-control @error('slug') is-invalid @enderror"
                                                    id="slug"
                                                    value="{{ $mainModel->slug ?? old('slug') }}"
                                                    data-generate-slug="{{ isset($mainModel) ? 'false' : 'true' }}"
                                            >
                                            <div class="position-absolute" style="top:46%;right:23px">
                                                <button type="button" class="btn-block-option edit-slug">
                                                    <div class="pen-icon">
                                                        <svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                    d="M10.207 2.75781c-.1015-.10156-.25388-.10156-.35544 0L4.31641 8.29297 4.0625 10.6543c-.05078.3047.22852.584.5332.5332l2.36133-.2539 5.53517-5.53516c.1016-.10156.1016-.25391 0-.35547L10.207 2.75781zm4.1133-.58398L13.0762.929688c-.3809-.38086-1.0157-.38086-1.3965 0l-.8887.888672c-.1015.10156-.1015.25391 0 .35547l2.2852 2.28515c.1015.10157.2539.10157.3554 0l.8887-.88867c.3809-.38086.3809-1.01562 0-1.39648zM9.75 9.43555V12H1.625V3.875h5.81445c.10157 0 .17774-.02539.22852-.07617L8.68359 2.7832c.17774-.20312.05079-.5332-.22851-.5332H1.21875C.533203 2.25 0 2.80859 0 3.46875v8.93745c0 .6856.533203 1.2188 1.21875 1.2188h8.93745c.6602 0 1.2188-.5332 1.2188-1.2188V8.41992c0-.2793-.3301-.40625-.5332-.22851L9.82617 9.20703c-.05078.05078-.07617.12695-.07617.22852z"/>
                                                        </svg>

                                                    </div>
                                                </button>
                                            </div>
                                            @error('slug')
                                            <div class="text-danger">
                                                <small><strong>{{ $message }}</strong></small>
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="col-xl-3 col-md-1"></div>
                                    </div>

                                    <div class="form-group row external_elements">
                                        <div class="col-xl-3 col-md-1"></div>
                                        <div class="col-xl-6 col-md-10 position-relative">
                                            <label for="external_url">URL</label>
                                            <input
                                                    type="text"
                                                    name="external_url"
                                                    class="form-control @error('external_url') is-invalid @enderror"
                                                    id="external_url"
                                                    value="{{ $mainModel->external_url ?? old('external_url') }}"
                                            >
                                            @error('external_url')
                                            <div class="text-danger">
                                                <small><strong>{{ $message }}</strong></small>
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="col-xl-3 col-md-1"></div>
                                    </div>

                                    <div class="form-group row page_elements">
                                        <div class="col-xl-3 col-md-1"></div>
                                        @foreach ($languages as $lang)
                                            <div class="col-xl-6 col-md-10 @error('text.' . $lang['locale']) is-invalid @enderror" data-lang="{{ $lang['locale'] }}">
                                                <label for="text-{{ $lang['locale'] }}">Text <span class="badge">{{ strtoupper($lang['locale']) }}</span></label>
                                                <textarea
                                                        name="text[{{ $lang['locale'] }}]"
                                                        id="text-{{ $lang['locale'] }}"
                                                        class="ckeditor">{{ isset($mainModel) ? $mainModel->getTranslation('text', $lang['locale']) : old('text.' . $lang['locale']) }}</textarea>
                                                @error('text.' . $lang['locale'])
                                                <div class="text-danger">
                                                    <small><strong>{{ str_replace('.' . $lang['locale'], '', $message) }}</strong></small>
                                                </div>
                                                @enderror
                                            </div>
                                        @endforeach
                                        <div class="col-xl-3 col-md-1"></div>
                                    </div>
                                </div>
                                <div class="tab-pane fade fade-up" id="btabs-animated-slideup-seo" role="tabpanel">
                                    <div class="form-group row">
                                        <div class="col-xl-3 col-md-1"></div>
                                        @foreach ($languages as $lang)
                                            <div class="col-xl-6 col-md-10" data-lang="{{ $lang['locale'] }}">
                                                <label for="seo_title-{{ $lang['locale'] }}">SEO title <span class="badge">{{ strtoupper($lang['locale']) }}</span></label>
                                                <input
                                                        type="text"
                                                        name="seo_title[{{ $lang['locale'] }}]"
                                                        class="form-control @error('seo_title.' . $lang['locale']) is-invalid @enderror"
                                                        id="seo_title-{{ $lang['locale'] }}"
                                                        value="{{ isset($mainModel) ? $mainModel->getTranslation('seo_title', $lang['locale'] ) : old('seo_title.' . $lang['locale']) }}"
                                                >
                                                @error('seo_title.' . $lang['locale'])
                                                <div class="text-danger">
                                                    <small><strong>{{ str_replace('.' . $lang['locale'], '', $message) }}</strong></small>
                                                </div>
                                                @enderror
                                            </div>
                                        @endforeach
                                        <div class="col-xl-3 col-md-1"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xl-3 col-md-1"></div>
                                        @foreach ($languages as $lang)
                                            <div class="col-xl-6 col-md-10" data-lang="{{ $lang['locale'] }}">
                                                <label for="seo_keywords-{{ $lang['locale'] }}">SEO keywords <span class="badge ">{{ strtoupper($lang['locale']) }}</span></label>
                                                <input
                                                        type="text"
                                                        name="seo_keywords[{{ $lang['locale'] }}]"
                                                        class="form-control @error('seo_keywords.' . $lang['locale']) is-invalid @enderror"
                                                        id="seo_keywords-{{ $lang['locale'] }}"
                                                        value="{{ isset($mainModel) ? $mainModel->getTranslation('seo_keywords', $lang['locale'] ) : old('seo_keywords.' . $lang['locale']) }}"
                                                >
                                                @error('seo_keywords.' . $lang['locale'])
                                                <div class="text-danger">
                                                    <small><strong>{{ str_replace('.' . $lang['locale'], '', $message) }}</strong></small>
                                                </div>
                                                @enderror
                                            </div>
                                        @endforeach
                                        <div class="col-xl-3 col-md-1"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xl-3 col-md-1"></div>
                                        @foreach ($languages as $lang)
                                            <div class="col-xl-6 col-md-10" data-lang="{{ $lang['locale'] }}">
                                                <label for="seo_description-{{ $lang['locale'] }}">SEO description <span class="badge">{{ strtoupper($lang['locale']) }}</span></label>
                                                <input
                                                        type="text"
                                                        name="seo_description[{{ $lang['locale'] }}]"
                                                        class="form-control @error('seo_description.' . $lang['locale']) is-invalid @enderror"
                                                        id="seo_description-{{ $lang['locale'] }}"
                                                        value="{{ isset($mainModel) ? $mainModel->getTranslation('seo_description', $lang['locale'] ) : old('seo_description.' . $lang['locale']) }}"
                                                >
                                                @error('seo_description.' . $lang['locale'])
                                                <div class="text-danger">
                                                    <small><strong>{{ str_replace('.' . $lang['locale'], '', $message) }}</strong></small>
                                                </div>
                                                @enderror
                                            </div>
                                        @endforeach
                                        <div class="col-xl-3 col-md-1"></div>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display:none;">
                <button class="click1 btn btn-outline-primary mr-3 ml-4" name="save" value="redirect">
                    Save and exit
                </button>
                <button class="click2 btn btn-primary mr-3" name="save" value="refresh">Save</button>
            </div>
    </form>
    <style>
        .nav-item {
            position: relative;
        }
    </style>
@stop

@push('scripts')
    <script>
        function pageTypeView() {
            val = $('#page_type').val();

            if (val == 1) {
                $('.page_elements').show();
                $('.external_elements').hide();
            } else {
                $('.page_elements').hide();
                $('.external_elements').show();
            }
        }

        $(document).ready(function () {
            var ind = $('.nav-item').length - 2;
            pageTypeView();

            $('#page_type').change(function () {
                pageTypeView();
            })

        })
    </script>
@endpush

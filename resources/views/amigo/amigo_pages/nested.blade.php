<ol class="dd-list">
    @foreach ($categories as $category)
        <li class="dd-item d-flex flex-wrap justify-content-between align-items-center publish-item {{ !$category->published ? 'table-danger' : '' }}" data-id="{{ $category->id }}">
            <div class="dd-handle"><i class="fa fa-arrows-alt fa-1x"></i></div>
            <div class="font-w600 font-size-sm flex-fill" data-item-id="{{ $category->id }}">
                {{ $category->title }}
            </div>
            <div class="text-center" style="width: 150px;">
                <div class="btn-group">
                    <a href="{{ route('amigo.amigo_pages.edit', $category->id) }}"
                       class="btn" data-toggle="tooltip"
                       data-animation="true" data-placement="top" title="Edit">
                        <i class="fa fa-fw fa-pencil-alt"></i>
                    </a>
                    <button type="button"
                            class="btn eye-btn js-tooltip-enabled toggle-publish"
                            data-toggle="tooltip" data-animation="true" data-placement="top"
                            title="Publish" data-id="{{ $category->id }}"
                            data-model="{{ get_class($category) }}">
                        <i class="fa fa-fw fa-eye"></i>
                    </button>
                    <button type="submit"
                            class="btn js-swal-warning delete-item"
                            data-action="{{ route('amigo.amigo_pages.destroy', $category->id) }}"
                            data-toggle="tooltip" data-animation="true" data-placement="top"
                            title="Remove">
                        <i class="fa fa-fw fa-times"></i>
                    </button>
                </div>
            </div>
            @if(!empty($category->children))

                @include('amigo-pub::amigo_pages.nested', ['categories' => $category->children])
            @endif
        </li>
    @endforeach
</ol>

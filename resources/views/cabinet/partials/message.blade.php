<aside class="overlay">
    <button class="overlay-toggler burger text-black" type="button">
        <span></span>
    </button>
    <div class="overlay-body">
        <div>
            <div class="row">
                <div class="col">
                    <span class="eyebrow text-secondary mb-1">Get in touch with</span>
                    <h2 class="h1 mb-2">{{ $user->name }}</h2>
                </div>
            </div>
            {!! form_start($messageForm) !!}
            <fieldset class="row gutter-2">
                <div class="col-12">
                    {!! form_row($messageForm->message) !!}
                </div>
            </fieldset>

            <input type="hidden" name="user_id" value="{{ $user->username }}">
            <input type="submit" hidden id="submit-hidden">
            {!! form_end($messageForm, false) !!}
        </div>
    </div>
    <div class="overlay-footer">
        <div>
            {!! form_row($messageForm->send) !!}
        </div>
    </div>
</aside>

@script
<script>
    $('#filter-form-submit').on('click', function (e) {
        $('#submit-hidden').click();
    });
</script>
@endscript


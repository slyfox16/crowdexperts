@extends('frontend.layouts.app')

@section('header')
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="font-weight-bold mb-1">Inbox</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Inbox</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')

    <chat-component />

@php Asset::js(mix('/js/chat.js')) @endphp
@endsection

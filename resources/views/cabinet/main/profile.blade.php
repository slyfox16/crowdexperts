@extends('frontend.layouts.app')

@section('header', '')

@section('sidebar')
    @include('cabinet.partials.message')
@endsection

@section('content')

    <section class="page-title">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('cabinet.experts') }}">Experts</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $user->name }}</li>
                        </ol>
                    </nav>
                </div>

                @auth
                    @if (Auth::id() != $user->id)
                        <div class="col-lg-6 text-lg-right">
                            <ul class="page-options">
                                <button id="add_to_chat" class="btn btn-primary overlay-toggler" type="button">
                                    Get in touch
                                </button>
                            </ul>
                        </div>
                    @endif
                @endauth
            </div>
        </div>
    </section>


    <section class="bg-light">
        <div class="container mt-0">

            <div class="row gutter-4 gutter-lg-6">
                <div class="col-6 col-md-4 col-lg-3">
                    <div class="card circle equal">
                        <figure class="image" style="background-image: url('{{ schema_asset($user->resize('avatar', 476, 476)) }}')"></figure>
                    </div>
                </div>
                <div class="col-md-10 col-lg-6">
                    <div class="row">
                        <div class="col">
                            <h1 class="mb-2">{{ $user->name }}</h1>
                            {!! nl2p($user->description) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <ul class="meta">
                                <li>
                                    <span><i class="icon-map-pin mr-1"></i> {{ $user->getCountryCity() }}</span>
                                </li>
                                @if($user->phone)
                                <li>
                                    <span><i class="icon-phone mr-1"></i> {{ $user->phone }}</span>
                                </li>
                                @endif
                                @if($user->web_url)
                                <li>
                                    <a href="{{ $user->web_url }}"><i class="icon-link mr-1"></i> {{ $user->web_url }}</a>
                                </li>
                                @endif
                                @if($user->facebook_url)
                                    <li>
                                        <a href="{{ $user->facebook_url }}" class="icon-facebook-2"></a>
                                    </li>
                                @endif
                                @if($user->twitter_url)
                                    <li>
                                        <a href="{{ $user->twitter_url }}" class="icon-twitter-2"></a>
                                    </li>
                                @endif
                                @if($user->linkedin_url)
                                    <li>
                                        <a href="{{ $user->linkedin_url }}" class="icon-linkedin-2"></a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 text-lg-right">
                    <div class="row gutter-4 gutter-lg-6">
                        @if (optional($user->specialities)->isNotEmpty())
                            <div class="col-md-4 col-lg-12">
                                <span class="eyebrow text-muted d-block mb-2">Qualifizierung</span>
                                @foreach($user->specialities as $speciality)
                                    <div class="pin" data-toggle="tooltip" data-placement="top" title="{{ $speciality->speciality }}">
                                        {{ $speciality->abbr }}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </section>

    @if ($showInfo)
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="accordion" id="accordion-1">
                        @if($user->specialities->isNotEmpty())
                            <div class="card">
                                <div class="card-header" id="heading-1-1">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-1-1" aria-expanded="false" aria-controls="collapse-1-1">
                                            Fachgebiete
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapse-1-1" class="collapse" aria-labelledby="heading-1-1" data-parent="#accordion-1">
                                    <div class="card-body">
                                        <ul class="meta">
                                            @foreach ($user->specialities as $speciality)
                                                <li>
                                                    <span>{{ $speciality->speciality }}</span>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($user->associations->isNotEmpty())
                            <div class="card">
                                <div class="card-header" id="heading-1-2">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-1-2" aria-expanded="false" aria-controls="collapse-1-2">
                                            Verbandszugehörigkeit
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse-1-2" class="collapse" aria-labelledby="heading-1-2" data-parent="#accordion-1">
                                    <div class="card-body">
                                        <ul class="meta">
                                            @foreach ($user->associations as $association)
                                                <li>
                                                    <span>
                                                      <img src="{{ schema_asset($association->getFile('logo')) }}" data-toggle="tooltip" data-placement="top" title="{{ $association->speciality }}">
                                                    </span>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($user->events->isNotEmpty())
                            <div class="card">
                                <div class="card-header" id="heading-1-3">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-1-3" aria-expanded="false" aria-controls="collapse-1-3">
                                            Experte(in) nimmt an folgenden event(s) teil
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse-1-3" class="collapse" aria-labelledby="heading-1-3" data-parent="#accordion-1">
                                    <div class="card-body">
                                        <div class="row gutter-2">
                                            @foreach ($user->events as $event)
                                                <div class="col-12">
                                                    <a href="{{ route('cabinet.event', $event) }}" class="event">
                                                        <time>{{ optional($event->date_start)->format('d F Y') }}</time>
                                                        <h2 class="event-title">{{ $event->title }}</h2>
                                                        <span class="event-location">{{ $event->getCountryCity() }}</span>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($user->projects->isNotEmpty())
                            <div class="card">
                                <div class="card-header" id="heading-1-4">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-1-4" aria-expanded="false" aria-controls="collapse-1-4">
                                            Projekte
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse-1-4" class="collapse" aria-labelledby="heading-1-4" data-parent="#accordion-1">
                                    <div class="card-body">
                                        <div class="row gutter-2">
                                            @foreach ($user->projects as $project)
                                                <div class="col-12">
                                                    <a href="{{ $project->link }}" class="event">
                                                        <time>{{ optional($project->date)->format('d F Y') }}</time>
                                                        <h2 class="event-title">{{ $project->name }}</h2>
                                                        <img src="{{ schema_asset($project->getFile('image')) }}">
                                                        <p class="mt-1">{{ $project->description }}</p>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif

    @php Asset::js('/js/share.js') @endphp
    @php Asset::js(mix('/js/login_register_validate.js')) @endphp
@endsection

@script
<script>
    $('#add_to_chat').on('click', function(e) {
        e.preventDefault();

        $.ajax({
            url: '{{ route('cabinet.user.addToChat', $user) }}',
            type: 'GET',
        })
    });
</script>
@endscript

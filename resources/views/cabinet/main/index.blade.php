@extends('frontend.layouts.app')

@section('header')
    <section class="pb-0">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="font-weight-bold">Account Settings</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')

    <section class="pb-0">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="nav nav-tabs" id="component-1" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#component-1-1" role="tab" aria-controls="component-1-1" aria-selected="true">Allgemein</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#component-1-2" role="tab" aria-controls="component-1-2" aria-selected="false">Kontakte</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#component-1-3" role="tab" aria-controls="component-1-3" aria-selected="false">Auswahl der Zertfizierungen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#component-1-4" role="tab" aria-controls="component-1-4" aria-selected="false">Projekte</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="change-photo">
                        <a id="fakeUpload" href="" class="btn btn-primary btn-icon btn-rounded"><i class="icon-upload"></i></a>

                        <div class="card circle equal">
                            <figure id="upload" class="image" style="background-image: url('{{ schema_asset($user->resize('avatar', 476, 476)) }}')"></figure>
                        </div>

                        <form id="imageUploadForm" enctype="multipart/form-data" method="post">
                            {!! form_widget($avatarForm->avatar) !!}
                        </form>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="tab-content" id="component-1-content">
                        <div class="tab-pane fade show active" id="component-1-1" role="tabpanel" aria-labelledby="component-1-1">
                            {!! form_start($cabinetForm) !!}
                                <fieldset class="row gutter-2">
                                    <div class="col-md-6">
                                        {!! form_row($cabinetForm->title) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row($cabinetForm->name) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row($cabinetForm->username) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row($cabinetForm->email) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($cabinetForm->specialities) !!}
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="inputText6">{!! form_label($cabinetForm->association_membership) !!}</label>
                                            <div class="input-group">

                                                {!! form_widget($cabinetForm->association_membership) !!}

                                                <div class="input-group-append">
                                                    <button class="btn btn-primary btn-icon" type="button" id="button-addon1">
                                                        <i class="icon-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <ul id="associations" class="pin-list">
                                            @foreach($associations as $association)
                                                <li>
                                                    {{ $association->name }}
                                                    <a id="assoc{{ $association->id }}" href="" class="icon-x assoc-delete" data-associd="{{ $association->id }}"></a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </fieldset>
                                <fieldset class="row gutter-2">
                                    <legend class="legend">Firma</legend>
                                    <div class="col-md-6">
                                        {!! form_row($cabinetForm->companyname) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row($cabinetForm->web_url) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($cabinetForm->position) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($cabinetForm->description) !!}
                                    </div>

                                    {!! form_row($cabinetForm->send) !!}
                                </fieldset>
                            {!! form_end($cabinetForm, false) !!}
                        </div>
                        <div class="tab-pane fade" id="component-1-2" role="tabpanel" aria-labelledby="component-1-2">
                            {!! form_start($contactsForm) !!}
                                <fieldset class="row gutter-2">
                                    <div class="col-md-12">
                                        {!! form_row($contactsForm->country_id) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($contactsForm->city_id) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($contactsForm->address) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($contactsForm->phone) !!}
                                    </div>
                                </fieldset>
                                <fieldset class="row gutter-2">
                                    <legend class="legend">SOCIAL LINKS</legend>
                                    <div class="col-md-12">
                                        {!! form_row($contactsForm->facebook_url) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($contactsForm->linkedin_url) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($contactsForm->twitter_url) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($contactsForm->xing_url) !!}
                                    </div>

                                    {!! form_row($cabinetForm->send) !!}
                                </fieldset>
                            {!! form_end($contactsForm, false) !!}
                        </div>
                        <div class="tab-pane fade" id="component-1-3" role="tabpanel" aria-labelledby="component-1-3">
                            {!! form_start($certificatesForm) !!}
                                <fieldset class="row gutter-2">
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="inputText6">{!! form_label($cabinetForm->association_membership) !!}</label>--}}
{{--                                            <div class="input-group">--}}

{{--                                                {!! form_widget($cabinetForm->association_membership) !!}--}}

{{--                                                <div class="input-group-append">--}}
{{--                                                    <button class="btn btn-primary btn-icon" type="button" id="button-addon1">--}}
{{--                                                        <i class="icon-plus"></i>--}}
{{--                                                    </button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="inputText6">{!! form_label($certificatesForm->certificate_id) !!}</label>
                                            <div class="input-group">
                                                {!! form_widget($certificatesForm->certificate_id) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($certificatesForm->date) !!}
                                    </div>

                                    {!! form_row($certificatesForm->send) !!}
                                </fieldset>
                                @if($certificates->isNotEmpty())
                                <fieldset class="row gutter-2">
                                    <legend class="legend">MEINE ZERTIFIKATE</legend>
                                    <div class="col-md-12">
                                        <ul id="certificates" class="pin-list">
                                            @foreach($certificates as $certificate)
                                                <li>
                                                    {{ $certificate->name }}
                                                    <span>{{ $certificate->pivot->date }}</span>
                                                    <a id="cert{{ $certificate->id }}" href="" class="icon-x cert-delete" data-certid="{{ $certificate->id }}"></a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </fieldset>
                                @endif
                            {!! form_end($certificatesForm, false) !!}
                        </div>
                        <div class="tab-pane fade" id="component-1-4" role="tabpanel" aria-labelledby="component-1-4">
                            {!! form_start($completedProjectsForm) !!}
                                <fieldset class="row gutter-2">
                                    <div class="col-md-12">
                                        {!! form_row($completedProjectsForm->name) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($completedProjectsForm->image) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($completedProjectsForm->description) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($completedProjectsForm->date) !!}
                                    </div>
                                    <div class="col-md-12">
                                        {!! form_row($completedProjectsForm->link) !!}
                                    </div>

                                    {!! form_row($completedProjectsForm->send) !!}
                                </fieldset>
                                @if($projects->isNotEmpty())
                                <fieldset class="row gutter-2">
                                    <legend class="legend">MEINE PROJEKTE</legend>
                                    <div class="col-md-12">
                                        <ul id="projects" class="pin-list">
                                            @foreach($projects as $project)
                                                <li>
                                                    {{ $project->name }}
                                                    <a id="cert{{ $project->id }}" href="" class="icon-x project-delete" data-projectid="{{ $project->id }}"></a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </fieldset>
                                @endif
                            {!! form_end($completedProjectsForm, false) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php Asset::js('/js/cabinet_forms_validate.js') @endphp
@endsection

@script
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#upload').css({'background-image':'url(' + e.target.result + ')'});
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    $('#button-addon1').on('click', function(e) {
        e.preventDefault();

        $.ajax({
            url: '{{ route('cabinet.add.association') }}',
            type: 'POST',
            data: $('select#assoc_list').serialize(),
            success: data => {
                $('#associations').append(data);
            }
        })
    });

    $(document).on('click', '.assoc-delete', function(e) {
        e.preventDefault();

        $.ajax({
            url: '{{ route('cabinet.delete.association') }}',
            type: 'POST',
            data: {"assocID": $(this).data('associd')},
            success: data => {
                $(`#assoc${data.deleted}`).closest('li').remove();
            }
        })
    });

    $(document).on('click', '.cert-delete', function(e) {
        e.preventDefault();

        $.ajax({
            url: '{{ route('cabinet.delete.certificate') }}',
            type: 'POST',
            data: {"certID": $(this).data('certid')},
            success: data => {
                $(`#cert${data.deleted}`).closest('li').remove();
            }
        })
    });

    $(document).on('click', '.project-delete', function(e) {
        e.preventDefault();

        $.ajax({
            url: '{{ route('cabinet.delete.project') }}',
            type: 'POST',
            data: {"projectID": $(this).data('projectid')},
            success: data => {
                $(`#project${data.deleted}`).closest('li').remove();
            }
        })
    });

    $('#fakeUpload').click(function(e) {
        e.preventDefault();
        $('#avatar').trigger('click');
    });

    $("#avatar").change(function() {
        readURL(this);

        const formData = new FormData();
        formData.append('avatar', $('#avatar')[0].files[0]);

        $.ajax({
            url: '{{ route('cabinet.avatar.store') }}',
            data: formData,
            type: 'POST',
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            success: data => {
                // $(`#assoc${data.deleted}`).closest('li').remove();
            }
        });
    });
</script>
@endscript

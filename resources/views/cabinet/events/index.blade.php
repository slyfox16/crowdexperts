@extends('frontend.layouts.app')

@section('header')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="font-weight-bold">Jetzt die besten Crowd Experten für Ihr Projekt finden</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('sidebar')
    <!-- sidebar -->
    <aside class="overlay">
        <button class="overlay-toggler burger text-black" type="button">
            <span></span>
        </button>
        <div class="overlay-body">
            <div>
                <div class="row">
                    <div class="col">
                        <h2 class="h1 mb-2">Create Event</h2>
                    </div>
                </div>
                {!! form_start($eventForm) !!}
                    <fieldset class="row gutter-2">
                        <div class="col-md-6 col-lg-12">
                            {!! form_row($eventForm->title) !!}
                        </div>
                        <div class="col-md-6 col-lg-12">
                            {!! form_row($eventForm->description) !!}
                        </div>
                        <div class="col-12">
                            {!! form_row($eventForm->facebook_url) !!}
                        </div>
                        <div class="col-12">
                            {!! form_row($eventForm->twitter_url) !!}
                        </div>
                        <div class="col-12">
                            {!! form_row($eventForm->xing_url) !!}
                        </div>
                        <div class="col-12">
                            {!! form_row($eventForm->web_url) !!}
                        </div>
                        <div class="col-12">
                            {!! form_row($eventForm->daterange) !!}
                        </div>
                        <div class="col-12">
                            {!! form_row($eventForm->address) !!}
                        </div>
                        <div class="col-12">
                            {!! form_row($eventForm->country_id) !!}
                        </div>
                        <div class="col-12">
                            {!! form_row($eventForm->city_id) !!}
                        </div>
                        <div class="col-12">
                            {!! form_row($eventForm->image) !!}
                        </div>
                    </fieldset>

                    <div class="overlay-footer">
                        <div>
                            {!! form_widget($eventForm->send) !!}
                        </div>
                    </div>

                {!! form_end($eventForm, false) !!}
            </div>
        </div>

    </aside>
@endsection

@section('content')
    <section class="page-title">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                    <h1 class="font-weight-bold">Events</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Events</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <ul class="page-options">
                        <li>
                            <button class="btn btn-primary overlay-toggler" type="button">
                                New Event
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- main info -->
    <section class="bg-light">
        <div class="container mt-0">

            <div class="row gutter-md-4 gutter-6">
                @forelse ($events as $event)
                    <div class="col-md-6 col-lg-4">
                        <a href="{{ route('cabinet.event', $event) }}" class="event">
                            <time>{{ optional($event->date_start)->format('d F Y') }}</time>
                            <h2 class="event-title">{{ $event->title }}</h2>
                            <span class="event-location">{{ $event->getCountryCity() }}</span>
                        </a>
                    </div>
                @empty
                    <div class="col-12">
                        <h2 class="font-weight-bold mb-1">No events found.</h2>
                        <p class="lead">Keep checking, more events are coming</p>
                    </div>
                @endforelse
            </div>

{{--            <div class="row">--}}
{{--                <div class="col text-center">--}}
{{--                    <a href="" class="btn btn-outline-primary">Load More</a>--}}
{{--                </div>--}}
{{--            </div>--}}

        </div>
    </section>

    @php Asset::js('/js/cabinet_forms_validate.js') @endphp

@endsection

@extends('frontend.layouts.app')

@section('content')
    <!-- page title -->
    <section class="page-title">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('cabinet.events') }}">Events</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $event->title }}</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <ul class="page-options">
                        @livewire('event-add-user', ['event' => $event])
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <section class="bg-light">
        <div class="container mt-0">

            <div class="row gutter-4 gutter-lg-6">
                <div class="col-6 col-md-4 col-lg-3">
                    <figure class="mb-3">
                        <img src="{{ schema_asset($event->getFile('image')) }}" alt="Event Image">
                    </figure>

                </div>
                <div class="col-md-10 col-lg-6">
                    <div class="row">
                        <div class="col">
                            <h1 class="mb-2">{{ $event->title }}</h1>
                            {{ $event->description }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <ul class="meta">
                                @if($event->getCountryCity())
                                    <li>
                                        <span>
                                            <i class="icon-map-pin mr-1"></i>{{ $event->getCountryCity() }}
                                        </span>
                                    </li>
                                @endif
                                @if($event->web_url)
                                    <li>
                                        <a href="{{ $event->web_url }}"><i class="icon-link mr-1"></i></a>
                                    </li>
                                @endif
                                @if($event->facebook_url)
                                    <li>
                                        <a href="{{ $event->facebook_url }}" class="icon-facebook-2"></a>
                                    </li>
                                @endif
                                @if($event->twitter_url)
                                    <li>
                                        <a href="{{ $event->twitter_url }}" class="icon-twitter-2"></a>
                                    </li>
                                @endif
                                @if($event->xing_url)
                                    <li>
                                        <a href="{{ $event->xing_url }}" class="icon-linkedin-2"></a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 text-lg-right">
                    <ul class="list-group list-group-sm list-group-minimal">
                        <li class="list-group-item">
                            <h5 class="mb-0">{{ $event->date_start->format('d M') }} - {{ $event->date_end->format('d M, Y') }}</h5>
                            <span class="eyebrow text-muted">Date</span>
                        </li>
                        <li class="list-group-item">
                            <h5 class="mb-0">{{ $event->getCountryCity() }}</h5>
                            <span class="eyebrow text-muted">Location</span>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>


    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="accordion" id="accordion-1">
                        <div class="card">
                            <div class="card-header" id="heading-1-1">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                            data-target="#collapse-1-1" aria-expanded="false" aria-controls="collapse-1-1">
                                        Diese Experten Nehmen Teil
                                    </button>
                                </h2>
                            </div>

                            <div id="collapse-1-1" class="collapse" aria-labelledby="heading-1-1" data-parent="#accordion-1">
                                <div class="card-body">
                                    @livewire('event-users', ['event' => $event])
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-1-2">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                            data-target="#collapse-1-2" aria-expanded="false" aria-controls="collapse-1-2">
                                        Location
                                    </button>
                                </h2>
                            </div>

                            <div id="collapse-1-2" class="collapse" aria-labelledby="heading-1-2" data-parent="#accordion-1">
                                <div id="card-body" class="card-body" style="height: 500px">
                                    <iframe src="https://www.google.com/maps/embed/v1/place?q={{ $address }}&key=AIzaSyDJVbJ6Hx1ujltysxHUZw0PXUakYihUcKA" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php Asset::js('https://maps.google.com/maps/api/js?key=' .  config('googlmapper.key') ) @endphp
@endsection

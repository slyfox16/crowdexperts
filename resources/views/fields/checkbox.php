<?php $laravel_form_builder_views = dirname(dirname(__FILE__)); ?>
<div class="form-group row">
<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
        <div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
<?php endif; ?>

<div class="custom-control custom-switch mb-sm-0 col-12">
<?php if ($showField): ?>
    <?= Form::checkbox($name, $options['value'], $options['checked'], $options['attr']) ?>

    <?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
        <?= htmlspecialchars_decode(Form::customLabel($name, $options['label'], $options['label_attr'])) ?>
    <?php endif; ?>
<?php endif; ?>
</div>

<?php include $laravel_form_builder_views . '/vendor/laravel-form-builder/help_block.php' ?>

<?php include $laravel_form_builder_views . '/vendor/laravel-form-builder/errors.php' ?>

<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
</div>

<?php $laravel_form_builder_views = dirname(dirname(__FILE__)); ?>
<div class="form-group row" <?php if (isset($options['data-lang'])): ?> data-lang="<?= $options['data-lang'] ?>"<?php endif; ?> style="<?= \Html::attributes(data_get($options, 'style')) ?>">
<?php if ($showLabel && $showField): ?>
<?php if ($options['wrapper'] !== false): ?>
<div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
    <?php endif; ?>

    <?php if ($showField): ?>
        <?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
            <?= htmlspecialchars_decode(Form::customLabel($name, $options['label'], $options['label_attr'])) ?>
        <?php endif; ?>
        <?= Form::input($type, $name, $options['value'], $options['attr']) ?>

        <?php include $laravel_form_builder_views . '/vendor/laravel-form-builder/help_block.php' ?>
    <?php endif; ?>

    <?php include $laravel_form_builder_views . '/vendor/laravel-form-builder/errors.php' ?>

    <?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
</div>
<?php endif; ?>
<?php endif; ?>
</div>

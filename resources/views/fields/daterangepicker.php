<?php $laravel_form_builder_views = dirname(dirname(__FILE__)); ?>

<?php if ($showLabel && $showField): ?>
<?php if ($options['wrapper'] !== false): ?>
<div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
    <?php endif; ?>

    <?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
        <?= Form::customLabel($name, $options['label'], $options['label_attr']) ?>
    <?php endif; ?>

    <?php if ($showField): ?>

        <?= Form::input($type, $name, $options['value'], $options['attr']) ?>

        <?php
        Asset::setParams($options['widgetOptions']);

        $parameters = Asset::getParams();
        $element = '#' . $options['attr']['id'];
        Asset::script("$('{$element}').daterangepicker({$parameters});");
        ?>

        <?php include $laravel_form_builder_views . '/vendor/laravel-form-builder/help_block.php' ?>
    <?php endif; ?>

    <?php include $laravel_form_builder_views . '/vendor/laravel-form-builder/errors.php' ?>

    <?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
</div>
<?php endif; ?>
<?php endif; ?>

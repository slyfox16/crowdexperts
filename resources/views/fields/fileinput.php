<?php $laravel_form_builder_views = dirname(dirname(__FILE__)); ?>
<div class="form-group row">
<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    <div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
<?php endif; ?>

<?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
    <?= Form::customLabel($name, $options['label'], $options['label_attr']) ?>
<?php endif; ?>

<?php if ($showField): ?>

    <?= FileInput::init([
        'uploadUrl' => 'upload',
    ])
        ->withSuffix(Str::slug($name, '_'))
        ->setCreateJSInit(strpos($name, '[__NAME__]') === false)
        ->withName($name)
        ->setSimpleAttr(data_get($options, 'simpleAttr'))
        ->setModel($options['model'])
        ->createHtml();
    ?>

    <?php include $laravel_form_builder_views . '/vendor/laravel-form-builder/help_block.php' ?>
<?php endif; ?>

<?php include $laravel_form_builder_views . '/vendor/laravel-form-builder/errors.php' ?>

<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    </div>
    <?php endif; ?>
<?php endif; ?>

</div>
